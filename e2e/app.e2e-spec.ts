import { QuasarPage } from './app.po';

describe('quasar App', () => {
  let page: QuasarPage;

  beforeEach(() => {
    page = new QuasarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
