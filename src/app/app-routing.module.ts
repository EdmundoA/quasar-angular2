import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { AuthGuard } from "./auth/auth-guard.module";
import { UsersCreateComponent } from "./components/users-create/users-create.component";
import { UsersListComponent } from "./components/users-list/users-list.component";
import { OrganizationsComponent } from "./components/organizations/organizations.component";
import { StoresCreateComponent } from "./components/stores-create/stores-create.component";
import { OrganizacionCreateComponent } from "./components/organizacion-create/organizacion-create.component";
import { OrganizationsListComponent } from "./components/organizations-list/organizations-list.component";
import { PermissionsComponent } from "./components/permissions/permissions.component";
import { ClientesComponent } from "./components/clientes/clientes.component";
import { ClientesListComponent } from "./components/clientes-list/clientes-list.component";
import { StoresComponent } from "./components/stores/stores.component";
import { StoresListComponent } from "./components/stores-list/stores-list.component";
import { ChainsComponent } from "./components/chains/chains.component";
import { ChainsListComponent } from "./components/chains-list/chains-list.component";
import { ClientesCreateComponent } from "./components/clientes-create/clientes-create.component";
import { ClientesResumenComponent } from "./components/clientes-resumen/clientes-resumen.component";
import { ClientesDetailComponent } from "./components/clientes-detail/clientes-detail.component";
import { ChainsCreateComponent } from "./components/chains-create/chains-create.component";
import { ElementsComponent } from "./components/elements/elements.component";
import { ElementsListComponent } from "./components/elements-list/elements-list.component";
import { ElementsCreateComponent } from "./components/elements-create/elements-create.component";
import { RoutesComponent } from './components/routes/routes.component';
import { RoutesListComponent } from './components/routes-list/routes-list.component';
import { RoutesCreateComponent } from './components/routes-create/routes-create.component';
import { CampaignComponent } from './components/campaign/campaign.component';
import { CampaignListComponent } from './components/campaign-list/campaign-list.component';
import { CampaignCreateComponent } from './components/campaign-create/campaign-create.component';
import { GeneralComponent } from './components/general/general.component';
import { GeneralResumeComponent } from './components/general-resume/general-resume.component';
import { TopComponent } from './components/top/top.component';
import { TopReportComponent } from './components/top-report/top-report.component';
import { ClientesReportComponent } from './components/clientes-report/clientes-report.component';
import { LevelServiceComponent } from './components/level-service/level-service.component';
import { LevelServiceDetailComponent } from './components/level-service-detail/level-service-detail.component';
import { BtlComponent } from './components/btl/btl.component';
import { BtlListComponent } from './components/btl-list/btl-list.component';
import { BtlCreateComponent } from './components/btl-create/btl-create.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { MeasurementComponent } from './components/measurement/measurement.component';
import { MeasurementListComponent } from './components/measurement-list/measurement-list.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { OpportunitiesComponent } from './components/opportunities/opportunities.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { UserHistorialComponent } from './components/user-historial/user-historial.component';
import { PromotionsComponent } from './components/promotions/promotions.component';
import { PromotionsListComponent } from './components/promotions-list/promotions-list.component';
import { PromotionsCreateComponent } from './components/promotions-create/promotions-create.component';
import { ClusterComponent } from './components/cluster/cluster.component';
import { IncidentComponent } from './components/incident/incident.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactsListComponent } from './components/contacts-list/contacts-list.component';

const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, children: [] },
  { path: 'home', component: HomeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'inventory', component: InventoryComponent,  pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'client', component: ClientesResumenComponent,  pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'client/detail/:idCampaign/:code/:campaign/:elementType', component: ClientesDetailComponent,  pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'confirmation', component: ConfirmationComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'opportunities', component: OpportunitiesComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'maintenance', component: MaintenanceComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'cluster', component: ClusterComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'incidents', component: IncidentComponent, pathMatch: 'full', canActivate: [AuthGuard] },  

  { path: 'contacts', component: ContactsComponent, children: [
    { path: '', component: ContactsListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'general', component: GeneralComponent, children:[
    { path: '', component: GeneralResumeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'measurement', component: MeasurementComponent, children:[
    { path: '', component: MeasurementListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'top', component: TopComponent, children:[
    { path: '', component: TopReportComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'promotions', component: PromotionsComponent, children: [
    { path: '', component: PromotionsListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: PromotionsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: PromotionsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'users', component: UsersComponent, children:[
    { path: '', component: UsersListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: UsersCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: UsersCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/permisos', component: PermissionsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/historial', component: UserHistorialComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'stores', component: StoresComponent, children:[
    { path: '', component: StoresListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: StoresCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: StoresCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'chains', component: ChainsComponent, children:[
    { path: '', component: ChainsListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: ChainsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: ChainsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },

  ]},

  { path: 'organizations', component: OrganizationsComponent, children:[
    { path: '', component: OrganizationsListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: OrganizacionCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: OrganizacionCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'permisos', component: PermissionsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'campaing', component: CampaignComponent, children:[
    { path: '', component: CampaignListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: CampaignCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: CampaignCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'elements', component: ElementsComponent, children:[
    { path: '', component: ElementsListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: ElementsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: ElementsCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'routes', component: RoutesComponent, children: [
    { path: '', component: RoutesListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: RoutesCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: RoutesCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'clientes', component: ClientesComponent, children:[
    { path: '', component: ClientesListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: ClientesCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: ClientesCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'report', component: ClientesReportComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'service', component: LevelServiceComponent, children: [
    { path: '', component: LevelServiceDetailComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},

  { path: 'btl', component: BtlComponent, children:[
    { path: '', component: BtlListComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'create', component: BtlCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: ':id/edit', component: BtlCreateComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ]},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
