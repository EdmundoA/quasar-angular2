import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from './services/user.service';
import { OrganizationService } from "./services/organization.service";
import { SessionService } from './services/session.service';
import { StoresService } from "./services/stores.service";
import { PromotionsService } from './services/promotions.service';
import { ChainsService } from './services/chains.service';
import { RoutesService } from './services/routes.service';
import { ClientService } from './services/clients.service';
import { ElementsService } from "./services/elements.service";
import { BtlService } from "./services/btl.service";
import { CampaignsService } from "./services/campaigns.service";
import { PermissionsService } from './services/permissions.service';
import { AppPaths } from './app.path';
import { AppSettings } from './app.settings';
import { LoaderEvent } from "./services/loader-event";
import { Quantity } from "./services/quantity.service";
import { initJQuery } from 'jquery-ts';
import { UserEvent}  from "./services/user-event";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [
    UserService,
    LoaderEvent,
    Quantity,
    PromotionsService,
    OrganizationService,
    StoresService,
    ChainsService,
    RoutesService,
    ElementsService,
    CampaignsService,
    BtlService,
    PermissionsService,
    UserEvent,
  ]
})
export class AppComponent implements OnInit {
  public showHeader: Boolean = false;
  public name: String;
  public showMenu: Boolean = false;
  public showClient: Boolean = false;
  public showSupervisor: Boolean = false;
  public showJefeCategoria: Boolean = false;
  public showManager: Boolean = false;
  public showGerente: Boolean = false;
  public showAdmin: Boolean = false;
  public showLoader = false;
  public promos;
  public logo: String;
  public promo;
  public URL = AppSettings.BASE_PATH;
  public currYear: String = (new Date()).getFullYear().toString();

  /* Cantidad Menú */
  public totalusers = 0;
  public totalOrgas = 0;
  public totalStores = 0;
  public totalContacts = 0;
  public totalChains = 0;
  public totalClients = 0;
  public totalRoutes = 0;
  public totalElements = 0;
  public totalCampaigns = 0;
  public totalbtl = 0;
  public permissions:any;

  constructor(
    private router: Router,
    private userService: UserService,
    private loader: LoaderEvent,
    private quantity: Quantity,
    private session: SessionService,
    private promotionsS: PromotionsService,
    private permissionS: PermissionsService,
    private orgaS: OrganizationService,
    private storeS: StoresService,
    private chainS: ChainsService,
    private clientS: ClientService,
    private routesS: RoutesService,
    private elementS: ElementsService,
    private campaignS: CampaignsService,
    private btlS: BtlService,
    private userEvent: UserEvent,
  ) {}

  ngOnInit() {
    this.logo = AppPaths.LOGO;
    // evento al cambiar de ruta / página
    this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          // esconder menu
          if(this.router.url === '/login') {
            this.showHeader = false;
            this.showMenu = false;
            if (this.userService.isAuthenticated()) {
              if(this.session.getObject('user').role == 'Admin'){
                this.router.navigate(['/users']);
              }else if(this.session.getObject('user').role == 'Cliente'){
                this.router.navigate(['/client']);
              }else if(this.session.getObject('user').role == 'Gerente' || this.session.getObject('user').role == 'Supervisor' || this.session.getObject('user').role == 'Jefe de categoria') {
                this.router.navigate(['/general']);
              }
            }
          }else {
            console.log(this.router.url);
            if(this.router.url.indexOf('clientes/report') == -1 && this.router.url.indexOf('client/detail') == -1){
              this.session.destroy('client_report');
              console.log('fue destruido');
            }

            this.showHeader = true;
            this.showMenu = true;
            this.name = this.session.getObject('user').firstname;
            let role = this.session.getObject('user').role;

            if(role == 'Cliente'){
              this.showMenu = false;
            }else{
              this.showClient = role == 'Gerente' || role == 'Admin';
              this.showManager = role == 'Admin';
              this.showAdmin = role == 'Admin';
              this.showGerente = role == 'Gerente';
              this.showSupervisor = role == 'Supervisor';
              this.showJefeCategoria = role == 'Jefe de categoria';
              if (this.showGerente) {
                this.promotionsS.promotionByManager()
                .subscribe( response => {
                  console.log(response);
                  this.promos = response.result;
                });
              }
              if (this.showAdmin) {
                this.quantity.fire(true);
              }
            }
          }
        }
      }
    );
    this.registerBroadcast();
  }

  goBack() {
    if (this.session.getObject('user').role == 'Cliente') {
      this.router.navigate(['/client']);
    } else {
      this.router.navigate(['/users']);
    }

  }

  destroySession() {
    localStorage.clear();
  }

  getPermissions(){
    //this.loader.fire(true);
    if(this.session.getObject('user') && this.session.getObject('user').role == 'Admin'){
      this.permissionS.getPermissionsPerUser(this.session.getObject('user')._id)
      .subscribe(response => {
        for (var key in response.result) {
          let tPermission = {};
          for (var i = response.result[key].length - 1; i >= 0; i--) {
            tPermission[response.result[key][i]] = true;
          }
          response.result[key] = tPermission;
        }
        this.session.setObject('permissions', response.result);
        console.log(response.result);
        this.permissions = response.result;
        this.userEvent.fire(this.permissions);
        //this.loader.fire(false);
      });
    }  
  }

  registerBroadcast() {
    this.loader.on()
    .subscribe(value => {
      this.showLoader = value;
    });
    this.quantity.on()
    .subscribe(value => {
      if (value) {
        this.getPermissions();
        // this.totalStores = 0;
        // this.totalContacts = 0;
        // this.totalCampaigns = 0;
        // this.userService.getUsers()
        // .subscribe( response => {
        //   this.totalusers = response.result.length;
        //   this.orgaS.getAll()
        //   .subscribe( response => {
        //     this.totalOrgas = response.result.length;
        //     this.storeS.getStores()
        //     .subscribe( response => {
        //       for (let i = 0; i < response.result.length; i++) {
        //         if (response.result[i].enabled) {
        //           this.totalStores++;
        //         }
        //         this.totalContacts +=  response.result[i].contacts.length;
        //       }                      
        //       this.chainS.getChains()
        //       .subscribe( response => {
        //         this.totalChains = response.result.length;
        //         this.clientS.getClients()
        //         .subscribe( response => {
        //           this.totalClients = response.result.length;
        //           this.routesS.getRoute()
        //           .subscribe( response => {
        //             this.totalRoutes = response.result.length;
        //             this.elementS.getAll()
        //             .subscribe( response => {
        //               this.totalElements = response.result.length;
        //               this.campaignS.getCampaign()
        //               .subscribe( response => {
        //                 for (let i = 0; i < response.result.length; i++) {
        //                   for (let o = 0; o < response.result[i].elements.length; o++) {
        //                       this.totalCampaigns++;
        //                   }
        //                 }
        //                 this.btlS.getAll()
        //                 .subscribe( response => {
        //                   this.totalbtl = response.result.length;
        //                 });
        //               });
        //             });
        //           });
        //         });
        //       });
        //     });
        //   });
        // });
      }        
    });
  }

}
