import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Directive } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions, XHRBackend } from '@angular/http';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Broadcaster } from './services/broacaster';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/auth-guard.module';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SessionService } from './services/session.service';
import { UserService } from './services/user.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
//import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { LoadersCssModule } from 'angular2-loaders-css';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { CustomAuthHttp } from "./auth/custom-auth-http";
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { OrganizacionCreateComponent } from './components/organizacion-create/organizacion-create.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { LoaderEvent } from "./services/loader-event";
import { StoresCreateComponent } from './components/stores-create/stores-create.component';
import { OrganizationsListComponent } from './components/organizations-list/organizations-list.component';
import { PermissionsComponent } from './components/permissions/permissions.component';
import { PermissionsService } from './services/permissions.service';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ChainsService } from './services/chains.service';
import { ClientService } from './services/clients.service';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { ClientesListComponent } from './components/clientes-list/clientes-list.component';
import { StoresComponent } from './components/stores/stores.component';
import { StoresListComponent } from './components/stores-list/stores-list.component';
import { ChainsComponent } from './components/chains/chains.component';
import { ChainsListComponent } from './components/chains-list/chains-list.component';
import { ChainsCreateComponent } from './components/chains-create/chains-create.component';
import { ClientesCreateComponent } from './components/clientes-create/clientes-create.component';
import { ElementsComponent } from "./components/elements/elements.component";
import { ElementsListComponent } from "./components/elements-list/elements-list.component";
import { ElementsCreateComponent } from "./components/elements-create/elements-create.component";
import { RoutesComponent } from './components/routes/routes.component';
import { RoutesListComponent } from './components/routes-list/routes-list.component';
import { RoutesCreateComponent } from './components/routes-create/routes-create.component';
import { RlTagInputModule } from 'angular2-tag-input';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { CampaignComponent } from './components/campaign/campaign.component';
import { CampaignListComponent } from './components/campaign-list/campaign-list.component';
import { CampaignCreateComponent } from './components/campaign-create/campaign-create.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ClientesResumenComponent } from './components/clientes-resumen/clientes-resumen.component';
import { ClientesDetailComponent } from './components/clientes-detail/clientes-detail.component';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MomentModule } from 'angular2-moment';
import { OrderModule } from 'ngx-order-pipe';
import { DpDatePickerModule } from "ng2-date-picker";
import { ServiceComponent } from './components/service/service.component';
import { GeneralComponent } from './components/general/general.component';
import { GeneralResumeComponent } from './components/general-resume/general-resume.component';
import { GeneralService } from "./services/general.service";
import { TopComponent } from './components/top/top.component';
import { TopReportComponent } from './components/top-report/top-report.component';
import { ClientesReportComponent } from './components/clientes-report/clientes-report.component';
import { LevelServiceComponent } from './components/level-service/level-service.component';
import { LevelServiceDetailComponent } from './components/level-service-detail/level-service-detail.component';
import { BtlComponent } from './components/btl/btl.component';
import { BtlListComponent } from './components/btl-list/btl-list.component';
import { BtlCreateComponent } from './components/btl-create/btl-create.component';
import { BtlService } from './services/btl.service';
import { InventoryComponent } from './components/inventory/inventory.component';
import { InventoryService } from "./services/inventories.service";
import { CampaignsService } from './services/campaigns.service';
import { MeasurementComponent } from './components/measurement/measurement.component';
import { MeasurementListComponent } from './components/measurement-list/measurement-list.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { OpportunitiesComponent } from './components/opportunities/opportunities.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { RouteService } from './services/route.service';
import { UserHistorialComponent } from './components/user-historial/user-historial.component';
import { PromotionsComponent } from './components/promotions/promotions.component';
import { PromotionsListComponent } from './components/promotions-list/promotions-list.component';
import { PromotionsCreateComponent } from './components/promotions-create/promotions-create.component';
import { IncidentComponent } from './components/incident/incident.component';
import { ClusterComponent } from './components/cluster/cluster.component';
import { LightboxModule } from 'angular2-lightbox';
import { KeysPipe } from './objects.pipe';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactsListComponent } from './components/contacts-list/contacts-list.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { ModalModule } from "ngx-modal";

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UsersComponent,
    UsersCreateComponent,
    OrganizationsComponent,
    OrganizacionCreateComponent,
    UsersListComponent,
    StoresCreateComponent,
    OrganizationsListComponent,
    PermissionsComponent,
    ClientesComponent,
    ClientesListComponent,
    StoresComponent,
    StoresListComponent,
    ChainsComponent,
    ChainsListComponent,
    ChainsCreateComponent,
    ElementsComponent,
    ElementsListComponent,
    ElementsCreateComponent,
    ClientesCreateComponent,
    RoutesComponent,
    RoutesListComponent,
    RoutesCreateComponent,
    CampaignComponent,
    CampaignListComponent,
    CampaignCreateComponent,
    ClientesResumenComponent,
    ClientesDetailComponent,
    ServiceComponent,
    GeneralComponent,
    GeneralResumeComponent,
    TopComponent,
    TopReportComponent,
    ClientesReportComponent,
    LevelServiceComponent,
    LevelServiceDetailComponent,
    BtlComponent,
    BtlListComponent,
    BtlCreateComponent,
    InventoryComponent,
    MeasurementComponent,
    MeasurementListComponent,
    ConfirmationComponent,
    OpportunitiesComponent,
    IncidentComponent,
    MaintenanceComponent,
    UserHistorialComponent,
    PromotionsComponent,
    PromotionsListComponent,
    PromotionsCreateComponent,
    ClusterComponent,
    KeysPipe,
    ContactsComponent,
    ContactsListComponent,
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    FormsModule,
    Ng2FilterPipeModule,
    HttpModule,
    ModalModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    BootstrapModalModule,
    AppRoutingModule,
    LoadersCssModule,
    RlTagInputModule,
    NguiDatetimePickerModule,
    LightboxModule,
    BsDropdownModule.forRoot(),
    AuthModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    }),
    ReactiveFormsModule,
    FileUploadModule,
    ChartsModule,
    MomentModule,
    OrderModule,
    DpDatePickerModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularDateTimePickerModule,
  ],
  providers: [
    AuthModule,
    Broadcaster,
    AuthGuard,
    UserService,
    ChainsService,
    ClientService,
    SessionService,
    PermissionsService,
    CustomAuthHttp,
    LoaderEvent,
    GeneralService,
    RouteService,
    BtlService,
    InventoryService,
    CampaignsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
