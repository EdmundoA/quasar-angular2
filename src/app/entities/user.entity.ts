export interface UserEntity{
  _id: String,
  firstname: String,
  lastnameP: String,
  lastnameM: String,
  email: String,
  alias: String,
  gender: String,
  documentType: String,
  documentNumber: String,
  birthday: Date,
  password: String,
  password2: String,
  rol: String,
  role: String,
  key: String,
}
