import { UserEntity } from "./user.entity";

export interface RoutesEntity{
  route: any,
  name: String,
  stores: any,
  creator: UserEntity,
  created: String,
  orga: String,
}
