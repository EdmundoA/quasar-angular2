import { UserEntity } from "./user.entity";

export interface StoreEntity{
  name: String,
  shortName: String,
  sapCode: Number,
  region: String,
  contacts: Array<any>;
  zone: String,
  cluster: String,
  cluster2: String,
  format: String,
  route: Number,
  address: String,
  department: String,
  province: String,
  district: String,
  lat: Number,
  lon: Number,
  _id: String,
  organization: String,
  chain: String,
  sector: String,

}
