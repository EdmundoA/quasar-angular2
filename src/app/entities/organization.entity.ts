import {UserEntity} from "./user.entity";

export interface OrganizationEntity{
  _id: String,
  name: String,
  shortName: String,
  creator: UserEntity,
  created: String,
  orga: String,
}
