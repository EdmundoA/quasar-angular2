import {Component, OnInit, ViewChild} from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";
import {LoaderEvent} from "../../services/loader-event";
import {ClientService} from "../../services/clients.service";
import {SessionService} from "../../services/session.service";
import {ClientesResumenComponent} from "../clientes-resumen/clientes-resumen.component";

@Component({
  selector: 'app-clientes-report',
  templateUrl: './clientes-report.component.html',
  providers: [
    ClientService,
    SessionService,
  ]
})
export class ClientesReportComponent implements OnInit {

  @ViewChild(ClientesResumenComponent) resume: ClientesResumenComponent;

  public clients = [];
  public showCampaigns = false;
  public clientFilter: any = { name: '' };

  constructor(
    private subtitleEvent: SubtitleEvent,
    private loader: LoaderEvent,
    private clientService: ClientService,
    private session: SessionService,
  ) { }

  ngOnInit() {
    this.subtitleEvent.fire('Reportes');
    this.loader.fire(true);
    this.clientService.getClients()
    .subscribe( response => {
      this.clients = response.result;
      this.loader.fire(false);
      if(this.session.getItem('client_report')){
        this.resume.getCampaigns(this.session.getItem('client_report'));
        this.showCampaigns = true;
      }
    });
  }

  onSelect(id){
    this.session.setItem('client_report', id);
    this.resume.getCampaigns(id);
    this.showCampaigns = true;
  }
}
