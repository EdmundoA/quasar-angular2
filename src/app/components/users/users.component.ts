import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent} from "../../services/subtitle-event";
import { UserEvent} from "../../services/user-event";
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  providers: [
    SubtitleEvent,
    UserEvent,
  ]
})
export class UsersComponent implements OnInit {

  public subtitle: string = '';
  public permissions:any;

  constructor(
    private subtitleEvent: SubtitleEvent,
    private userEvent: UserEvent,
    private session:SessionService,
  ){ }

  ngOnInit(){
    this.registerBroadcast();
    this.permissions = this.session.getObject('permissions');
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });

    this.userEvent.on()
    .subscribe(value => {
      this.permissions = value;
    });
  }

}
