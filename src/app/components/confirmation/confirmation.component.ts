import { Component, OnInit } from '@angular/core';
import { LoaderEvent } from '../../services/loader-event';
import { CampaignsService } from '../../services/campaigns.service';
import { AppSettings } from '../../app.settings';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  providers: [
    LoaderEvent
  ]
})
export class ConfirmationComponent implements OnInit {
	
	public userFilter: any = { name: '', store: { name: ''}, elementType: '', confirmed: {
    ok: null,
  } };
	public confirmations = [];
  public URL = AppSettings.BASE_PATH;
  public LOW = 'low/';

  constructor(
      private campaignS: CampaignsService,
      private loader: LoaderEvent,
  	) { }

  ngOnInit() {
  	this.loader.fire(true);
  	this.campaignS.getConfirmation()
  		.subscribe( response => {
        // console.log(response);
  			// console.log(response.result.sort(this.compare));
  			this.confirmations = response.result.sort(this.date).sort(this.compare);
  			this.loader.fire(false);
  		});
  }

  compare(a) {
    
    if(a.confirmed.ok == true) return -1;
    else if (a.confirmed.ok == false) return 1;
    return 0;
  }

  date(a, b) {
    let date1 = new Date(a.confirmed.date);
    let date2 = new Date(b.confirmed.date);
    if(date1 < date2) return -1;
    else if (date2 < date1) return 1;
    return 0;
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone2");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone2');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone2").css({visibility:'hidden'});
            $("#clone2 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone2").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
