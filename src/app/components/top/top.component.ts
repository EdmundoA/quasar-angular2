import { Component, OnInit } from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  providers: [
    SubtitleEvent
  ]
})
export class TopComponent implements OnInit {

  public subtitle: string = '';

  constructor(
    private subtitleEvent: SubtitleEvent
  ){

  }

  ngOnInit(){
    this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
