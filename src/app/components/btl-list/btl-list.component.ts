import { Component, OnInit } from '@angular/core';
import {LoaderEvent} from "../../services/loader-event";
import {BtlService} from "../../services/btl.service";
import {SubtitleEvent} from "../../services/subtitle-event";
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-btl-list',
  templateUrl: './btl-list.component.html',
  providers: [
    LoaderEvent, BtlService, SubtitleEvent
  ]
})
export class BtlListComponent implements OnInit {

  public  btls = [];

  constructor(
    private loader: LoaderEvent,
    private subtitle: SubtitleEvent,
    private btlService: BtlService
  ) { }

  ngOnInit() {
    this.subtitle.fire('');
    this.loader.fire(true);

    this.btlService.getAll()
      .subscribe( response => {
        this.btls = response.result;
        this.loader.fire(false);
      })
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
