import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {ElementsService} from "../../services/elements.service";
import {LoaderEvent} from "../../services/loader-event";
import {SessionService} from "../../services/session.service";
import {TopService} from "../../services/top.service";
import {OrganizationService} from "../../services/organization.service";

@Component({
  selector: 'app-top-report',
  templateUrl: './top-report.component.html',
  providers: [
    ElementsService, LoaderEvent, SessionService, TopService, OrganizationService
  ]
})
export class TopReportComponent implements OnInit {

  public user;
  public message;
  public elementClasses = [];
  public categories = [];
  public top = [];
  public total = [];
  public showTop = false;
  public title = '';
  public cliente = false;
  public search = {
    unit: '',
    from: moment(),
    to: moment(),
    organization: ''
  };

  public topPie = {
    view: [700, 300],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    data: [ ]
  };

  public clientPie = {
    view: [700, 300],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    data: [ ]
  };

  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'}
  };

  public organizations = [];
  public organization = '';

  constructor(
    private elementService: ElementsService,
    private loader: LoaderEvent,
    private session: SessionService,
    private topService: TopService,
    private organizationService: OrganizationService,
  ) { }

  ngOnInit() {

    this.user = this.session.getObject('user');
    this.categories = this.user.permissions.categories;

    if(this.user.role == 'Admin'){
      this.getOrganizations();
    }else{
      this.organization = this.user.permissions.organization;
    }
  }

  getOrganizations(){
    this.loader.fire(true);
    this.organizationService.getAll()
      .subscribe( response => {
        this.loader.fire(false);
        this.organizations = response.result;
      });
  }

  onSearch(){
    this.message = '';

    if(this.search.unit == '') {
      this.message = 'Debes seleccionar la unidad';
    } else {


      let by = this.search.unit;
      // console.log(this.search);
      this.search.organization = this.organization;

      this.loader.fire(true);
      this.topService.getTop(by, this.search)
        .subscribe( response => {
          // console.log(response);
          if (by == 'clients') {
            this.cliente = true;
            this.total = response.result;
            this.top = [];
            let total = 0;
            let data = [];
            let clientD = [];

            for (let i = 0; i < this.total.length; i++) {
               total += this.total[i].elements;      
            }
            var tempClient = {elements:0, campaigns:0, percent:'', name:'Otros'};
            for (let i = 0; i < this.total.length; i++) {
              if(i<10){
                this.total[i].percent = ((this.total[i].elements/total)*100).toFixed(2);
                this.top.push(this.total[i]);
                data.push({ name: this.total[i].name, value: this.total[i].elements }); 
                clientD.push({ name: this.total[i].name, value: this.total[i].campaigns });  
              }
              else{
                tempClient.elements+=this.total[i].elements;
                tempClient.campaigns+=this.total[i].campaigns;
              }                      
            }
            if(this.total.length > 10){
              tempClient.percent = ((tempClient.elements/total)*100).toFixed(2);
              this.top.push(tempClient);
              data.push({ name: tempClient.name, value: tempClient.elements });
              clientD.push({ name: tempClient.name, value: tempClient.campaigns });
            }

            this.clientPie.data = clientD;
            this.topPie.data = data;
            
            this.title = 'Clientes';
            this.showTop = true;
            this.loader.fire(false);

          } else {
            this.cliente = false;
            this.top = [];
            this.total = response.result;
            // console.log(this.top);
            if(this.total.length <= 0){
              this.loader.fire(false);
              return;
            }
            
            let total = 0;
            let data = [];

            for (let i = 0; i < this.total.length; i++) {
               total += this.total[i].elements;      
            }
            var tempAll = {elements:0, percent:'', name:'Otros'};
            for (let i = 0; i < this.total.length; i++) {
              if(i<10){
                this.total[i].percent = ((this.total[i].elements/total)*100).toFixed(2);
                this.top.push(this.total[i]);
                data.push({ name: this.total[i].name, value: this.total[i].elements });    
              }
              else{
                tempAll.elements+=this.total[i].elements;
              }                      
            }
            if(this.total.length > 10){
              tempAll.percent = ((tempAll.elements/total)*100).toFixed(2);
              this.top.push(tempAll);
              data.push({ name: tempAll.name, value: tempAll.elements }); 
            }
            this.top[0]['clients'] = [{campaigns:false}];            
            
            // console.log(this.top);

            
            // this.top.forEach( item => {
            //   item.forEach( info => {
            //     data.push({
            //       name: info.name,
            //       value: info.elements
            //     });
            //   });              
            // });

            this.topPie.data = data;


            this.title = '';
            if(this.search.unit == 'chains'){
              this.title = 'Cadenas';
            }else if(this.search.unit == 'stores'){
              this.title = 'Tiendas';
            }else if(this.search.unit == 'clients'){
              this.title = 'Clientes';
            }else if(this.search.unit == 'carga'){
              this.title = 'Elementos de carga';
            }else if(this.search.unit == 'liviano'){
              this.title = 'Elementos Livianos';
            }

            this.showTop = true;
            this.loader.fire(false);
          }
        });
    }
  }

}
