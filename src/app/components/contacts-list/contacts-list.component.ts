import { Component, OnInit, ViewChild } from '@angular/core';
import { SubtitleEvent } from "../../services/subtitle-event";
import { LoaderEvent } from "../../services/loader-event";
import { StoresService } from "../../services/stores.service";
import { Modal } from 'ngx-modal';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  providers: [
    LoaderEvent,
    StoresService,
    SubtitleEvent,
  ]
})
export class ContactsListComponent implements OnInit {

  @ViewChild('elementModal') public elementModal:Modal;

  public contactFilter: any = { name: '' };
  public allStores = [];
  public message;
  public positions;
  public actualStore;
  public creatingcontact: boolean = true;
  public showContacts:boolean = false;
  public contact = {
    store: '',
    position: '',
    firstname: '',
    lastname: '',
    phone: '',
    email: '',
  }

  constructor(
    private subtitleEvent: SubtitleEvent,
    private loader: LoaderEvent,
    private storeS: StoresService,
  ) { }

  ngOnInit() {
  	this.subtitleEvent.fire('');
    this.loadStores();
  }

  loadStores() {
  	this.loader.fire(true);
  	this.storeS.getStores()
  		.subscribe( response => {
        for (let i = 0; i < response.result.length; i++) {
          if (response.result[i].enabled) {
            this.allStores.push(response.result[i]);
          }
        }
        this.storeS.getPositions()
          .subscribe( response => {
            this.positions = response.result;
            this.loader.fire(false);
          });  			
  		});
  }

  onSelect(storedata){
  	this.showContacts = true;
    this.actualStore = storedata;
    this.message = 'No se encontraron contactos';
  }

  showModal() {
    this.creatingcontact = true;
    this.contact = {
      store: this.actualStore._id,
      position: '',
      firstname: '',
      lastname: '',
      phone: '',
      email: ''
    }
    this.elementModal.open();
  }

  updateModal(contacto) {
    this.creatingcontact = false;
    this.contact = {
      store: this.actualStore._id,
      position: contacto.position,
      firstname: contacto.firstname,
      lastname: contacto.lastname,
      phone: contacto.phone,
      email: contacto.email
    }
    this.elementModal.open();
  }

  onDelete(contactdata, index){
    // console.log(index);
    var confirmation = confirm(`¿Está seguro que desea eliminar el contacto ${contactdata.firstname}?`);
    if(confirmation){
      this.loader.fire(true);
      this.storeS.deleteContact(this.actualStore._id, contactdata.position)
      .subscribe( response => {
        this.actualStore.contacts.splice(index, 1);
        this.loader.fire(false);
      });
    }
  }

  sendContact() {
    this.loader.fire(true);
    if (this.creatingcontact) {
      this.storeS.createContact(this.contact)
        .subscribe( response => {
          this.elementModal.close();
          this.loader.fire(false);
        });
    } else {
      this.storeS.updateContact(this.actualStore._id, this.contact)
        .subscribe( response => {
          this.elementModal.close();
          this.loader.fire(false);
        });
    }
    
  }

  hideElementModal() {
    this.elementModal.close();
  }

}
