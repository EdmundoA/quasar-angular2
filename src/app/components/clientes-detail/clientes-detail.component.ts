import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderEvent } from "../../services/loader-event";
import { SessionService } from "../../services/session.service";
import { CampaignsService } from "../../services/campaigns.service";
import { ActivatedRoute } from "@angular/router";
import { AppSettings } from "../../app.settings";
import { Lightbox } from 'angular2-lightbox';
import { HostListener } from '@angular/core';
import * as moment from "moment";
import * as $ from 'jquery';


@Component({
  selector: 'app-clientes-detail',
  templateUrl: './clientes-detail.component.html',
  providers: [
    LoaderEvent, SessionService, CampaignsService
  ]
})
export class ClientesDetailComponent implements OnInit, OnDestroy {

  private sub;
  public detail;
  public params;
  public showXAxisLabel = true;
  public resume;
  public installs;
  public labelsLineChart = [];
  public evidences;
  public showevidence: Boolean = true;
  public showbinnacle: Boolean = false;
  public binnacles;
  public user;
  public _album:any;
  public base = AppSettings.BASE_PATH;
  public pieColors = [
    {
      backgroundColor: [
        '#28e679', '#fe303b'
      ]
    }
  ];
  public pieOptions = {
    responsive: true,
    responsiveAnimationDuration: 0.5,
    maintainAspectRatio: true,
  };
  public installsFilter:any = {
    store: {
      name: '',
    },
    status:{
      install: null,
      inventory: null,
      service: null,
    },
  };
  public lineOptions = {
    showLabelsOnBars:true,
    barLabelFontColor:"gray",
    responsive: true,
    responsiveAnimationDuration: 0.5,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }

  public lineChartData = [{data: [1, 2, 3, 4], label: 'nada'}];
  public lineChartColours:Array<any> = [
    {
      backgroundColor: '#28e679',
      borderColor: '#28e679',
      pointBackgroundColor: '#003082',
      pointBorderColor: 'black',
      pointHoverBackgroundColor: 'black',
      pointHoverBorderColor: 'rgba(148,159,177,1)'
    }
  ];
  public lineChartLabel = [];

  constructor(
    private loader: LoaderEvent,
    private session: SessionService,
    private campaignService: CampaignsService,
    private routeParams: ActivatedRoute,
    private _lightbox: Lightbox,
  ) { }

  ngOnInit() {
    this.sub = this.routeParams.params.subscribe(params => {
      this.params = {
        idCampaign: params['idCampaign'],
        code: params['code'],
        campaign: params['campaign'],
        elementType:  params['elementType']
      };
      this.loader.fire(true);
      this.user = this.session.getObject('user');
      this.campaignService.getCampaignsByClientDetail(this.params.idCampaign, this.params.code)
      .subscribe( response => {
        if(response.status == 200){
          this.loader.fire(false);
          this.detail = response.result;
          if(!this.session.getItem('client_report' && response.result.client._id)){
            this.session.setItem('client_report', response.result.client._id);
            console.log(response.result.client);
          }          
          this.getResume();
        }
      });
    });
  }

  getResume(){
    this.showbinnacle = false;
    this.labelsLineChart = [];
    this.loader.fire(true);
    //resumen
    this.campaignService.getCampaignsByClientResume(
      {
        campaign: this.params.idCampaign,
        code: this.params.code,
      }
    ).subscribe( response => {
      this.resume = response.result;
      this.resume.installs.percent = (this.resume.installs.total > 0 ? this.resume.installs.ok / this.resume.installs.total * 100 : 0).toFixed(2);
      this.resume.service.percent = (this.resume.service.total > 0 ? this.resume.service.ok / this.resume.service.total * 100 : 0).toFixed(2);

      this.resume.installs.percentnok = (this.resume.installs.total > 0 ? this.resume.installs.nok / this.resume.installs.total * 100 : 0).toFixed(2);
      this.resume.service.percentnok = (this.resume.service.total > 0 ? this.resume.service.nok / this.resume.service.total * 100 : 0).toFixed(2);

      // console.log(this.lineChartLabel);
      this.resume.graphic.sort(this.dateFilter);
      let data = [];
      if (this.labelsLineChart.length == 0) {
        this.resume.graphic.forEach( item => {
          // console.log(item);
          if(item._id){
            data.push(item.count);
            this.labelsLineChart.push(item._id);
          }
        });
      }
      if(this.resume.graphic[0]){
        data.push(0);
      }
      else if(this.resume.graphic[0] && this.resume.graphic[0].count < 10) {
        data.push(10);
      }
      this.lineChartData = [
        {data: data , label: 'Instalaciones por día'}
      ];
      this.lineChartColours = [{
        backgroundColor: '#B3C1D9',
        borderColor: '#264E94',
        pointBackgroundColor: '#003082',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }];
      this.loader.fire(false);
    })
  }

  dateFilter(a, b) {
    var date1 = new Date(a._id);
    var date2 = new Date(b._id)
    if(date1 < date2) return -1;
    else if (date2 < date1) return 1;
    return 0;
  }

  redText(status){
    // console.log('status', status);
    return status!='INSTALADO';
  }

  open(objeto:any){
    this._album = [];

      const d = moment(objeto.date).format("YYYY-MM-DD");
      const src = this.base + objeto.picture;
      const caption = objeto.message + '-' + d;
      this._album.push({
        src: src,
        caption: caption,
      });

    this._lightbox.open(this._album, 0);
  }

  showBinna(evidence) {
    this.loader.fire(true);
    this.showevidence = false;

    // console.log(evidence);
    this.campaignService.getBinnacle({
        campaign: this.params.idCampaign,
        // element: this.params.idElement,
        // elementType: this.params.elementType,
        store: evidence.store._id,
        code: evidence._id.code
      }
    ).subscribe( response => {
      // console.log(response);
      this.binnacles = response.result;
      this.showbinnacle = true;
      this.loader.fire(false);
    });

  }

  goBack() {
    this.showbinnacle = false;
    this.showevidence = true;
  }

  getEvidence(){
    this.showevidence = true;
    this.loader.fire(true);
    this.campaignService.getEvidence({
        campaign: this.params.idCampaign,
        code: this.params.code,
      }
    ).subscribe( response => {
       console.log(response);
      this.evidences = response.result.sort(this.status);
      this.loader.fire(false);
    });
  }

  getInstalls(){
    this.showbinnacle = false;
    this.loader.fire(true);
    this.campaignService.getCampaignsByClientInstalls(
      {
        campaign: this.params.idCampaign,
        code: this.params.code,
      }
    ).subscribe( response => {

      this.installs = response.result;
      for (var i = this.installs.length - 1; i >= 0; i--) {
        this.installs[i].status.install = this.installs[i].status.install == 'OK'?this.installs[i].status.install + ' ':this.installs[i].status.install;
        this.installs[i].status.service = this.installs[i].status.service == 'OK'?this.installs[i].status.service + ' ':this.installs[i].status.service;
      }
      this.installs.sort(this.byInAndStatus);
      this.loader.fire(false);
    });
  }

  byInAndStatus(a, b){
    
    if ((a.status.install === 'NOK') != (b.status.installd === 'NOK')) {
        return a.status.install === 'NOK' ? 1 : -1;
    }

    const date1 = new Date(a.real.install)
    const date2 = new Date(b.real.install)

    if (date1 > date2) return 1;
    return -1;

  }

  status(a, b) {
    if(a.installed.status == 'INSTALADO') {
      if (a.installed.date > b.installed.date) {
        return 1;
      } else {
        return -1;
      }
    }
    else if (a.installed.status == 'CANCELADA') return 1;
    else if (a.installed.status == 'EN PROCESO') return 1;
    else if (a.installed.status == 'PENDIENTE') return 1;
    return 0;
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone2");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone2');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone2").css({visibility:'hidden'});
            $("#clone2 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone2").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

  ngOnDestroy(){
    
  }
}
