import { Component, OnInit } from '@angular/core';
import { ChainsService } from '../../services/chains.service';
import { Router } from "@angular/router";
import { SubtitleEvent} from "../../services/subtitle-event";
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-chains-list',
  templateUrl: './chains-list.component.html',
  providers: [
    LoaderEvent,
    SessionService,
  ]
})
export class ChainsListComponent implements OnInit {

	public chains:Array<String> = [];
  public userFilter: any = { name: '' };
  public values = '';
  public permissions:any;

  constructor(
  		private serviceChain: ChainsService,
  		private subtitleEvent: SubtitleEvent,
      private loader: LoaderEvent,
      private session: SessionService,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.subtitleEvent.fire('');
  	this.loadChain();
    this.permissions = this.session.getObject('permissions');
  }

  loadChain() {
    this.loader.fire(true);
  	this.serviceChain.getChains()
    .subscribe( response => {
      this.loader.fire(false);
    	this.chains = response.result;
    });
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.serviceChain.delete(data._id)
      .subscribe(
        response => {
          // console.log(response);
          this.ngOnInit();
        }
      );
    }
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
