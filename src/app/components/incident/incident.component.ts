import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";
import { ChainsService } from "../../services/chains.service";
import { ServiceLevelService } from "../../services/service-level.service";
import { LoaderEvent } from "../../services/loader-event";
import { IncidentServiceService } from "../../services/incident-service.service";
import { StoresService } from "../../services/stores.service";
import { OrganizationService } from "../../services/organization.service";
import { UtilService } from '../../services/util.service';
import { AppSettings } from "../../app.settings";
import { SessionService } from "../../services/session.service";
import * as moment from 'moment';
import { Modal } from 'ngx-modal';
import { KeysPipe } from "../../objects.pipe";
import {saveAs} from 'file-saver';
//import * as XLSX from 'ts-xlsx';
declare var XLSX:any;

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  providers: [
  	SubtitleEvent,
  	IncidentServiceService,
  	StoresService,
  	OrganizationService,
    UtilService,
  ],
  

})

export class IncidentComponent implements OnInit {

  constructor(
  		private subtitleEvent: SubtitleEvent,
  		private router: Router,
  		private chainS: ChainsService,
  		private loader: LoaderEvent,
  		private inciS: IncidentServiceService,
      private session: SessionService,
  		private storeS: StoresService,
  		private orgaS: OrganizationService,
      private utilService: UtilService,
  	) { }
  public wb:any = {
    SheetNames:[],
    Sheets:{},
  };

  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
  };

  public inci = {
    name: '',
  };
  public params = {
    status: null,
    date: moment(),

  };

  public URL = AppSettings.BASE_PATH;
  public message2;
  public userFilter: any = { name: '', store: [{ name: '' }], element: [{ name: '' }] };
  public chainselected;
  public showResolve: boolean = false;
  public details: boolean = false;
  public totalstores = 0;
  public message;
  public totalelementoscantidad = 0;
  public totalmotivos = 0;
  public totalsubmotivos = 0;
  public cantidadelemento = 0;
  public filters: boolean = false;
  public chains;
  public tiendas = {};
  public incidencias = {};
  public submotives = {};
  public showincidence: boolean = false;
  public stores;
  public organizations;
  public elementos = {};
  public search = {
  		orga: '',
	    chain: '',
	    store: '',
	    from: moment(),
	    to: moment(),
	};

  @ViewChild('editInci') public editInci: Modal;

  public totalTiendas = 0;

  public resTiendas = [];
  public insidence = [];
  public sortable = [];
  public sortableInci = [];
  public sortableEle = [];
  public sortableSub = [];
  public showManager;

  ngOnInit() {
  	this.loader.fire(true);
    let user = this.session.getObject('user');
    let role = user.role;
  	this.orgaS.getAll()
		.subscribe( response => {
			this.organizations = response.result;
      if(role == 'Admin'){
        this.showManager = true;
      }else{
        this.showManager = false;   
        if(role == 'Gerente' || role == 'Supervisor' || role == 'Jefe de categoria'){
          this.organizations = this.organizations.filter( item => item._id == user.permissions.organization);
          if(this.organizations.length == 1){
            this.search.orga = user.permissions.organization;
            this.showChain();
          }
        }
      }    
			this.loader.fire(false);
  	});
  }

  showAll() {
    this.loader.fire(true);
    this.inciS.listInsi(this.search)
    .subscribe( response => {
      for (var i = 0; i < response.result.length; i++) {
        if (!response.result[i].name) {
          response.result[i].name = '';              
          response.result[i].element.push({ name: '-'});
          response.result[i].store.push({ name: '-'});
          response.result[i].client.push({ type: '-'});
        }      
      }
      this.showResolve = true;
      this.filters = false;
      this.details = false;
      this.showincidence = false;
      this.insidence = response.result.sort(this.dateFilter);
      this.loader.fire(false);
    });    
  }

  dateFilter(a, b) {
    let date1 = new Date(a.issue.date);
    let date2 = new Date(b.issue.date);
    if(date1 > date2) return -1;
    else if (date2 > date1) return 1;
    return 0;
  }

  showModal(data) {
    this.inci = data;
    if (this.inci['issue'].status == 'PENDIENTE') {
      this.params.status = false;
    } else {      
      this.params.status = true;
    }
    this.params.date = moment(data.issue.date);
    this.editInci.open();
  }

  deleteIncident(data, index) {
    data.isquasar = (data.isquasar == 'true')?true:false;
    var confirmation = confirm(`¿Está seguro que desea eliminar esta incidencia`);
    if (confirmation) {
      this.inciS.deleteInsi({
        campaign: data.isquasar?data._id:null,
        isquasar: data.isquasar,
        id: data.isquasar?data.issue._id:data._id
      }).subscribe(response => {
        this.insidence.splice(index, 1);
      });
    }
  }

  putInsi() {
    if (this.params.status) {
      this.inci['issue'].status = 'FINALIZADO'
    } else {
      this.inci['issue'].status = 'PENDIENTE'
    }
    this.inciS.updateInsi(this.inci['issue']._id, {
      campaignid: this.inci['_id'],
      elementid: this.inci['element'][0]._id,
      storeid: this.inci['store'][0]._id,
      dateResolution: this.params.date,
      status: this.inci['issue'].status
    }).subscribe( response => {
      this.editInci.close();
    });
  }

  showChain() {
    this.totalmotivos = 0;
    this.totalsubmotivos = 0;
    this.cantidadelemento = 0;
    this.totalelementoscantidad = 0;
    this.loader.fire(true);
    //Clear tables
    this.sortableInci = [];
    this.sortableSub = [];
    this.sortableEle = [];
    this.sortable = [];
    this.submotives = {};
    this.incidencias = {};
    this.elementos = {}; 
    this.tiendas = {};
    this.stores = [];
    this.search.chain = '';
    this.search.store = '';
    
    // if (this.search.orga == '') {
    //   this.onSearch();
    // } else {
    this.totalTiendas = 0;
    this.loader.fire(true);
    this.chainS.getChainsByOrganization({organization: this.search.orga})
    .subscribe( response => {
      // this.sortable = [];
      this.chains = response.result;
      
      this.sortable = []; 
      for (var i = 0; i < this.resTiendas.length; i++) {
        if((this.resTiendas[i].store.length != 0 ) && ((this.resTiendas[i].organization && this.resTiendas[i].organization._id == this.search.orga) || this.search.orga == '')){
          if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
            this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
          }
          if(this.tiendas[this.resTiendas[i].store[0].name + '']){
            this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
            this.totalTiendas++;
          }
          if (!this.incidencias[this.resTiendas[i].motive + '']) {
            this.incidencias[this.resTiendas[i].motive + ''] = [];
          }
          if (this.incidencias[this.resTiendas[i].motive + '']) {
            this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
          }
          if (!this.submotives[this.resTiendas[i].submotive + '']) {
            this.submotives[this.resTiendas[i].submotive + ''] = [];
          }
          if (this.submotives[this.resTiendas[i].submotive + '']) {
            this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);
          }
          if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
            this.elementos[this.resTiendas[i].element[0].name + ''] = [];
          }
          if (this.elementos[this.resTiendas[i].element[0].name + '']) {
            this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
          }
        }
      }

      for (var sub in this.submotives) {
        this.sortableSub.push({submotive:sub, detail:this.submotives[sub]});
        this.totalsubmotivos += this.submotives[sub].length;
      }

      for (var ele in this.elementos) {
        this.sortableEle.push({element:ele, detail:this.elementos[ele]});
        this.totalelementoscantidad += this.elementos[ele].length;
      }

      for (var inci in this.incidencias) {
        this.sortableInci.push({incidencia:inci, detail:this.incidencias[inci]});
        this.totalmotivos += this.incidencias[inci].length;
      }

      for(var store in this.tiendas){
        this.sortable.push({store:store, detail:this.tiendas[store]});
      }

      this.sortableSub.sort(this.sortArray);
      this.sortableSub.reverse();

      this.sortableEle.sort(this.sortArray);
      this.sortableEle.reverse();

      this.sortableInci.sort(this.sortArray);
      this.sortableInci.reverse();

      this.sortable.sort(this.sortArray);
      this.sortable.reverse();

      this.details = true;

      this.loader.fire(false);
    });
    // }
  }

  showStore() {
    this.totalmotivos = 0;
    this.totalsubmotivos = 0;
    this.cantidadelemento = 0;
    this.totalelementoscantidad = 0;
    this.loader.fire(true);
    //Clear tables
    this.sortableInci = [];
    this.sortableSub = [];
    this.sortableEle = [];
    this.sortable = [];
    this.submotives = {};
    this.incidencias = {};
    this.elementos = {}; 
    this.tiendas = {};
    this.stores = [];
    this.search.store = '';

    if (this.search.chain == '') {
      this.onSearch();
    } else {
      this.totalTiendas = 0;
      this.loader.fire(true);
      this.storeS.getStoresById(this.search.chain)
      .subscribe( response => {          
        this.stores = response.result;
        this.sortable = [];          
        this.tiendas = {};
        for (var i = 0; i < this.resTiendas.length; i++) {
          // if(this.resTiendas[i].store.length != 0){
          //   if (this.resTiendas[i].chain && this.resTiendas[i].chain._id == this.search.chain) {
          //     if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
          //       this.elementos[this.resTiendas[i].element[0].name + ''] = [];
          //     }
          //     if (this.elementos[this.resTiendas[i].element[0].name + '']) {
          //       this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
          //     }
          //     if (!this.incidencias[this.resTiendas[i].motive + '']) {
          //       this.incidencias[this.resTiendas[i].motive + ''] = [];
          //     }
          //     if (this.incidencias[this.resTiendas[i].motive + '']) {
          //       this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
          //     }
          //     if (!this.submotives[this.resTiendas[i].submotive + '']) {
          //       this.submotives[this.resTiendas[i].submotive + ''] = [];       
          //     }
          //     if (this.submotives[this.resTiendas[i].submotive + '']) {
          //       this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);            
          //     }
          //     if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
          //       this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
          //     }
          //     if(this.tiendas[this.resTiendas[i].store[0].name + '']){
          //       this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
          //       this.totalTiendas++;
          //     }
          //   }
          // }

          if(this.resTiendas[i].chain && this.resTiendas[i].chain._id == this.search.chain){
            if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
              this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
            }
            if(this.tiendas[this.resTiendas[i].store[0].name + '']){
              this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
              this.totalTiendas++;
            }
            if (!this.incidencias[this.resTiendas[i].motive + '']) {
              this.incidencias[this.resTiendas[i].motive + ''] = [];
            }
            if (this.incidencias[this.resTiendas[i].motive + '']) {
              this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
            }
            if (!this.submotives[this.resTiendas[i].submotive + '']) {
              this.submotives[this.resTiendas[i].submotive + ''] = [];
            }
            if (this.submotives[this.resTiendas[i].submotive + '']) {
              this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);
            }
            if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
              this.elementos[this.resTiendas[i].element[0].name + ''] = [];
            }
            if (this.elementos[this.resTiendas[i].element[0].name + '']) {
              this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
            }
          }
        }

        for (var sub in this.submotives) {
          this.sortableSub.push({submotive:sub, detail:this.submotives[sub]});
          this.totalsubmotivos += this.submotives[sub].length;
        }

        for (var ele in this.elementos) {
          this.sortableEle.push({element:ele, detail:this.elementos[ele]});
          this.totalelementoscantidad += this.elementos[ele].length;
        }

        for (var inci in this.incidencias) {
          this.sortableInci.push({incidencia:inci, detail:this.incidencias[inci]});
          this.totalmotivos += this.incidencias[inci].length;
        }

        for(var store in this.tiendas){
          this.sortable.push({store:store, detail:this.tiendas[store]});
        }

        this.sortableSub.sort(this.sortArray);
        this.sortableSub.reverse();

        this.sortableEle.sort(this.sortArray);
        this.sortableEle.reverse();

        this.sortableInci.sort(this.sortArray);
        this.sortableInci.reverse();

        this.sortable.sort(this.sortArray);
        this.sortable.reverse();

        this.details = true;
        this.loader.fire(false);

      });
    }
  }

  showTables() {
    this.totalmotivos = 0;
    this.totalsubmotivos = 0;
    this.cantidadelemento = 0;
    this.totalelementoscantidad = 0;
    this.loader.fire(true);
    //Clear tables
    this.sortableInci = [];
    this.sortableSub = [];
    this.sortableEle = [];
    this.sortable = [];
    this.submotives = {};
    this.incidencias = {};
    this.elementos = {}; 
    this.tiendas = {};
    for (var i = 0; i < this.resTiendas.length; i++) {
      // for (let o = 0; o < this.resTiendas[i].store.length; o++) {//if (this.resTiendas[i].chain && this.resTiendas[i].chain._id == this.search.chain) {//this.resTiendas[i].store[o]._id == this.search.store
      //   if (((this.resTiendas[i].chain && this.resTiendas[i].chain._id == this.search.chain) || this.search.chain == '') && (this.resTiendas[i].store[o]._id == this.search.store || this.search.store == '')) {
      //     if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
      //       this.elementos[this.resTiendas[i].element[0].name + ''] = [];
      //     }
      //     if (this.elementos[this.resTiendas[i].element[0].name + '']) {
      //       this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
      //     }
      //     if (!this.incidencias[this.resTiendas[i].motive + '']) {
      //       this.incidencias[this.resTiendas[i].motive + ''] = [];
      //     }
      //     if (this.incidencias[this.resTiendas[i].motive + '']) {
      //       this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
      //     }
      //     if (!this.submotives[this.resTiendas[i].submotive + '']) {
      //       this.submotives[this.resTiendas[i].submotive + ''] = [];
      //     }
      //     if (this.submotives[this.resTiendas[i].submotive + '']) {
      //       this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);
      //     }
      //     if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
      //       this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
      //     }
      //     if(this.tiendas[this.resTiendas[i].store[0].name + '']){
      //       this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
      //       this.totalTiendas++;
      //     }
      //   }
      // }

      if(this.resTiendas[i].store.length != 0 && ((this.resTiendas[i].chain && this.resTiendas[i].chain._id == this.search.chain) || this.search.chain == '') && (this.resTiendas[i].store[0]._id == this.search.store || this.search.store == '')){
        if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
          this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
        }
        if(this.tiendas[this.resTiendas[i].store[0].name + '']){
          this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
          this.totalTiendas++;
        }
        if (!this.incidencias[this.resTiendas[i].motive + '']) {
          this.incidencias[this.resTiendas[i].motive + ''] = [];
        }
        if (this.incidencias[this.resTiendas[i].motive + '']) {
          this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
        }
        if (!this.submotives[this.resTiendas[i].submotive + '']) {
          this.submotives[this.resTiendas[i].submotive + ''] = [];
        }
        if (this.submotives[this.resTiendas[i].submotive + '']) {
          this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);
        }
        if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
          this.elementos[this.resTiendas[i].element[0].name + ''] = [];
        }
        if (this.elementos[this.resTiendas[i].element[0].name + '']) {
          this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
        }
      }
    }

    for (var sub in this.submotives) {
      this.sortableSub.push({submotive:sub, detail:this.submotives[sub]});
      this.totalsubmotivos += this.submotives[sub].length;
    }

    for (var ele in this.elementos) {
      this.sortableEle.push({element:ele, detail:this.elementos[ele]});
      this.totalelementoscantidad += this.elementos[ele].length;
    }

    for (var inci in this.incidencias) {
      this.sortableInci.push({incidencia:inci, detail:this.incidencias[inci]});
      this.totalmotivos += this.incidencias[inci].length;
    }

    for(var store in this.tiendas){
      this.sortable.push({store:store, detail:this.tiendas[store]});
    }

    this.sortableSub.sort(this.sortArray);
    this.sortableSub.reverse();

    this.sortableEle.sort(this.sortArray);
    this.sortableEle.reverse();

    this.sortableInci.sort(this.sortArray);
    this.sortableInci.reverse();

    this.sortable.sort(this.sortArray);
    this.sortable.reverse();

    this.details = true;
    this.loader.fire(false);
  }

  hideeditInci() {
    this.editInci.close();
  }

  onSearch() {
    this.loader.fire(true);
  	this.inciS.sendInsi(this.search)
		.subscribe( response => {
      this.search.orga = '';
      if(this.organizations.length == 1){
        this.search.orga = this.organizations[0]._id;
        //this.showChain();
      }      
      this.search.chain = '';
      this.search.store = '';
      this.resTiendas = response.result;
      this.totalTiendas = 0;
      this.totalmotivos = 0;
      this.totalsubmotivos = 0;
      this.cantidadelemento = 0;
      this.totalelementoscantidad = 0;
      //Clear tables
      this.sortableInci = [];
      this.sortableSub = [];
      this.sortableEle = [];
      this.sortable = [];
      this.submotives = {};
      this.incidencias = {};
      this.elementos = {}; 
      this.tiendas = {};


      for (var i = 0; i < this.resTiendas.length; i++) {
        if(this.resTiendas[i].store.length != 0){
          if(!this.tiendas[this.resTiendas[i].store[0].name + '']){
            this.tiendas[this.resTiendas[i].store[0].name + ''] = [];
          }
          if(this.tiendas[this.resTiendas[i].store[0].name + '']){
            this.tiendas[this.resTiendas[i].store[0].name + ''].push(this.resTiendas[i]);
            this.totalTiendas++;
          }
          if (!this.incidencias[this.resTiendas[i].motive + '']) {
            this.incidencias[this.resTiendas[i].motive + ''] = [];
          }
          if (this.incidencias[this.resTiendas[i].motive + '']) {
            this.incidencias[this.resTiendas[i].motive + ''].push(this.resTiendas[i]);
          }
          if (!this.submotives[this.resTiendas[i].submotive + '']) {
            this.submotives[this.resTiendas[i].submotive + ''] = [];
          }
          if (this.submotives[this.resTiendas[i].submotive + '']) {
            this.submotives[this.resTiendas[i].submotive + ''].push(this.resTiendas[i]);
          }
          if (!this.elementos[this.resTiendas[i].element[0].name + '']) {
            this.elementos[this.resTiendas[i].element[0].name + ''] = [];
          }
          if (this.elementos[this.resTiendas[i].element[0].name + '']) {
            this.elementos[this.resTiendas[i].element[0].name + ''].push(this.resTiendas[i]);
          }
        }
      }        

      for(var store in this.tiendas){
        this.sortable.push({store:store, detail:this.tiendas[store]});
      }

      for (var inci in this.incidencias) {
        this.sortableInci.push({incidencia:inci, detail:this.incidencias[inci]});
        this.totalmotivos += this.incidencias[inci].length;
      }

      for (var sub in this.submotives) {
        this.sortableSub.push({submotive:sub, detail:this.submotives[sub]});
        this.totalsubmotivos += this.submotives[sub].length;
      }

      for (var ele in this.elementos) {
        this.sortableEle.push({element:ele, detail:this.elementos[ele]});
        this.totalelementoscantidad += this.elementos[ele].length;
      }

      this.sortable.sort(this.sortArray);
      this.sortable.reverse();

      this.sortableInci.sort(this.sortArray);
      this.sortableInci.reverse();

      this.sortableSub.sort(this.sortArray);
      this.sortableSub.reverse();

      this.sortableEle.sort(this.sortArray);
      this.sortableEle.reverse();

      this.filters = true;
      this.details = true;

      this.showincidence = true;
      this.showResolve = false;
      this.loader.fire(false);
		});
  }

  sortArray(a, b){
    var x = a.detail.length;
    var y = b.detail.length;
    if (x < y) {return -1;}
    if (x > y) {return 1;}
    return 0;
  }

  createWS(name){
   this.wb.SheetNames.push(name);
   this.wb.Sheets[name] = {};
   this.wb.cellFormula = true;

    return this.wb.Sheets[name];
  }

  exportReport(){
    this.loader.fire(true);
    if(!this.resTiendas  || this.resTiendas.length == 0){
      this.loader.fire(false);
      return;
    }

    var dataExcel = JSON.parse(JSON.stringify(this.resTiendas));
    var fila = 1;
    var col = 0;
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };
    var ws = this.createWS('Report');
    var wscols = [
      {wch:25},
      {wch:30},
      {wch:20},
      {wch:25},
      {wch:25},
      {wch:25},
      {wch:25},
      {wch:40},
      {wch:20},
      {wch:25},
      {wch:45},
      {wch:45},
      {wch:45},
      {wch:45},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:45},
      {wch:45},
      {wch:30},
      {wch:120},
    ];
    ws['!merges'] = [];

    let headers = [  'FECHA', 'AÑO', 'MES', 'OPERADOR', 'SUPERVISOR', 'CADENA', 'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGION', 'COD. ELEMENTO', 'CLIENTE', 'MARCA', 'CAMPAÑA', 'ELEMENTO', 'TIṔO ELEMENTO', 'CANTIDAD', 'IN', 'OUT', 'IN 2', 'OUT 2', 'STATUS INSTALACIÓN', 'MOTIVO', 'SUBMOTIVO', 'STATUS FINAL', 'LINK FOTO'];

    for (var k = 0; k < headers.length; k++) {
      ws[this.utilService.getLetter(k) + fila] = {t:'s', v:headers[k], s:AppSettings.fillCabecera};
    }
        
    fila++;

    for (var i = 0; i < dataExcel.length; i++) {
      if(dataExcel[i].date){
        let dateArray = dataExcel[i].date.split('-');
        let year = dateArray[0];
        // To use month in spanish
        moment.locale('es');
        let month = moment().month(dateArray[1] - 1).format('MMMM');
        let supervisor =  this.getDataProperty(dataExcel[i], 'operator.permissions.supervisor.firstname') + ' ' + this.getDataProperty(dataExcel[i], 'operator.permissions.supervisor.lastname');
        let codeElement = this.getDataProperty(dataExcel[i], 'elementCode');
        let operator =  this.getDataProperty(dataExcel[i], 'operator.firstname') + ' ' + 
                        this.getDataProperty(dataExcel[i], 'operator.lastname');
        var quantity = this.getDataProperty(dataExcel[i], 'quantity');
        var campaignS = this.getDataProperty(dataExcel[i], 'campaign');
        var elementType = this.getDataProperty(dataExcel[i], 'elementType');

        let inDate = this.getDataProperty(dataExcel[i], 'inDate').split('T')[0];
        let outDate = this.getDataProperty(dataExcel[i], 'outDate').split('T')[0];
        let in2Date = this.getDataProperty(dataExcel[i], 'in2Date').split('T')[0];
        let out2Date = this.getDataProperty(dataExcel[i], 'out2Date').split('T')[0];
        let city = this.getDataProperty(dataExcel[i], 'store[0].city.name');

        let data = [
          this.getDataProperty(dataExcel[i], 'date').split('T')[0],
          year,
          month,
          operator != ' '?operator:'Operador no definido',
          supervisor != ' '?supervisor:'Sin supervisor',
          this.getDataProperty(dataExcel[i], 'chain.name'),
          this.getDataProperty(dataExcel[i], 'store[0].sapCode'),
          this.getDataProperty(dataExcel[i], 'store[0].name'),
          city?city:this.getDataProperty(dataExcel[i], 'store[0].city2.name'),
          this.getDataProperty(dataExcel[i], 'store[0].region'),
          codeElement?codeElement:'Elemento no quasar',
          this.getDataProperty(dataExcel[i], 'client[0].name'),
          this.getDataProperty(dataExcel[i], 'brand'),
          campaignS?campaignS:'-',
          this.getDataProperty(dataExcel[i], 'element[0].name'),
          elementType?elementType:'-',
          quantity?quantity:1,
          inDate?inDate:'-',
          outDate?outDate:'-',
          in2Date?in2Date:'-',
          out2Date?outDate:'-',
          this.getDataProperty(dataExcel[i], 'status'),
          this.getDataProperty(dataExcel[i], 'motive'),
          this.getDataProperty(dataExcel[i], 'submotive'),
          this.getDataProperty(dataExcel[i], 'target'),
          (dataExcel[i].picture) ? '=HIPERVINCULO("' + AppSettings.BASE_PATH + dataExcel[i].picture + '";"link")' + '\n' : ''
        ];

        for (var k = 0; k < data.length; k++) {
          ws[this.utilService.getLetter(k) + fila] = {t:'s', v:data[k], s:AppSettings.fillClient};
        }

        fila++;
      }
    }

    ws["!ref"] = "A1:AZ" + (fila+1);
    ws['!cols'] = wscols;

    var wopts:any = { bookType:'xlsx', bookSST:false, type:'binary', Props:{cellStyles:true, cellFormula:true}};

    var wbout = XLSX.write(this.wb,wopts);

    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([this.utilService.s2ab(wbout)],{type:"application/octet-stream"}), "reporte_incidentes.xlsx");
    this.loader.fire(false);
  }

  /**
   *  To figure out if an object have all properties passed
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties) {
    // Get all properties to analize
    const allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {

      let propertyRaw = allProperties[i];
      const indexOpen = allProperties[i].indexOf('[');
      const indexClose = allProperties[i].indexOf(']');

      if (indexOpen != -1) {
        propertyRaw = propertyRaw.substr(0, indexOpen);
      }

      if (!obj || !obj.hasOwnProperty(propertyRaw)) {
        return '';
      }
      if (indexOpen != -1) {
        const indexArray = parseInt(allProperties[i].substr(indexOpen + 1, indexClose - 1));
        obj = obj[propertyRaw][indexArray];
      } else {
        obj = obj[allProperties[i]];
      }
 
    }
    return obj;
  }


}
