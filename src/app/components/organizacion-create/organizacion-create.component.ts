import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {OrganizationService} from "../../services/organization.service";
import {LoaderEvent} from "../../services/loader-event";
import {ActivatedRoute, Router} from "@angular/router";
import {OrganizationEntity} from "../../entities/organization.entity";
import {SubtitleEvent} from "../../services/subtitle-event";
import { ChainsService } from '../../services/chains.service';


@Component({
  selector: 'app-organizacion-create',
  templateUrl: './organizacion-create.component.html',
  providers:[
    FormBuilder, 
    OrganizationService, 
    LoaderEvent, 
    SubtitleEvent
  ]
})
export class OrganizacionCreateComponent implements OnInit {

  public organizationForm: FormGroup;
  public message: String = '';
  public sub: any;
  public organization: OrganizationEntity;
  public creating: Boolean = true;
  

  // goCadena(event){
  //   console.log("go");
  // }

  constructor(
    private organizationService: OrganizationService,
    private _fb: FormBuilder,
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private serviceChain: ChainsService,

  ) { }

  ngOnInit() {
    this.organizationForm = this._fb.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
    });

    this.sub = this.routeParams.params.subscribe(
      params => {

        if (params['id']) {
          this.loader.fire(true);
          const id = params['id'];
          this.organizationService.getOrg(id)
            .subscribe(response => {
              if (response.status === 200) {
                this.organization = response.result;
                // console.log(this.organization);
                this.creating = false;
                this.refreshOrg();
              }else {

              }
            });
          ;
          this.subtitleEvent.fire('Editar');
        }else{
          this.loader.fire(false);
          this.subtitleEvent.fire('Crear');
        }
      }
    );
  }

  

  refreshOrg(){
    this.organizationForm.patchValue(this.organization);
    this.loader.fire(false);
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.serviceChain.delete(data._id)
      .subscribe(
        response => {
          // console.log(response);
          this.ngOnInit();
        }
      );
    }
  }

  onSubmit(model: OrganizationEntity, isValid: Boolean) {
    if (isValid) {
      this.loader.fire(true);
      if (this.creating) {
        this.organizationService.create(model)
          .subscribe(
            response => {
              this.router.navigate(['organizations']);
              this.loader.fire(false);
            }
          )
      }
      else {
        this.organizationService.update(this.organization._id, model)
          .subscribe(
            response => {
              this.router.navigate(['organizations']);
              this.loader.fire(false);
            }
          )

      }
    }
  }

}
