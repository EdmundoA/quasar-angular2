import { Component, OnInit } from '@angular/core';
import { OrganizationService } from "../../services/organization.service";
import { OrganizationEntity } from "../../entities/organization.entity";
import { Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";
import { UserEvent } from "../../services/user-event";
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-organizations-list',
  templateUrl: './organizations-list.component.html',
  providers: [
  	OrganizationService, LoaderEvent, UserEvent, SessionService,
  ]
})
export class OrganizationsListComponent implements OnInit {
  public userFilter: any = { name: '' };

	public organizations: Array<OrganizationEntity>;
  public permissions:any;
  constructor(
  		private organizationService: OrganizationService,
  		private subtitleEvent: SubtitleEvent,
      private loader: LoaderEvent,
      private userEvent: UserEvent,
      private session: SessionService,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.subtitleEvent.fire('');
  	this.loadOrganzations();
    this.permissions = this.session.getObject('permissions');
  }

  loadOrganzations() {
  	this.organizationService.getAll()
      .subscribe( response => {
        if(response.status === 200){
          this.organizations = response.result;
        }else{

        }
        this.loader.fire(false);
      })
  }

  onDelete(organization: OrganizationEntity){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${organization.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.organizationService.delete(organization._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          this.ngOnInit();
        }
      );
    }
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
