import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderEvent } from '../../services/loader-event';
import { SubtitleEvent } from "../../services/subtitle-event";
import { PromotionsService } from "../../services/promotions.service";
import { ClientService } from "../../services/clients.service";
import { AppSettings } from '../../app.settings';
import * as moment from "moment";

@Component({
  selector: 'app-promotions-create',
  templateUrl: './promotions-create.component.html',
  providers: [
  	LoaderEvent,
  	PromotionsService,
  	SubtitleEvent,
  ]
})
export class PromotionsCreateComponent implements OnInit {
  public url_base: String = AppSettings.BASE_PATH;
	private promoimage: String = "";
  public sub:any;
  public type:String = "";
  public nameType:String = "";
  public clients = [];
  public clientid = '';
  public month = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	public datePickerConfig = {
	    closeOnSelect: true,
	    disableKeypress: true,
	    firstDayOfWeek: 'mo',
	    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
	};

  public message = '';

	public promo = {
		name: '',
		file: '',
    role: '',
    client: '',
		start: moment(),
		end: moment(),
    image: ''
	}

  public creating: Boolean = false;

  public idpromo;

  constructor(
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private promoS: PromotionsService,
    private clientS: ClientService,
  ) { }

  ngOnInit() {
    this.sub = this.routeParams.params.subscribe(
      params => {
        if (params['id']) {
          this.loader.fire(true);
          this.idpromo = params['id'];
          this.promoS.promotionDetail(this.idpromo)
           .subscribe( response=> {
             // console.log(response.result);
             this.promo = response.result;
             this.promo.end = moment(this.promo.end);
             this.promo.start = moment(this.promo.start);
             this.type = this.promo.role;
             this.clientid = this.promo.client;
             this.showType(this.type);
             this.loader.fire(false);
           });
           this.subtitleEvent.fire('Editar');
        } else {
          this.subtitleEvent.fire('Crear');
          this.creating = true;
        }
      });  	
  }

  formatDateChange(date){
    var partDate = date.split('-');
    var nDate = partDate[0] + ' ' + this.month[parseInt(partDate[1])] + ' ' + partDate[2];
    return nDate;
  }

  close(){
    this.message = '';
  }

  onSubmit(data) {
    if(!this.promoimage || !this.type || !this.promo.name){
      this.message = 'Falta rellenar datos';
      return;
    }
    if (this.clientid == '') {
      this.clientid = null;
    }
  	if (this.creating) {
        this.promoS.sendPromo( {
        name: this.promo.name,
        file: this.promoimage,
        start: this.promo.start,
        end: this.promo.end,
        role: this.type,
        client: this.clientid
      }).subscribe( response => {
        // console.log(response);
        this.router.navigate(['promotions']);
        this.close();
      });
      // console.log(data);
    } else {
      this.promoS.updatePromo(this.idpromo, {
        name: this.promo.name,
        file: this.promoimage,
        start: this.promo.start,
        end: this.promo.end,
        role: this.type,
        client: this.clientid
      }).subscribe( response => {
        // console.log(response);
        this.router.navigate(['promotions']);
        this.close();
      });
    }
  }

  showType(data) {
    if (data == 'Cliente') {
      this.nameType = 'Cliente: ';
      this.clientS.getClients()
        .subscribe( response => {
          this.clients = response.result;
        });
    }
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.promoimage = btoa(binaryString);
  }

}
