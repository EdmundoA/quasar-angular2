import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { UserService } from '../../services/user.service';
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { UserEvent } from "../../services/user-event";
import { AppSettings } from '../../app.settings';
import { HostListener } from '@angular/core';
import { Quantity } from "../../services/quantity.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-user-list',
  templateUrl: './users-list.component.html',
  providers: [
    SessionService,
    UserService,
    LoaderEvent,
    SubtitleEvent,
  ]
})
export class UsersListComponent implements OnInit {

  public users: Array<String> = [];
  public userFilter: any = { firstname: '', lastname: '', role: '' };
  public message: String;
  public permissions:any;

  constructor(
    private session: SessionService,
    private userService: UserService,
    private loader: LoaderEvent,
    private router: Router,
    private subtitleEvent: SubtitleEvent,
    private userEvent: UserEvent,
    private quantity: Quantity,
  ) { }

  ngOnInit() {
    this.subtitleEvent.fire('');
    this.loadList();
    this.permissions = this.session.getObject('permissions');
    this.quantity.fire(true);
    this.registerBroadcast();
  }

  onDelete(data) {
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.firstname}?`);
    if(confirmation){
      this.userService.delete(data._id)
      .subscribe(response => {
        this.loadList();
      });
    }
  }

  loadList() {
    this.loader.fire(true);
    this.userService.getUsers()
    .subscribe(response => {
      this.loader.fire(false);
      this.users = response.result;
    },
    error => {
      this.loader.fire(false);
      if (error.status === 403) {
        this.message = AppSettings.ERROR_403;
      }
    });
  }

  registerBroadcast() {
    this.userEvent.on()
    .subscribe(value => {
      this.permissions = value;
    });
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }
}
