import { Component, OnInit } from '@angular/core';
import { LoaderEvent } from "../../services/loader-event";
import { ElementsService } from "../../services/elements.service";
import { SubtitleEvent } from "../../services/subtitle-event";
import { SessionService } from "../../services/session.service";
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-elements-list',
  templateUrl: './elements-list.component.html',
  providers: [
    LoaderEvent, ElementsService, SessionService,
  ]

})
export class ElementsListComponent implements OnInit {

  public elements: Array<any> = [];
  public message: any;
  public userFilter: any = { name: '', class: '' };
  public permissions:any;

  constructor(
    private loader: LoaderEvent,
    private element: ElementsService,
    private subtitleEvent: SubtitleEvent,
    private session: SessionService,
  ) { }

  ngOnInit() {
    this.subtitleEvent.fire('');
    this.loader.fire(true);
    this.permissions = this.session.getObject('permissions');
    this.element.getAll()
    .subscribe(response => {
      if(response.status === 200){
        console.log(response);
        this.loader.fire(false);
        this.elements = response.result.sort(this.compare);
      }
    });
  }

  hide(data){
    this.loader.fire(true);
    this.element.nothidden(data._id)
    .subscribe( response => {
      // console.log(response.result);
      this.loader.fire(false);
      this.ngOnInit();
    });
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.element.delete(data._id)
      .subscribe(response => {
        this.loader.fire(false);
        this.ngOnInit();
      });
    }
  }

  compare(a, b){
    var textA = a.name.toUpperCase();
    var textB = b.name.toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
