import { Component, OnInit } from '@angular/core';
import { LoaderEvent } from '../../services/loader-event';
import { OpportunitiesService } from '../../services/opportunities.service';
import { UtilService } from '../../services/util.service';
import { AppSettings } from '../../app.settings';
import * as moment from 'moment';
import { saveAs } from 'file-saver';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

declare var XLSX: any;

@Component({
  selector: 'app-opportunities',
  templateUrl: './opportunities.component.html',
  providers: [
    LoaderEvent,
    OpportunitiesService,
    UtilService
  ]
})
export class OpportunitiesComponent implements OnInit {

  public URL = AppSettings.BASE_PATH;
  public userFilter: any = { store: {
    name: ''
  } };
  public type: any = {
    type: '',
  };
  public Oppo = [];
  public src;
  // To define stuff excel
  // Variable for sheet definition
  public wb: any = {
    SheetNames: [],
    Sheets: {},
  };

  constructor(
    private OppoS: OpportunitiesService,
    private loader: LoaderEvent,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    this.loader.fire(true);
    this.OppoS.getOppo()
      .subscribe( response => {

        this.Oppo = response.result.sort(this.compare);
        for (let i = 0; i < this.Oppo.length; i++) {
          this.Oppo[i].picturelow = this.Oppo[i].picture.split('/');
          this.Oppo[i].picturelow[1] = this.Oppo[i].picturelow[1] + '/low';
          this.Oppo[i].picturelow.toString();
          this.Oppo[i].picturelow = this.Oppo[i].picturelow.toString().replace(/,/g , '/');
        }

        // console.log(new Date(this.Oppo[0].created) > new Date());
        this.loader.fire(false);
      });
  }

  onDelete(data) {

    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.store.name}?`);

    if (confirmation) {
      this.OppoS.deleteOppo(data._id)
      .subscribe(
        response => {
          this.ngOnInit();
        }
      );
    }

  }

  compare(a, b){

    let date1 = new Date(a.created);
    let date2 = new Date(b.created);
    if (date1 < date2) return 1;
    else if (date2 < date1) return -1;
    return 0;

  }

  /**
   *  To create a worksheet, note is a sheet not a excel
   *
   *  @param {string} name: name sheet excel
   */

  createWS(name){

    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;

    return this.wb.Sheets[name];
  }

  /**
   *  Funcion when button Report is clicked, It calls to get all information to generate the excel
   */

  exportReport() {
    this.OppoS.getDataExcel()
      .subscribe(data => {
        this.generateReport(data);
      });
  }
  
  /**
   *  To generate the excel with the data passed
   *
   *  @param {array} dataExcel: all data to print in excel
   */

  generateReport(dataExcel) {

    // Clean the last excel
    this.wb = {

      SheetNames: [],
      Sheets: {},

    };

    let ws = this.createWS('Report');
    let wscols = [];
    const columsWidth = 35;
    const rowOffset = 2;

    //  Header column
    const headers = ['FECHA', 'AÑO', 'MES', 'OPERADOR', 'SUPERVISOR', 'CADENA', 'CODIGO SAP',
                     'TIENDA', 'CIUDAD', 'REGIÓN', 'MOTIVO', 'OBSERVACIONES', 'LINK FOTO'];

    // Configuring 12 width cells with same size, -1 for picture link
    for (var i = 0; i < headers.length - 2; i++) {
      wscols.push({ wch: columsWidth });
    }
    //  Widht for picture link and observations
    wscols.push({ wch: 90 });
    wscols.push({ wch: 90 });

    // Drawing headers
    headers.forEach((header, index) => {
      ws[this.utilService.getLetter(index) + rowOffset] = {
                                                            t: 's',
                                                            v: header,
                                                            s: AppSettings.fillCabecera
                                                          };
    });

    // To use month in spanish
    moment.locale('es');

    // Getting main data and drawing in excel
    dataExcel.result.forEach((comercial_oportunity, index) => {

      const dateArray = this.getDataProperty(comercial_oportunity, 'date').split('-');
      const year = dateArray[0];
      const month = moment().month(dateArray[1] - 1).format('MMMM');

      const data = [
          this.getDataProperty(comercial_oportunity, 'date').split('T')[0],
          year,
          month,
          this.getDataProperty(comercial_oportunity, 'operatorFn') + ' ' +
          this.getDataProperty(comercial_oportunity, 'operatorLn'),
          this.getDataProperty(comercial_oportunity, 'supervisorFn') + ' ' +
          this.getDataProperty(comercial_oportunity, 'supervisorLn'),
          this.getDataProperty(comercial_oportunity, 'chain'),
          this.getDataProperty(comercial_oportunity, 'sapCode'),
          this.getDataProperty(comercial_oportunity, 'store'),
          this.getDataProperty(comercial_oportunity, 'city'),
          this.getDataProperty(comercial_oportunity, 'region'),
          this.getDataProperty(comercial_oportunity, 'motive'),
          this.getDataProperty(comercial_oportunity, 'observation'),
          (comercial_oportunity.picture) ? 
          '=HIPERVINCULO("' + 'http://quasareport.pe:3036/'+ 
          comercial_oportunity.picture + '";"link")' + '\n' : ''
        ];

      // Drawing main data in excel, only 1 row
      data.forEach((colum, columIndex) => {
        ws[this.utilService.getLetter(columIndex) + (index + rowOffset + 1)] = {
                                                                        t: 's',
                                                                        v: colum,
                                                                        s: AppSettings.fillClient
                                                                      };
      });

    });


    // Final configurations to draw excel

    // The total size is length data + offset + 1(for the header)
    ws['!ref'] = 'A1:N' + (dataExcel.result.length + rowOffset + 1);
    ws['!cols'] = wscols;

    const wopts: any = {
                        bookType: 'xlsx',
                        bookSST: false,
                        type : 'binary',
                        Props : { cellStyles : true, cellFormula : true }
                      };

    const wbout = XLSX.write(this.wb, wopts);
    saveAs(new Blob([ this.utilService.s2ab(wbout) ], { type : 'application/octet-stream' }),
           'reporte_oportunidades_comerciales.xlsx');

  }

  /**
   *  To figure out if an object have all properties passed
   *  IMPORTANT, this function is just to validate if the data exist
   *  and it not has a influence in the logic
   *
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    const allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }

    // New validation case the final value is null
    return obj ? obj : '';
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone2");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone2');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone2").css({visibility:'hidden'});
            $("#clone2 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone2").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
