import { Component, OnInit } from '@angular/core';
import { LoaderEvent } from '../../services/loader-event';
import { AppSettings } from '../../app.settings';
import { MaintenanceService } from '../../services/maintenance.service';
import { UtilService } from '../../services/util.service';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';
import * as moment from 'moment';
import { saveAs } from 'file-saver';

declare var XLSX: any;

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  providers: [
    LoaderEvent,
    MaintenanceService,
    UtilService
  ]
})
export class MaintenanceComponent implements OnInit {
	public URL = AppSettings.BASE_PATH;

	public userFilter: any = { name: '', store: '', elementType: '',  };

	public maintenance = [];


  // To define stuff excel
  // Variable for sheet definition
  public wb: any = {
    SheetNames: [],
    Sheets: {},
  };

  constructor(
      private mainS: MaintenanceService,
      private loader: LoaderEvent,
      private utilService: UtilService
  	) { }

  ngOnInit() {
  	this.loader.fire(true);
  	this.mainS.getMain()
		.subscribe( response => {
			// console.log(response);
			this.maintenance = response.result.sort(this.compare);
			this.loader.fire(false);
		});
  }

  compare(a, b){
    let date1 = new Date(a.maintenance.date);
    let date2 = new Date(b.maintenance.date);
    if(date1 > date2) return -1;
    else if (date2 > date1) return 1;
    return 0;
  }

  /**
   *  To create a worksheet, note is a sheet not a excel
   *
   *  @param {string} name: name sheet excel
   */

  createWS(name){
    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;
    return this.wb.Sheets[name];
  }

  /**
   *  Funcion when button Report is clicked, It calls to get all information to generate the excel
   */

  exportReport() {
    this.mainS.getDataExcel()
    .subscribe(data => {
      this.generateReport(data);
    });
  }
  
  /**
   *  To generate the excel with the data passed
   *
   *  @param {array} dataExcel: all data to print in excel
   */

  generateReport(dataExcel) {

    // Clean the last excel
    this.wb = {

      SheetNames: [],
      Sheets: {},

    };

    let ws = this.createWS('Report');
    let wscols = [];
    const columsWidth = 35;
    const rowOffset = 2;

    //  Header column
    const headers = ['FECHA', 'AÑO', 'MES', 'OPERADOR', 'SUPERVISOR', 'IMPLEMENTADOR', 'COD. ELEMENTO', 'CLASIFICACION ELEMENTO', 'ELEMENTO', 'TIPO ELEMENTO', 'CLIENTE', 'CAMPAÑA', 'CADENA', 'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGIÓN', 'IN', 'OUT', 'IN 2', 'OUT 2', 'OBSERVACIONES', 'LINK FOTO ANTES', 'LINK FORO DESPUES'];

    // Configuring 12 width cells with same size, -1 for picture link
    for (var i = 0; i < headers.length - 2; i++) {
      wscols.push({ wch: columsWidth });
    }
    //  Widht for pictures link
    wscols.push({ wch: 180 });
    wscols.push({ wch: 180 });

    // Drawing headers
    headers.forEach((header, index) => {
      ws[this.utilService.getLetter(index) + rowOffset] = {
        t: 's',
        v: header,
        s: AppSettings.fillCabecera
      };
    });

    // To use month in spanish
    moment.locale('es');

    // Getting main data and drawing in excel
    dataExcel.result.forEach((maintenance, index) => {

      const dateArray = this.getDataProperty(maintenance, 'date').split('-');
      const year = dateArray[0];
      const month = moment().month(dateArray[1] - 1).format('MMMM');
      const comment = this.getDataProperty(maintenance, 'comment');
      const data = [
          this.getDataProperty(maintenance, 'date').split('T')[0],
          year,
          month,
          this.getDataProperty(maintenance, 'operatorFn') + ' ' +
          this.getDataProperty(maintenance, 'operatorLn'),
          this.getDataProperty(maintenance, 'supervisorFn') + ' ' +
          this.getDataProperty(maintenance, 'supervisorLn'),
          this.getDataProperty(maintenance, 'implementer'),
          this.getDataProperty(maintenance, 'elementcode'),
          this.getDataProperty(maintenance, 'elementclass'),
          this.getDataProperty(maintenance, 'element'),
          this.getDataProperty(maintenance, 'elementType'),
          this.getDataProperty(maintenance, 'client'),
          this.getDataProperty(maintenance, 'name'),
          this.getDataProperty(maintenance, 'chain'),
          this.getDataProperty(maintenance, 'sapcode'),
          this.getDataProperty(maintenance, 'store'),
          this.getDataProperty(maintenance, 'city'),
          this.getDataProperty(maintenance, 'region'),
          this.getDataProperty(maintenance, 'in1').split('T')[0],
          this.getDataProperty(maintenance, 'out1').split('T')[0],
          this.getDataProperty(maintenance, 'in2').split('T')[0],
          this.getDataProperty(maintenance, 'out2').split('T')[0],
          comment?comment:'-',
          (maintenance.pictureBefore) ? 
          '=HIPERVINCULO("' + AppSettings.BASE_PATH + 
          maintenance.pictureBefore + '";"link")' + '\n' : '',
          (maintenance.pictureAfter) ?
          '=HIPERVINCULO("' + AppSettings.BASE_PATH + 
          maintenance.pictureAfter + '";"link")' + '\n' : ''
        ];

      // Drawing main data in excel, only 1 row
      data.forEach((colum, columIndex) => {
        ws[this.utilService.getLetter(columIndex) + (index + rowOffset + 1)] = {
          t: 's',
          v: colum,
          s: AppSettings.fillClient
        };
      });

    });


    // Final configurations to draw excel

    // The total size is length data + offset + 1(for the header)
    ws['!ref'] = 'A1:Z' + (dataExcel.result.length + rowOffset + 1);
    ws['!cols'] = wscols;

    const wopts: any = {
                        bookType: 'xlsx',
                        bookSST: false,
                        type : 'binary',
                        Props : { cellStyles : true, cellFormula : true }
                      };

    const wbout = XLSX.write(this.wb, wopts);
    saveAs(new Blob([ this.utilService.s2ab(wbout) ], { type : 'application/octet-stream' }),
           'reporte_mantenimiento.xlsx');

  }

  /**
   *  To figure out if an object have all properties passed
   *  IMPORTANT, this function is just to validate if the data exist
   *  and it not has a influence in the logic
   *
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    const allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }

    // New validation case the final value is null
    return obj ? obj : '';
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone2");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone2');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone2").css({visibility:'hidden'});
            $("#clone2 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone2").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
