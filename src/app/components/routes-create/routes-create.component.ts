import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { OrganizationService } from "../../services/organization.service";
import { LoaderEvent } from "../../services/loader-event";
import { RoutesService } from '../../services/routes.service';
import { ActivatedRoute, Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";
import { StoresService } from "../../services/stores.service";
import { UserService } from "../../services/user.service";
import { RoutesEntity } from "../../entities/routes.entity";
import { Modal } from 'ngx-modal';

@Component({
  selector: 'app-routes-create',
  templateUrl: './routes-create.component.html',
  providers:[
   LoaderEvent, 
   SubtitleEvent,
   RoutesService,
   StoresService,
   UserService,
  ]
})
export class RoutesCreateComponent implements OnInit {


    @ViewChild('lgModal') public lgModal:Modal;
    @ViewChild('lgModal2') public lgModal2:Modal;
	  public message : any;
    public message2 : any;
    public tableimple: boolean = false;
    public isModalShown:boolean = false;
    public isModalShown2:boolean = false;
    public indexStore;
    public storeedit = false;
  	public sub: any;
    public userimple: Array<String> = [];
  	public creating: Boolean = true;
    public showDays: Boolean = false;
  	public id_client: String;
    public showTableDays: Boolean = false;
    public showSaveRoute: Boolean = false;
  	public route: any = {
      code: '',
      stores: [],
      implementer: [],
    };
    // I created another similar to not crash other functionalities
    public routeAlone: any = {
      code: ''
    };

    public imple: any = {
      _id: "",
    }

    public stores: any = {
      store: '',
      days: {
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false,
        sunday: false,
      }
    }

    public organization = '';

  	public allStores: any = [];
    public allOrganizations: any = [];
    public sameAllStores:any = [];
    public ruta ={
      organization: '',
    }

  constructor(
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private routesService: RoutesService,
    private storesService: StoresService,
    private user: UserService,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.sub = this.routeParams.params
    .subscribe(params => {
      if (params['id']) {
        this.showSaveRoute = false;
      	this.id_client = params['id'];
        const id = params['id'];
        this.routesService.detail(id)
          .subscribe(response => {
            if (response.status === 200) {
               console.log(response);
              this.creating = false;
              this.showTableDays = true;
              this.tableimple = true;
              this.route = response.result;
              // console.log(this.route);
              for (var i = 0; i < this.route['stores'].length; i++) {
                this.route['stores'][i].name = this.route['stores'][i].store.name;
                this.route['stores'][i].store = this.route['stores'][i].store._id;
              }

              for (var j = 0; j < this.route['implementer'].length; j++) {
                this.route['implementer'][j].name = this.route['implementer'][j].firstname + ' ' + this.route['implementer'][j].lastname;
              }
            }else {

            }
          });
        this.subtitleEvent.fire('Editar');
      }else{
        this.showSaveRoute = true;
        this.subtitleEvent.fire('Crear');
      }
    });
    this.getStores();
  }

  getStores() {
    this.storesService.getStores()
    .subscribe(response => {
      for (let i = 0; i < response.result.length; i++) {
        if (response.result[i].enabled) {
          this.sameAllStores.push(response.result[i]);
          var orga = this.searchOrganization(response.result[i].reference.organization._id);
          if(!orga){
            orga = JSON.parse(JSON.stringify(response.result[i].reference.organization)); 
            orga.stores = [response.result[i]];
            this.allOrganizations.push(orga);
          }
          else{
            orga.stores.push(response.result[i]);
          }
        }
      }
      console.log(this.allOrganizations);
      this.user.getUsers()
      .subscribe(response => {
        // console.log(response);
        for (var i = 0; i < response.result.length; i++) {
          if (response.result[i].role == "Operador") {
            this.userimple.push(response.result[i]);
          }
        }
        this.loader.fire(false);
      }); 
    });
  }

  changeOrganization($event){
    if(!$event || $event == ''){
      this.allStores = [];
      return;
    }
    this.allStores = this.allOrganizations.filter(item => item._id == $event)[0].stores;
  }

  searchOrganization(idOrga){
    for (var i = this.allOrganizations.length - 1; i >= 0; i--) {
      if(this.allOrganizations[i]._id == idOrga){
        return this.allOrganizations[i];
      }
    }
    return false;
  }

  onDelete(data, index) {
    let confirmation = confirm(`¿Está seguro que desea eliminar Tienda: ${data.name}?`);
    if(confirmation){
      this.route.stores.splice(index, 1);
    }    
  }

  deleteOpe(operator, index) {
    let confirmation = confirm(`¿Está seguro que desea eliminar al operador ${operator.firstname}?`);
    if(confirmation){
      this.route.implementer.splice(index, 1);
    }
  }

  onEdit(data, index) {
    this.loader.fire(true);
    var tOrga = this.sameAllStores.filter(item => item._id == data.store)[0].reference.organization._id;
    this.organization = this.allOrganizations.filter(item => item._id == tOrga)[0]._id;
    console.log(this.organization);
    this.changeOrganization(this.organization);
    this.indexStore = index;
    this.showDays = true;
    this.message = false;
    this.stores = JSON.parse(JSON.stringify(data));
    this.storeedit = true;
    this.lgModal.open();
    this.loader.fire(false);
  }

  close() {
    this.message2 = false;
  }

  onSubmit() {
    if (this.creating) {
      this.routesService.create(this.route)
      .subscribe(response => {
        if (response.status == 201) {
          this.router.navigate(['routes']);
        }        
      }, 
        error => {
          this.message2 = 'Esta Tienda ya tiene un Implementador asignado';
      });
    } else {
      this.routesService.update(this.id_client, this.route)
      .subscribe(response => {
        this.router.navigate(['routes']);
      });
    }
  }

  saveRoute(){
    this.routeAlone.code = this.route.code;
    if (this.route.code) {
      this.routesService.createAlone(this.routeAlone)
      .subscribe(response => {
        if (response.status == 201) {
          this.router.navigate(['routes']);
        }
      });
    } else {
      this.message2 = 'Ingrese un código';
    }

  }

  onHidden() {
    this.isModalShown = false;
    this.isModalShown2 = false;
  }

  hideModal() {
    this.lgModal.close();
  }

  hideModal2() {
    this.lgModal2.close();
  }

  sendImpleId() {
    if (this.imple['_id'].split(',')[1] == undefined) {
      
    } else {
      this.imple['name'] = this.imple['_id'].split(',')[0];
      this.imple['_id'] = this.imple['_id'].split(',')[1];
      this.route['implementer'].push(this.imple); 
      this.imple = {
        _id: '',
      };
      this.tableimple = true;
    }
  }

  showStores() {
    this.showDays = false;
    this.stores['store'] = '';
    this.message = false;
    this.storeedit = false;
    this.lgModal.open();
  }

  showImple() {
    this.imple['_id'] = '';
    this.message = false;
    this.lgModal2.open();
  }

  sendStore() {
    if(!this.stores.store || this.stores.store == ''){
      return;
    }
    if (this.storeedit) {
      //this.stores['name'] = this.stores['store'].split(',')[0];
      //this.stores['store'] = this.stores['store'].split(',')[1];
      this.route['stores'][this.indexStore] = JSON.parse(JSON.stringify(this.stores));
      this.lgModal.close();
      this.showTableDays = true;
    } else {
      this.stores['name'] = this.allStores.filter(item => item._id == this.stores['store'])[0].name;
      //this.stores['store'] = this.stores['store'].split(',')[1];
      this.route['stores'].push(JSON.parse(JSON.stringify(this.stores)));
      this.showTableDays = true;
    }    
    this.stores = {
      store: '',
      days: {
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false,
        sunday: false,
      }
    }
  }

  changeDays($event) {
    if(!$event || $event == ''){
      this.showDays = false;
      return;
    }
    this.stores['days'] = {
      monday: false,
      tuesday: false,
      wednesday: false,
      thursday: false,
      friday: false,
      saturday: false,
      sunday: false,
    }
    this.showDays = true;
  }

}
