import { Component, OnInit } from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-btl',
  templateUrl: './btl.component.html',
  providers: [
    SubtitleEvent
  ]
})
export class BtlComponent implements OnInit {

  public subtitle: string = '';

  constructor(
    private subtitleEvent: SubtitleEvent
  ) { }

  ngOnInit() {
    this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
