import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";
import { ChainsService } from "../../services/chains.service";
import { SessionService } from "../../services/session.service";
import { ServiceLevelService } from "../../services/service-level.service";
import { LoaderEvent } from "../../services/loader-event";
import * as moment from 'moment';

@Component({
  selector: 'app-level-service-detail',
  templateUrl: './level-service-detail.component.html',
  providers: [
  	SubtitleEvent, LoaderEvent, ServiceLevelService
  ]
})
export class LevelServiceDetailComponent implements OnInit {

	public subtitle: string = '';

	public chains: Array<any> = [];
	public classes = {
		stores: [],
		regions: [],
		client: []
	};
	public firstScreenData: Boolean = true;
	public secondScreenData: Boolean = false;
	public showChart: Boolean = false;
	public message;
	public totalTypeok = 0;
	public totalTypenok = 0;
	public totalTypenotInst = 0;
	public totalTypeTotal = 0;
	public totalTypepercent = 0;

	public totalEleok = 0;
	public totalElenok = 0;
	public totalElenotInst = 0;
	public totalEleTotal = 0;
	public totalElepercent = 0;

	public totalTopClientok = 0;
	public totalTopClientnok = 0;
	public totalTopClientnotInst = 0;
	public totalTopClientTotal = 0;
	public totalTopClientpercent = 0;

	public totalTopStoreok = 0;
	public totalTopStorenok = 0;
	public totalTopStorenotInst = 0;
	public totalTopStoreTotal = 0;
	public totalTopStorepercent = 0;

	public totalTopRegionok = 0;
	public totalTopRegionnok = 0;
	public totalTopRegionnotInst = 0;
	public totalTopRegionTotal = 0;
	public totalTopRegionpercent = 0;

	public typeData;
	public ElementData;
	public allData = {
		ok: 0,
		nok: 0,
		not_installed: 0
	};
  
	public search = {
	    chain: '',
	    class: '',
	    from: moment(),
	    to: moment(),
	};

	public datePickerConfig = {
	    closeOnSelect: true,
	    disableKeypress: true,
	    firstDayOfWeek: 'mo',
	    weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'},
      monthsNames: {jan: 'ene', feb: 'feb'}
	};

	public resumepie = {
	    view: [800, 300],
	    showLegend: true,
	    showLabels: true,
	    explodeSlices: false,
	    doughnut: false,
	    autoScale: true,
	    legendTitle: 'Estado',
	    data: [ ]
	};

	public resumeType = {
	    view: [500, 300],
	    showLegend: true,
	    showLabels: true,
	    explodeSlices: false,
	    doughnut: false,
	    autoScale: true,
	    legendTitle: 'Tiendas',
	    data: [ ]
	};

  public user = {
    permissions: {
      organization: '',
      categories: []
    },
    role: '',
  };
  public organization;

  constructor(
  		private subtitleEvent: SubtitleEvent,
  		private router: Router,
  		private chainS: ChainsService,
  		private loader: LoaderEvent,
  		private sessionLS: ServiceLevelService,
      private session: SessionService,
  	) { }

  ngOnInit() {
    this.user = this.session.getObject('user');
    this.organization = this.user.permissions.organization;
    this.loader.fire(true);
    //obtener cadenas
    if(this.user.role == 'Gerente' || this.user.role == 'Jefe de categoria'){
      this.chainS.getChainsByOrganization({organization: this.organization})
      .subscribe( response => {
        this.chains = response.result;
        this.registerBroadcast();
        this.loader.fire(false);
      });
    }
    else{
      this.chainS.getChains()
      .subscribe( response => {
        // console.log(response);
        this.chains = response.result;
        this.registerBroadcast();
        this.loader.fire(false);
      });
    }    
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });
  }

  onSearch() {
  	this.message = '';
  	this.loader.fire(true);
  	this.sessionLS.getServiceLevel(this.search)
  		.subscribe( response => {
        this.totalTypeok = 0;
        this.totalTypenok = 0;
        this.totalTypenotInst = 0;
        this.totalTypeTotal = 0;

        this.totalEleok = 0;
        this.totalElenok = 0;
        this.totalElenotInst = 0;
        this.totalEleTotal = 0;
        
  			// console.log(response);
  			this.allData = response.result.all;
        this.allData['total'] = this.allData.ok + this.allData.nok + this.allData.not_installed;
  			this.typeData = response.result.type;
  			this.ElementData = response.result.element;
  			// console.log(this.typeData);

  			for (var i = 0; i < this.typeData.length; i++) {
  				this.typeData[i].total = this.typeData[i].ok + this.typeData[i].nok + this.typeData[i].not_installed;
  				this.typeData[i].percent = (this.typeData[i].ok / this.typeData[i].total)*100;
  				this.totalTypeok += this.typeData[i].ok; 
  				this.totalTypenok += this.typeData[i].nok;
  				this.totalTypenotInst += this.typeData[i].not_installed;
  				this.totalTypeTotal += this.typeData[i].total;        
  			}

        this.typeData = this.typeData.sort(this.totalcolum);
  			this.totalTypepercent = (this.totalTypeok / this.totalTypeTotal)*100;

  			for (var h = 0; h < this.ElementData.length; h++) {
  				this.ElementData[h].total = this.ElementData[h].ok + this.ElementData[h].nok + this.ElementData[h].not_installed;
  				this.ElementData[h].percent = (this.ElementData[h].ok / this.ElementData[h].total)*100;
  				this.totalEleok += this.ElementData[h].ok; 
  				this.totalElenok += this.ElementData[h].nok;
  				this.totalElenotInst += this.ElementData[h].not_installed;
  				this.totalEleTotal += this.ElementData[h].total;
  			}

        this.ElementData = this.ElementData.sort(this.totalcolum);

  			this.totalElepercent = (this.totalEleok / this.totalEleTotal)*100;

  			// Grafico de Tip
  			/*let dataType = [];
  			this.typeData.forEach( item => {
  				console.log(item);
  				dataType.push({
  					name: item.type,
  					value: Math.round(item.percent * 100) / 100
  				})
  			});

  			this.resumeType.data = dataType;*/ 

  			this.resumepie.data = [
  			{
  				name: 'OK',
  				value: this.allData.ok,
  			},
  			{
  				name: 'NOK',
  				value: this.allData.nok,
  			},
  			{
  				name: 'No Instalado',
  				value: this.allData.not_installed,
  			},];

  			let chain = this.chains.filter(item => item._id == this.search.chain)[0];			
  			this.showChart = true;
  			this.loader.fire(false);
  		});  	
  }


  sendType(data) {
  	this.loader.fire(true);
  	this.firstScreenData = false;
  	this.search.class = data.type;
  	this.sessionLS.getServiceElement(this.search)
  		.subscribe(response => {
        this.totalTopClientok = 0;
        this.totalTopClientnok = 0;
        this.totalTopClientnotInst = 0;
        this.totalTopClientTotal = 0;

        this.totalTopStoreok = 0;
        this.totalTopStorenok = 0;
        this.totalTopStorenotInst = 0;
        this.totalTopStoreTotal = 0;

        this.totalTopRegionok = 0;
        this.totalTopRegionnok = 0;
        this.totalTopRegionnotInst = 0;
        this.totalTopRegionTotal = 0;
  			this.subtitleEvent.fire(data.type);
  			this.secondScreenData = true;
  			this.loader.fire(false);
  			this.classes = response.result;
        // console.log(this.classes);

  			for (var g = 0; g < this.classes['client'].length; g++) {
  				this.classes['client'][g].total = this.classes['client'][g].ok + this.classes['client'][g].nok + this.classes['client'][g].not_installed;
  				this.classes['client'][g].percent = (this.classes['client'][g].ok / this.classes['client'][g].total) * 100;
  				this.totalTopClientok += this.classes['client'][g].ok;
  				this.totalTopClientnok += this.classes['client'][g].nok;
  				this.totalTopClientnotInst += this.classes['client'][g].not_installed;
  				this.totalTopClientTotal += this.classes['client'][g].total;
  				this.totalTopClientpercent = (this.totalTopClientok / this.totalTopClientTotal) * 100;
  			}

  			for (var k = 0; k < this.classes['stores'].length; k++) {
  				this.classes['stores'][k].total = this.classes['stores'][k].ok + this.classes['stores'][k].nok + this.classes['stores'][k].not_installed;
  				this.classes['stores'][k].percent = (this.classes['stores'][k].ok / this.classes['stores'][k].total) * 100;
  				this.totalTopStoreok += this.classes['stores'][k].ok;
  				this.totalTopStorenok += this.classes['stores'][k].nok;
  				this.totalTopStorenotInst += this.classes['stores'][k].not_installed;
  				this.totalTopStoreTotal += this.classes['stores'][k].total;
  				this.totalTopStorepercent = (this.totalTopStoreok / this.totalTopStoreTotal) * 100;
  			}

  			for (var e = 0; e < this.classes.regions.length; e++) {
  				this.classes.regions[e].total = this.classes.regions[e].ok + this.classes.regions[e].nok + this.classes.regions[e].not_installed;
  				this.classes.regions[e].percent = (this.classes.regions[e].ok / this.classes.regions[e].total) * 100;
  				this.totalTopRegionok += this.classes.regions[e].ok;
  				this.totalTopRegionnok += this.classes.regions[e].nok;
  				this.totalTopRegionnotInst += this.classes.regions[e].not_installed;
  				this.totalTopRegionTotal += this.classes.regions[e].total;
  				this.totalTopRegionpercent = (this.totalTopRegionok / this.totalTopRegionTotal) * 100;
  			}

  			this.classes.stores = this.classes.stores.sort(this.totalcolum);
  			this.classes.client = this.classes.client.sort(this.totalcolum);
  			this.classes.regions = this.classes.regions.sort(this.totalcolum);

  			// console.log(this.classes);
  		});  	
  	
  }

  back() {
  	this.secondScreenData = false;
  	this.subtitleEvent.fire('');
  	this.firstScreenData = true;
  }

  compare(a, b){
  	if(a.percent > b.percent) return -1;
  	else if (b.percent > a.percent) return 1;
  	return 0;
  }

  totalcolum(a, b) {
    if(a.total > b.total) return -1;
    else if (b.total > a.total) return 1;
    return 0;
  }

}
