import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";

@Component({
  selector: 'app-level-service',
  templateUrl: './level-service.component.html',
  providers: [
  	SubtitleEvent
  ],
})
export class LevelServiceComponent implements OnInit {
	
	public subtitle: string = '';

  constructor(
		private subtitleEvent: SubtitleEvent
  	) { }

  ngOnInit() {
  	this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
