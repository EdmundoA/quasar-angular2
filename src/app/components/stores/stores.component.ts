import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent} from "../../services/subtitle-event";
import { UserEvent} from "../../services/user-event";
import { StoresService } from "../../services/stores.service";
import { UtilService } from '../../services/util.service';
import { SessionService } from '../../services/session.service';
import { AppSettings } from "../../app.settings";
import { saveAs } from 'file-saver';

declare var XLSX: any;

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  providers: [
    SubtitleEvent,
    StoresService,
    UtilService,
    UserEvent,
    SessionService,
  ]
})
export class StoresComponent implements OnInit {

  public subtitle: string = '';
  public permissions:any;
  // To define stuff excel
  // Varible for sheet definition
  public wb: any = {
    SheetNames:[],
    Sheets:{},
  };

  constructor(
    private subtitleEvent: SubtitleEvent,
    private storesService: StoresService,
    private utilService: UtilService,
    private userEvent: UserEvent,
    private session: SessionService,
    ) { }

  ngOnInit() {
    this.registerBroadcast();
    this.permissions = this.session.getObject('permissions');
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });

    this.userEvent.on()
    .subscribe(value => {
      this.permissions = value;
    });
  }

  /**
   *  To create a worksheet, note is a sheet not a excel
   *
   *  @param {string} name: name sheet excel
   */

  createWS(name){
    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;
    return this.wb.Sheets[name];
  }

  /**
   *  Funcion when button Report is clicked, It calls to get all information to generate the excel
   */

  exportReport() {
    this.storesService.getDataExcel()
      .subscribe(data => {
        this.generateReport(data)
      });
  }

  /**
   *  To generate the excel with the data passed
   *
   *  @param {array} dataExcel: all data to print in excel
   */

  generateReport(dataExcel) {

    // Clean the last excel
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };

    let ws = this.createWS('Report');
    let wscols = [];
    const columsWidth = 35;
    const rowOffset = 2;

    // Header column
    const headers = ['CADENA', 'CODIGO SAP', 'TIENDA', 'CLUSTER 1', 'FORMATO', 'CIUDAD',
                     'REGIÓN', 'CARGO', 'NOMBRE', 'APELLIDO', 'CELULAR', 'CORREO'];

    // Configuring 12 width cells with same size
    for (var i = 0; i < headers.length; i++) {
      wscols.push({ wch: columsWidth });
    }

    // Drawing headers
    headers.forEach((header, index) => {
      ws[this.utilService.getLetter(index) + rowOffset] = {
                                                            t: 's',
                                                            v: header,
                                                            s: AppSettings.fillCabecera
                                                          };
    });

    // Getting main data and drawing in excel
    dataExcel.result.forEach((store, storeIndex) => {

      let data = [
        this.getDataProperty(store, 'chain'),
        this.getDataProperty(store, 'sapCode'),
        this.getDataProperty(store, 'name'),
        this.getDataProperty(store, 'cluster1'),
        this.getDataProperty(store, 'format'),
        this.getDataProperty(store, 'city'),
        this.getDataProperty(store, 'region'),
        this.getDataProperty(store, 'cargo.position'),
        this.getDataProperty(store, 'cargo.firstname'),
        this.getDataProperty(store, 'cargo.lastname'),
        this.getDataProperty(store, 'cargo.phone'),
        this.getDataProperty(store, 'cargo.email')

      ];

      // Drawing main data in excel, only 1 row
      data.forEach((colum, columIndex) => {
        ws[this.utilService.getLetter(columIndex) + (storeIndex + rowOffset + 1)] = {
                                                                          t: 's',
                                                                          v: colum,
                                                                          s: AppSettings.fillClient
                                                                        };
      });

    });


    // Final configurations to draw excel

    // The total size is length data + offset + 1(for the header)
    ws["!ref"] = "A1:M" + (dataExcel.result.length + rowOffset + 1);
    ws['!cols'] = wscols;

    let wopts: any = {
                        bookType:'xlsx',
                        bookSST:false,
                        type : 'binary',
                        Props : { cellStyles : true, cellFormula : true }
                      };

    let wbout = XLSX.write(this.wb, wopts);
    saveAs(new Blob([ this.utilService.s2ab(wbout) ], { type : "application/octet-stream" }),
           "reporte_tiendas.xlsx");

  }

  /**
   *  To figure out if an object have all properties passed
   *  IMPORTANT, this function is just to validate if the data exist
   *  and it not has a influence in the logic
   *
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    let allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }

    // New validation case the final value is null
    return obj ? obj : '';
  }

}
