import { Component, Directive, OnInit, ViewChild } from '@angular/core';
import { LoaderEvent } from "../../services/loader-event";
import { ActivatedRoute, Router } from "@angular/router";
import { CampaignsService } from '../../services/campaigns.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { ClientService } from "../../services/clients.service";
import { StoresService } from "../../services/stores.service";
import { RouteService } from "../../services/route.service";
import { ElementsService } from "../../services/elements.service";
//import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalContent, Modal } from 'ngx-modal';
import { FileSelectDirective, FileDropDirective, FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { AppSettings } from "../../app.settings";
import { TaskService } from "../../services/task.service";
import { UtilService } from '../../services/util.service';
import { InventoryService } from '../../services/inventories.service';
import { FormGroup, FormBuilder } from "@angular/forms";
//import * as XLSX from 'ts-xlsx';
import * as moment from "moment";
import {saveAs} from 'file-saver';

declare var XLSX:any;
import * as $ from 'jquery';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

type AOA = Array<Array<any> >;

@Component({
  selector: 'app-campaign-create',
  templateUrl: './campaign-create.component.html',
  providers: [
    CampaignsService,
    LoaderEvent,
    SubtitleEvent,
    ClientService,
    StoresService,
    ElementsService,
    TaskService,
    UtilService,
    InventoryService,
    RouteService,
  ],

})
export class CampaignCreateComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({ url: AppSettings.BASE_PATH + AppSettings.CMP_IMG });
  private installbase64textString: String = "";
  private uninstallbase64textString: String = "";
  private confirmatiobase64textString: String = "";
  private measurementbase64textString: String = "";
  private campaignimage: String = "";

  /*@ViewChild('importExcelModal') public importExcelModal: ModalDirective;
  @ViewChild('elementModal') public elementModal: ModalDirective;
  @ViewChild('storeModal') public storeModal: ModalDirective;
  @ViewChild('listStoreModal') public listStoreModal: ModalDirective;
  @ViewChild('InstallModal') public InstallModal: ModalDirective;
  @ViewChild('UninstallModal') public UninstallModal: ModalDirective;
  @ViewChild('EditStoreModal') public EditStoreModal: ModalDirective;
  @ViewChild('ConfirmationModal') public ConfirmationModal: ModalDirective;
  @ViewChild('MeasurementModal') public MeasurementModal: ModalDirective;
  @ViewChild('InventoryModal') public InventoryModal: ModalDirective;*/
  @ViewChild('importExcelModal') public importExcelModal: Modal;
  @ViewChild('elementModal') public elementModal: Modal;
  @ViewChild('storeModal') public storeModal: Modal;
  @ViewChild('listStoreModal') public listStoreModal: Modal;
  @ViewChild('InstallModal') public InstallModal: Modal;
  @ViewChild('UninstallModal') public UninstallModal: Modal;
  @ViewChild('EditStoreModal') public EditStoreModal: Modal;
  @ViewChild('ConfirmationModal') public ConfirmationModal: Modal;
  @ViewChild('MeasurementModal') public MeasurementModal: Modal;
  @ViewChild('InventoryModal') public InventoryModal: Modal;
  @ViewChild('ListIssuesModal') public ListIssuesModal: Modal;
  @ViewChild('ListInventoriesModal') public ListInventoriesModal: Modal;

  public category: Array<any>;
  public storeData;
  public index_Stores;
  public idCampaign: String;
  public listStore: Array<any> = [];
  public url_base: String = AppSettings.BASE_PATH;
  public message2: String;
  public codedit: Boolean = true;
  public indexEle;
  public date2 = moment();
  public dInventary = moment();
  public changeInventory:any;

  public arCategories = [];
  public arMaterials = [];

  public tienda = {
    store: { _id: ''},
    code: '',
    measurements: []
  };

  public field1 : moment.Moment;
  public comment;

  public confirm = {
    reference: '',
    comment: ''
  };
  public wb:any = {
    SheetNames:[],
    Sheets:{},
  };
  public taskMeasu = {
    observation: '',
  }

  public motives:any = [];
  public submotives:any = [];
  public motives_install:any = [];
  public submotives_install:any = [];
  public statusInventory = '0';
  public statusInstall = '0';
  public statusUninstall = '0';
  public dataInstall: any = {
    motive: '',
    submotive: '',
  };
  public dataUninstall: any = {
    motive: 'No autorizado para retiro',
    submotive: 'Vigencia concluida, personal de tienda no autoriza el retiro',
  }
  public dataInventory:any = {
    motive: '',
    submotive: '',
    material: '',
    client: '',
    category: '',
    element: '',
    quantity: '',
    brand: '',
    responsable: '',
  };

  public storeName;

  public installation = moment();
  public uninstallation = moment();

  public editstore: any = {
    store: '',
    code: '',
    location: '',
    needConfirmation: null,
    needMeasurement: null,
    measurements: [],
    programed: {
      install: moment(),
      uninstall: moment(),
    },
    chronological: {
      install: moment(),
      uninstall: moment(),
    },
    installed: {
      picture: '',
    },
    uninstalled: {
      comment: '',
    },
    issues: [],
  }

  public options = {
    format: "DD.MM.YYYY",
    maxDate: ''
  };

  public willShowInstallModal: Boolean = false;

  public operators: Array<any> = [];

  public showBtnAddElement = false;

  public message: String = '';
  public campaign: any = {
    name: '',
    client: '',
    implementer: '',
    brand: '',
    master: '',
    categoryParent: '',
    category: '',
    duration: {
      tstart: '',
      tend: '',
      start: moment(),
      end: moment(),
    },
    stores: [],
  };
  public currentLocationELement;
  public list_clients: Array<any> = [];
  public list_stores: Array<any> = [];
  public list_organization: Array<any> = [];
  public list_elements: Array<any> = [];
  public list_categories = [];
  public sub: any;
  public creating: Boolean = true;

  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
  };

  public datePickerConfig2 = {
    closeOnSelect: true,
    firstDayOfWeek: 'mo',
    format: 'DD-MM-YYYY HH:mm:ss',
    showSeconds: true,
    disableKeypress: true,
    timeSeparator: ':',
    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
  };

  public month = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  public currentElement = {
    update: {
      code: '',
      element: {
        _id: '',
      },
      elementType: '',
      implementer: '',
      location: '',
    },
    original: {
      code: '',
      element: {
        _id: '',
      },
      elementType: '',
      implementer: '',
      location: '',
    }
  };
  public elementTypes = [];

  public currentStores = {
    code: '',
    location: '',
    element: '',
    elementType: '',
    stores: [],
  };

  public excelTiendas:AOA;

  public settingsDate = {
    bigBanner: false,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false,
  };
  public settingsDateTime = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MM-yyyy HH:mm',
    defaultOpen: false,
  }

  public tempElement;
  public issuesInstalls = [];
  public inventories = [];
  public tempPdf = {};
  public pdfElement:any;
  public pdfStores:any;
  public pdfNumStores = 0;
  public pdfTablaStores:any;
  public numCallGetDetail = 0;

  constructor(
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private campaignS: CampaignsService,
    private client: ClientService,
    private storeS: StoresService,
    private elementS: ElementsService,
    private taskS: TaskService,
    private utilService: UtilService,
    private inventoryServices: InventoryService,
    private routeS: RouteService
  ) { }

  ngOnInit() {
    this.loader.fire(true);
    this.sub = this.routeParams.params.subscribe(
      params => {
        if (params['id']) {
          this.idCampaign = params['id'];
          this.getDetail();

        } else {
          this.subtitleEvent.fire('Crear');
          this.showBtnAddElement = false;
          this.loader.fire(false);
          this.getAll();
        }
      }
    );
  }

  execStore(exec, editstore) {
    var title_exec = '';
    if(exec == 'delete_install'){
      title_exec = '¿Está seguro que desea elmininar esta Instalación?'
    }
    else if(exec == 'delete_uninstall'){
      title_exec = '¿Está seguro que desea elmininar esta Desinstalación?'
    }
    else if(exec == 'change_location'){
      return;
      //title_exec = '¿Está seguro que desea elmininar esta Instalación?'
    }
    var confirmation = confirm(`${title_exec}`);
    if (confirmation) {
      this.loader.fire(false);
      editstore['exec'] = exec;
      this.campaignS.execStore(this.idCampaign, editstore)
      .subscribe(response => {
        this.currentStores = {
          code: this.campaign.elements[this.indexEle].code,
          location: this.campaign.elements[this.indexEle].location,
          element: this.campaign.elements[this.indexEle].element,
          elementType: this.campaign.elements[this.indexEle].elementType,
          stores: [],
        };
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
        this.hideEditStoreModal();
      })
      /*this.campaignS.deleteStore({
        campaign: this.idCampaign,
        store: store.store._id,
        code: store.code
      }).subscribe(response => {
        this.listStore.splice(index, 1);
      });*/
    }
  }

  putStore(editstore) {
    this.editstore.measurements = this.campaign.elements[this.indexEle].stores[this.index_Stores].measurements;
    // Update new installed image in case it were changed
    if (this.installbase64textString) {
      this.storeS.updateImageInstalled(
        this.editstore.store,
        {
          file: this.installbase64textString,
          code: this.editstore.code,
          campaign: this.campaign._id
        }
      );
    }
    if(!this.editstore.installed.picture){
      this.editstore.installed.date = null;
    }
    if(!this.editstore.uninstalled.ok){
      this.editstore.uninstalled.date = null;
    }
    this.loader.fire(true);
    this.campaignS.updateStore(this.idCampaign, this.editstore)
    .subscribe(response => {
      console.log(response);
      this.currentStores = {
        code: this.campaign.elements[this.indexEle].code,
        location: this.campaign.elements[this.indexEle].location,
        element: this.campaign.elements[this.indexEle].element,
        elementType: this.campaign.elements[this.indexEle].elementType,
        stores: [],
      };
      this.campaignS.detail(this.idCampaign)
      .subscribe(response => {
        this.campaign.elements = response.result.elements;
        this.getChronicalCampaign();
        this.getElementListStore();
        this.loader.fire(false);
      });
      this.hideEditStoreModal();
    });
  }

  getDetail() {
    this.campaignS.detail(this.idCampaign)
      .subscribe(response => {
        if (response.status === 200 || response.status == 209) {
          this.creating = false;
          this.codedit = false;
          this.campaign = response.result;

          //convertir fechas a Date
          this.campaign.duration.start = moment(this.campaign.duration.start);
          this.campaign.duration.end = moment(this.campaign.duration.end);

          this.getChronicalCampaign();
          this.showBtnAddElement = true;
          this.getAll();

          if(this.numCallGetDetail == 0){
            this.numCallGetDetail++;
            this.routeS.sendRoute({path: '/campaing/' + this.campaign.name + '/' + this.campaign.elements[0].elementType, date: Date.now()})
            .subscribe( response => {
            });
          }
        }
         else {

        }
      });
    this.subtitleEvent.fire('Editar');
  }

  getChronicalCampaign(){
    for (var i = this.campaign.elements.length - 1; i >= 0; i--) {
      var telement = this.campaign.elements[i];
      for (var o = telement.stores.length - 1; o >= 0; o--) {
        if(!telement.stores[o].chronological){
          telement.stores[o].chronological = {
            install: this.campaign.duration.start,
            uninstall: this.campaign.duration.end,
          }
        }
        var countIssuesInstalls = (telement.stores[o].issues.filter(item => item.target && (item.target.toLowerCase() == 'instalación' || item.target.toLowerCase() == 'install' || item.target.toLowerCase() == 'desinstalación'))).length;
        telement.stores[o].count = countIssuesInstalls;
        if(telement.stores[o].installed.status != 'PENDIENTE'){
          telement.stores[o].installed.date = moment(telement.stores[o].installed.date);
        }
        if(telement.stores[o].uninstalled.ok){
          telement.stores[o].uninstalled.date = moment(telement.stores[o].uninstalled.date);
        }
      }
    }
    this.hideConfirmationModal();
    this.hideElementModal();
    this.hideInstallModal();
    this.hideInventoryModal();
    this.hideMeasurementModal();
    this.hideStoreModal();
    this.hideUninstallModal();
    this.hideEditStoreModal();
    this.hideImportExcelModal();
  }

  getElementListStore(){
    if(this.tempElement){
      for (var i = this.campaign.elements.length - 1; i >= 0; i--) {
        if(this.campaign.elements[i]._id == this.tempElement){
          this.showList(this.campaign.elements[i], i);
          return;
        }
      }
    }
  }

  sortAlpha(a ,b){
    if(a.name > b.name) return 1;
    if(b.name > a.name) return -1;
    return 0;
  }

  existOrganization(name){
    for (var i = this.list_organization.length - 1; i >= 0; i--) {
      if(this.list_organization[i].name == name){
        return this.list_organization[i];
      }
    }
    return false;
  }

  getAll() {
    this.loader.fire(true);
    //listado de clientes
    this.client.getClients()
    .subscribe(response => {
      this.list_clients = response.result;
      this.list_clients.sort(this.sortAlpha);
      //listado de tiendas
      this.storeS.groupOrganizations()
      .subscribe(response => {
        this.list_stores = [];
        this.list_organization = [];
        console.log(response.result);
        for (var i = response.result.length - 1; i >= 0; i--) {
          for (var j = response.result[i].stores.length - 1; j >= 0; j--) {
            if(response.result[i].stores[j].enabled){
              this.list_stores.push(response.result[i].stores[j]);
              var organization = this.existOrganization(response.result[i]._id);
              if(organization){
                organization.stores.push(response.result[i].stores[j]);
              }
              else{
                this.list_organization.push({
                  name: response.result[i]._id,
                  stores:[
                    response.result[i].stores[j],
                  ]
                });
              }
            }
          }
        }
        console.log(this.list_organization);
        this.resetStores();
        //listado de elementos
        this.elementS.getAll()
        .subscribe(response => {
          this.list_elements = response.result;
          //listado de categorias
          this.campaignS.getCategories()
          .subscribe(response => {
            this.list_categories = response.result;
            //si esta editando... llenar las categorias
            if (!this.creating && this.campaign.category != '-') {
              for (let i = 0; i < this.list_categories.length; i++) {
                for (let j = 0; j < this.list_categories[i].categories.length; j++) {
                  this.arCategories.push(this.list_categories[i].categories[j].name);
                  for (let k = 0; k < this.list_categories[i].categories[j].subcategories.length; k++) {
                    if (this.list_categories[i].categories[j].subcategories[k] == this.campaign.category) {
                      this.campaign.master = i + 1;
                      this.campaign.categoryParent = j + 1;
                      this.campaign.category = this.list_categories[i].categories[j].subcategories[k];
                      break;
                    }
                  }
                }
              }
            } else {
              this.campaign.master = '';
              this.campaign.categoryParent = '';
              this.campaign.category = '';
            }
            this.loader.fire(false);
            this.getMaterials();
            this.getmotiveAndSubmotive();
          });
          this.campaignS.getImplementers()
            .subscribe(response => {
              this.operators = response.result;
            });
        });
      });
    });
  }

  getMaterials(){
    this.inventoryServices.getMaterials()
    .subscribe(response => {
      this.arMaterials = response.result;
    })
  }

  getmotiveAndSubmotive(){
    this.loader.fire(true);
    this.campaignS.getMotive()
    .subscribe(response => {
      this.motives = response.result;
      this.campaignS.getSubmotive()
      .subscribe(response => {
        this.submotives = response.result;
        this.campaignS.getMotiveInstall()
        .subscribe(response => {
          this.motives_install = response.result;
          this.campaignS.getSubmotiveInstall()
          .subscribe(response => {
            this.submotives_install = response.result;
            this.loader.fire(false);
          });
        });
      });
    }); 
  }

  //resetear valores de las tiendas
  resetStores() {
    for (var i = this.list_organization.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.list_organization[i].stores.length; j++) {
        this.list_organization[i].stores[j].check = false;
      }
    }
  }

  onDelete(data, index, campanias) {
    var confirmation = confirm(`¿Está seguro que desea eliminar el Elemento con código ${data.code}?`);
    if (confirmation) {   
      this.loader.fire(true);   
      this.campaignS.deleteEleever(this.idCampaign, data.code)
        .subscribe(response => {
          data.enabled = !data.enabled;
          this.loader.fire(false);
        })
    }
  }

  onDisabled(data, index, campanias) {
    var confirmation = confirm(`¿Está seguro que desea bloquear el Elemento con código ${data.code}?`);
    if (confirmation) {   
      this.loader.fire(true);   
      this.campaignS.deleteEle(this.idCampaign, data.code)
        .subscribe(response => {
          data.enabled = !data.enabled;
          this.loader.fire(false);
        })
    }
  }

  hide(data, index, campanias) {
    this.loader.fire(true);
    this.campaignS.eleOnHidden(this.idCampaign, data.code)
      .subscribe( response => {
        data.enabled = !data.enabled;
        this.loader.fire(false);
      });
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);

    }
  }

  changeFileInventory(evt, invenotry) {
    var files = evt.target.files;
    var file = files[0];
    this.changeInventory = invenotry;

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderInventory.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderInventory(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.changeInventory.base64 = btoa(binaryString);
  }

  invenotryUpdate(inventory){
    this.loader.fire(true);
    this.taskS.updateInventory({
      file: inventory.base64,
      campaign: this.idCampaign,
      code: this.storeData.code,
      store: this.storeData.store._id,
      idInventory: inventory._id
    })
    .subscribe(response =>{
      inventory.picture = response.picture;
      for (var i = this.inventories.length - 1; i >= 0; i--) {
        if(this.inventories[i]._id == inventory._id){
          console.log('dasdasd');
          var input = $("#file-in-" + i);
          this.inventories[i].base64 = null;
          input.replaceWith(input.val('').clone(true));
          break;
        }
      }
      this.campaignS.detail(this.idCampaign)
      .subscribe(response => {
        this.campaign.elements = response.result.elements;
        this.getChronicalCampaign();
        this.getElementListStore();
        this.loader.fire(false);
      });
    });
  }

  inventoryDelete(inventory){
    var confirmation = confirm(`¿Está seguro que desea eliminar este inventario?`);
    if (confirmation) {
      this.loader.fire(true);
      this.taskS.deleteInventory({
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        idInventory: inventory._id
      })
      .subscribe(response =>{
        inventory.picture = response.picture;
        for (var i = this.inventories.length - 1; i >= 0; i--) {
          if(this.inventories[i]._id == inventory._id){
            this.inventories.splice(i,1);
            break;
          }
        }
        if(this.inventories.length == 0){
          this.ListInventoriesModal.close();
        }
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
  }

  close() {
    this.message = '';
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.installbase64textString = btoa(binaryString);
    this.uninstallbase64textString = btoa(binaryString);
    this.confirmatiobase64textString = btoa(binaryString);
    this.measurementbase64textString = btoa(binaryString);
    this.campaignimage = btoa(binaryString);
  }

  install(inst, comment) {
    if(!this.installbase64textString){
      return;
    }
    this.loader.fire(true);
    if(this.statusInstall == '0'){
      this.taskS.createInstall({
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        file: this.installbase64textString,
        date: inst,
        comment: comment
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
    else{
      this.taskS.createIssue({
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        file: this.installbase64textString,
        motive: this.dataInstall.motive,
        submotive: this.dataInstall.submotive,
        date: inst,
        target: 'Instalación',
        comment: comment
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
  }

  uninstall(uni, comment) {
    this.loader.fire(true);
    if(this.statusUninstall == '0'){
      this.taskS.createUninstall({
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        date: uni,
        comment: comment
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
    else{
      if(!this.installbase64textString){
        this.loader.fire(false);
        return;
      }
      this.taskS.createIssue({
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        file: this.installbase64textString,
        motive: this.dataUninstall.motive,
        submotive: this.dataUninstall.submotive,
        date: uni,
        target: 'Desinstalación',
        comment: comment
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
  }

  taskInventory(date, comment){
    if(!this.installbase64textString){
      return;
    }
    if(this.statusInventory == '0'){
      this.loader.fire(true);
      this.taskS.createInventory({
        implementer: 'Quasar',
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        file: this.installbase64textString,
        date: date,
        comment: comment,
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }  
    else{
      if(((this.dataInventory.material == "" || this.dataInventory.client == "" || this.dataInventory.category == "" || this.dataInventory.element == "" || this.dataInventory.submotive == "" || this.dataInventory.quantity == "" || this.dataInventory.brand == "" || this.dataInventory.responsable == "") && this.dataInventory.motive == "Elemento no instalado por Quasar") || this.dataInventory.submotive == ""){
        return;
      }
      this.loader.fire(true);
      this.taskS.createIssue({
        implementer: 'Quasar',
        campaign: this.idCampaign,
        code: this.storeData.code,
        store: this.storeData.store._id,
        file: this.installbase64textString,
        material: this.dataInventory.material,
        client: this.dataInventory.client,
        category: this.dataInventory.category,
        element: this.dataInventory.element,
        motive: this.dataInventory.motive,
        submotive: this.dataInventory.submotive,
        quantity: this.dataInventory.quantity,
        brand: this.dataInventory.brand,
        responsable: this.dataInventory.responsable,
        date: date,
        target: 'Inventario',
        comment: comment,
      }).subscribe(response => {
        this.campaignS.detail(this.idCampaign)
        .subscribe(response => {
          this.campaign.elements = response.result.elements;
          this.getChronicalCampaign();
          this.getElementListStore();
          this.loader.fire(false);
        });
      });
    }
  }

  confirmation(data, index) {
    this.tienda = data;
    this.storeName = data.store.name;
    this.index_Stores = index;
    this.confirmatiobase64textString = '';
    this.confirm.comment = '';
    this.confirm.reference = '';
    this.ConfirmationModal.open();
  }

  confirmConfirmation(data) {
    this.taskS.sendConfirmation({
      campaign: this.campaign._id,
      code: this.tienda.code,
      store: this.tienda.store._id,
      file: this.confirmatiobase64textString,
      reference: data.reference,
      comment: data.comment
    })
    .subscribe( response => {
      this.campaignS.detail(this.idCampaign)
      .subscribe(response => {
        this.campaign.elements = response.result.elements;
        this.getChronicalCampaign();
        this.getElementListStore();
        this.loader.fire(false);
      });
    });
  }

  measurement(data, index) {
    this.tienda = data;
    this.storeName = data.store.name;
    this.index_Stores = index;
    this.taskMeasu.observation = '';
    for (var i = 0; i < this.tienda.measurements.length; i++) {
      this.tienda.measurements[i].value = '';
      this.tienda.measurements[i].quantity = null;
    }
    this.MeasurementModal.open();
  }

  confirmMeasurement(data, medi) {
    this.taskS.sendMeasurement({
      campaign: this.campaign._id,
      code: this.tienda.code,
      store: this.tienda.store._id,
      file: this.measurementbase64textString,
      observation: data.observation,
      measurements: medi 
    })
    .subscribe( response =>  {
      this.campaignS.detail(this.idCampaign)
      .subscribe(response => {
        this.campaign.elements = response.result.elements;
        this.getChronicalCampaign();
        this.getElementListStore();
        this.loader.fire(false);
      });
    });
  }

  editStore(store, index) {
    this.storeName = store.store.name;
    this.index_Stores = index;

    this.editstore = {
      store: store.store._id,
      code: store.code,
      location: store.location,
      needConfirmation: store.needConfirmation,
      needMeasurement: store.needMeasurement,
      measurements: [],
      programed: {
        install: moment(store.programed.install),
        uninstall: moment(store.programed.uninstall),
      },
      chronological:{
        install: moment(store.chronological && store.chronological.install?store.chronological.install:this.campaign.duration.start),
        uninstall: moment(store.chronological && store.chronological.uninstall?store.chronological.uninstall:this.campaign.duration.end),
      },
      installed: {
        picture: store.installed.picture,
        date: moment(store.installed.date),
        comment: store.installed.comment,
      },
      uninstalled: {
        date: moment(store.uninstalled.date),
        comment: store.uninstalled.comment,
        ok: store.uninstalled.ok,
      },
      issues: store.issues,
    }

    this.EditStoreModal.open();
  }

  listIssues(store, index){
    this.storeName = store.store.name;
    this.issuesInstalls = store.issues.filter(item => item.target && (item.target.toLowerCase() == 'instalación' || item.target.toLowerCase() == 'install' || item.target.toLowerCase() == 'desinstalación'));

    this.ListIssuesModal.open();
  }

  listInventories(store, index){
    this.storeName = store.store.name;
    this.storeData = store;
    this.inventories = store.inventory;
    for (var i = this.inventories.length - 1; i >= 0; i--) {
      this.inventories[i].base64 = null;
    }
    this.ListInventoriesModal.open();
  }

  formatDateChange(date){
    var partDate = date.split('-');
    var nDate = partDate[0] + ' ' + this.month[parseInt(partDate[1])] + ' ' + partDate[2];
    return nDate;
  }

  onSubmit(data) {
    let params = data;
      if (this.creating) {
        if(this.campaignimage == ''){
          this.message = 'Te falta seleccionar una imagen';
          return
        }
        this.loader.fire(true);
        this.campaignS.create(params)
        .subscribe(response => {          
          this.idCampaign = response.result._id;
          this.campaignS.getImage(this.idCampaign, { file: this.campaignimage })
          .subscribe( response => {
            this.loader.fire(false);
            this.router.navigate(['/campaing/' + this.idCampaign + '/edit']);
          });   
        });
      } else {
        this.loader.fire(true);
        this.campaignS.update(this.campaign._id, params)
        .subscribe(response => {
          console.log(response);
          if(response.result.badRoutes>0){
            this.message = "Hay tiendas que no tienen una ruta asignada";
          }
          this.loader.fire(false);
          if(this.campaignimage == ''){
            this.getDetail();
            return;
          }
          this.campaignS.getImage(this.campaign._id, { file: this.campaignimage })
          .subscribe( response => {
            this.getDetail();
          });
        });
      }
    
  }

  onChangeElement() {
    this.elementTypes = this.list_elements.filter(item => item._id == this.currentElement.update.element._id)[0].types;
    this.currentElement.update.elementType = '';
  }

  showElementModal(element = null) {
    if (element != null) {
      //va a editar
      this.codedit = false;
      this.currentElement = {
        update: {
          code: element.code,
          element: element.element,
          elementType: element.elementType,
          implementer: element.implementer,
          location: element.location,
        },
        original: {
          code: element.code,
          element: element.element,
          elementType: element.elementType,
          implementer: element.implementer,
          location: element.location,
        }
      }
      this.elementTypes = this.list_elements.filter(item => item._id == this.currentElement.update.element._id)[0].types;
    } else {
      this.codedit = true;
      //va a crear
      this.currentElement = {
        update: {
          code: '',
          element: {
            _id: '',
          },
          elementType: '',
          implementer: '',
          location: '',
        },
        original: {
          code: '',
          element: {
            _id: '',
          },
          elementType: '',
          implementer: '',
          location: '',
        }
      }
    }
    this.elementModal.open();
  }

  hideElementModal() {
    this.elementModal.close();
  }

  hideStoreModal() {
    this.storeModal.close();
  }

  hideInstallModal() {
    this.InstallModal.close();
  }

  hidelistStoreModal() {
    this.listStoreModal.close();
  }

  hideUninstallModal() {
    this.UninstallModal.close();
  }

  hideInventoryModal() {
    this.InventoryModal.close();
  }

  hideEditStoreModal() {
    this.EditStoreModal.close();
    //this.listStoreModal.show();
  }

  hideConfirmationModal() {
    this.ConfirmationModal.close();
  }

  hideMeasurementModal() {
    this.MeasurementModal.close();
  }

  hideImportExcelModal(){
    this.importExcelModal.close();
  }

  hideListInventoriesModal(){
    this.ListInventoriesModal.close();
  }

  saveElement() {
    this.loader.fire(true);
    if (this.currentElement.original.code == '') {
      //crear
      this.campaignS.addElement(this.idCampaign, this.currentElement.update)
        .subscribe(response => {
          this.loader.fire(false);
          this.elementModal.close();
          this.getDetail();
        });
    } else {
      //actualizar
      this.campaignS.updateEle(this.idCampaign, this.currentElement)
        .subscribe(response => {

          this.loader.fire(false);
          this.elementModal.close();
          this.getDetail();
        });
      // When update all elements also update locations of all stores
      this.campaignS.updateLocationStores(
        this.campaign._id,
        {
          element: this.currentElement.update.element._id,
          location: this.currentElement.update.location
        }
      );
    }
  }

  installRealDate(data) {
    this.storeName = data.store.name;
    this.storeData = data;
    this.InstallModal.open();
  }

  uninstallRealDate(data) {
    this.storeName = data.store.name;
    this.storeData = data;
    this.UninstallModal.open();    
  }

  inventoryRealDate(data){
    this.storeName = data.store.name;
    this.storeData = data;
    this.dataInventory = {
      motive: '',
      submotive: '',
      material: '',
      client: '',
      category: '',
      element: '',
      quantity: '',
      brand: '',
      responsable: '',
    };
    this.statusInventory = '0';
    this.InventoryModal.open();   
  }

  showStoreModal(element, index) {
    this.index_Stores = index;
    this.resetStores();
    this.currentStores = {
      code: element.code,
      element: element.element,
      location: element.location,
      elementType: element.elementType,
      stores: [],
    };
    this.storeModal.open();
  }

  showImportExcelModal(element, index) {
    this.index_Stores = index;
    this.currentStores = {
      code: element.code,
      location: element.location,
      element: element.element,
      elementType: element.elementType,
      stores: [],
    };
    this.excelTiendas = [];

    this.importExcelModal.open();
  }

  openExcel(files){
    if(files.length != 1) throw new Error("Cannot upload multiple files on the entry");
    const scope = this;
    const reader = new FileReader();
    reader.onload = function (e:any) {
      /* read workbook */
      const bstr = e.target.result;
      //const wb = XLSX.read(bstr, {type:'binary'});
      var wb = XLSX.read(bstr, {type:'binary'});
      /* grab first sheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* save data to scope */
      scope.excelTiendas = (<AOA>(XLSX.utils.sheet_to_json(ws, {header:1})));
    };
    reader.readAsBinaryString(files[0]);
  }

  /* Eliminar tienda de listado */
  deletethisStore(store, index) {
    var confirmation = confirm(`¿Está seguro que desea eliminar ${store.store.name}?`);
    if (confirmation) {
      this.campaignS.deleteStore({
        campaign: this.idCampaign,
        store: store.store._id,
        code: store.code
      }).subscribe(response => {
        this.listStore.splice(index, 1);
      });
    }
  }

  showList(element, index) {
    this.tempElement = element._id;
    this.indexEle = index;
    this.listStore = [];
    for (var i = 0; i < element.stores.length; i++) {
      this.listStore.push(element.stores[i]);
    }
    this.currentLocationELement = element.location;
    this.listStore.sort(this.sortAlphabet);
    this.listStoreModal.open();
  }

  saveStores() {
    this.currentStores.stores = [];
    let list_index = 1;
    this.list_organization.forEach(organization => {
      organization.stores.forEach(store => {
        if (store.check) {
          let cantidad = this.campaign.elements[this.index_Stores].stores.filter(item => item.store._id == store._id);
          let element = this.list_elements.filter(item => item._id == this.currentStores.element['_id'])[0];
          let elementType = element.types.filter(item => item.name == this.currentStores.elementType)[0];
          let measurements = [];
          if (elementType.measurements) {
            elementType.measurements.forEach(item => {
              measurements.push({
                side: item,
                value: '',
                quantity: 1
              })
            });
          }
          let alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'];
          cantidad = alphabet[cantidad.length];
          this.currentStores.stores.push({
            store: store._id,
            location: this.currentStores.location,
            code: this.currentStores.code + '-' + cantidad,
            needConfirmation: elementType.needConfirmation,
            needMeasurement: elementType.needMeasurement,
            measurements: measurements,
          });
        }
      });
    });
    this.loader.fire(true);
    this.campaignS.addStore(this.idCampaign, this.currentStores)
      .subscribe(response => {
        if(response.alert){
          console.log(response);
          this.loader.fire(false);
          this.message2 = response.alert;
          var parent = this;
          setTimeout(function(){
            parent.message2 = '';
          }, 5000);
          return;
        }

        this.storeModal.close();
        this.loader.fire(false);
        this.getDetail();
      });
  }

  saveExcelStores() {
    this.currentStores.stores = [];
    let list_index = 1;
    for(var excelStore in this.excelTiendas){
      var excelDates = this.excelTiendas[excelStore];
      if(excelDates){
        if (parseInt(excelDates['8']) > 0) {
          var cantidad = parseInt(excelDates['8']);
          let store = this.list_stores.filter(item => item.name == excelDates['7'])[0];
          if(store){
            let element = this.list_elements.filter(item => item._id == this.currentStores.element["_id"])[0];
            let elementType = element.types.filter(item => item.name == this.currentStores.elementType)[0];
            let contStore = this.campaign.elements[this.index_Stores].stores.filter(item => item.store._id == store._id).length;
            let measurements = [];
            if (elementType.measurements) {
              elementType.measurements.forEach(item => {
                measurements.push({
                  side: item,
                  value: '',
                  quantity: 1
                })
              });
            }
            let alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'];
            for (var i = contStore; i < cantidad + contStore; i++) {
              let letter = alphabet[i];
              this.currentStores.stores.push({
                store: store._id,
                location: this.currentStores.location,
                code: this.currentStores.code + '-' + letter,
                needConfirmation: elementType.needConfirmation,
                needMeasurement: elementType.needMeasurement,
                measurements: measurements,
              });
            }
          }        
        }
      }
    };
    this.importExcelModal.close();

    this.loader.fire(true);
    this.campaignS.addStore(this.idCampaign, this.currentStores)
    .subscribe(response => {
      this.storeModal.close();
      this.loader.fire(false);
      this.getDetail();
    });
  }

  createWS(name){
    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;
    return this.wb.Sheets[name];
  }

  searchElement(id){
    for (var i = this.list_elements.length - 1; i >= 0; i--) {
      if(this.list_elements[i]._id == id){
        return this.list_elements[i].name;
      }
    }
    return '';
  }

  searchClient(id){
    for (var i = this.list_clients.length - 1; i >= 0; i--) {
      if(this.list_clients[i]._id == id){
        return this.list_clients[i].name;
      }
    }
    return '';
  }

  blocked(dataStore){
    var confirmation = confirm(`¿Está seguro que desea bloquear ${dataStore.store.name}?`);
    if(confirmation){
      this.campaignS.updateStore(this.idCampaign, { code: dataStore.code, store: dataStore.store._id, blocked: true})
      .subscribe( response => {
        //this.listStoreModal.close();
        this.getDetail();
        dataStore.blocked = true;
      });
    }    
  }

  unblocked(dataStore){
    this.campaignS.updateStore(this.idCampaign, { code: dataStore.code, store: dataStore.store._id, blocked: false})
    .subscribe( response => {
      //this.listStoreModal.close();
      this.getDetail();
      dataStore.blocked = false;
    });    
  }

  sortAlphabet(a,b){
    if(a.store.name == b.store.name){
      if(a.code < b.code) return -1;
      if(b.code < a.code) return 1;
    }
    if(a.store.name < b.store.name) return -1;
    if(b.store.name < a.store.name) return 1;
    return 0;
  }

  compare(a){
    if(a.blocked == false) return -1;
    else if (a.blocked == true) return 1;
    return 0;
  }

  exportReport(){
    this.loader.fire(true);
    if(!this.campaign  || this.campaign.length == 0){
      this.loader.fire(false);
      return;
    }

    var dataProcessed = JSON.parse(JSON.stringify(this.campaign));
    var storesProcessed = [];

    for (var jj = 0; jj < dataProcessed.elements.length; jj++) {
      for (var jk = 0; jk < dataProcessed.elements[jj].stores.length; jk++) {
        var tempStore = JSON.parse(JSON.stringify(dataProcessed.elements[jj].stores[jk]));
        tempStore.element = {
          code: dataProcessed.elements[jj].code,
          element: this.searchElement(dataProcessed.elements[jj].element._id),
          elementType: dataProcessed.elements[jj].elementType,
          implementer: dataProcessed.elements[jj].implementer,
          location: dataProcessed.elements[jj].location,
          class: dataProcessed.elements[jj].element.class
        }
        tempStore.campaign = {
          enabled: dataProcessed.enabled,
          brand: dataProcessed.brand,
          category: dataProcessed.category,
          name: dataProcessed.name,
          image: dataProcessed.image,
          duration: dataProcessed.duration,
          created: dataProcessed.created,
          client: this.searchClient(dataProcessed.client),
        }
        storesProcessed.push(tempStore);
      }
    }

    var fila = 1;
    var col = 0;
    let wscols = [];
    const columsWidth = 35;
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };
    var ws = this.createWS('Report');

    ws['!merges'] = [];

    fila++;
    col = 0;

    let headersInv = ['STATUS', 'FECHA', 'OBS'];

    let headers = ['FECHA DE REGISTRO', 'OPERADOR', 'SUPERVISOR', 'AÑO', 'MES', 'IMPLEMENTADOR', 'COD. ELEMENTO', 
      'CANTIDAD', 'CLASIFICACION ELEMENTO', 'ELEMENTO', 'TIPO DE ELEMENTO', 'CLIENTE', 'MARCA', 'CAMPAÑA', 
      'CADENA', 'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGION', 'IN', 'OUT', 'IN 2', 'OUT 2', 'MOTIVOS NO INSTALACIÓN',
      'STATUS INSTALACIÓN', 'OBSERVACIÓN', 'UBICACIÓN', 'CATEGORÍA'];

    for (var k = 0; k < headers.length; k++) {
      wscols.push({ wch: columsWidth });
    }

    for (var k = 0; k < headers.length; k++) {
      ws[this.utilService.getLetter(k) + fila] = {t:'s', v:headers[k], s:AppSettings.fillCabecera};
      col++;
    }

    // Find the maximum number of inventories
    let countInventoriesMax = 0;
    /*for (var k = 0; k < storesProcessed.length; k++) {
      let countInventories = storesProcessed[k].inventory.length;
      countInventoriesMax = ((countInventories > countInventoriesMax) ? countInventories : countInventoriesMax);
    }*/
    countInventoriesMax = 1;

    //  For draw inventories
    for (var k = 0; k < 1; k++) {//countInventoriesMax
      //  To merge cells dinamically
      ws['!merges'].push({ 
                        s: { r: 0, c: col }, 
                        e:{ r: 0, c: col + 2 } 
                      });
      //  Write intentory label
      ws[this.utilService.getLetter(col) + 1] = {t:'s', v:'INV ' + (k+1), s:AppSettings.fillClient};
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[0], s:AppSettings.fillCabecera};
      col++;
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[1], s:AppSettings.fillCabecera};
      col++;
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[2], s:AppSettings.fillCabecera};
      col++;

    }

    for (var k = 0; k < countInventoriesMax*3; k++) {
      wscols.push({ wch: columsWidth });
    }

    ws[this.utilService.getLetter(col) + fila] = {t:'s', v:'LINK FOTO', s:AppSettings.fillCabecera};

    wscols.push({ wch: 90 });
    fila++;
    col = 0;

    for (var i = 0; i < storesProcessed.length; i++) {

      var side = '';
      var value = '';
      var quantity = '';
      for (var j = 0; j < storesProcessed[i].measurements.length; j++) {
        side += '-' + storesProcessed[i].measurements[j].side + '\n';
        value += '-' + storesProcessed[i].measurements[j].value + '\n';
        quantity += '-' + storesProcessed[i].measurements[j].side + ':' + storesProcessed[i].measurements[j].quantity + '\n';
      }
      col = 0;

      // Information
      let dateArray = storesProcessed[i].campaign.created.split('-');
      let year = dateArray[0];
      // To use month in spanish
      moment.locale('es');
      let month = moment().month(dateArray[1] - 1).format('MMMM');
      let supervisor = (!this.getDataProperty(storesProcessed[i], 'confirmed.ok'))?this.getDataProperty(storesProcessed[i], 'installed.operator.permissions.supervisor.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'installed.operator.permissions.supervisor.lastname'):this.getDataProperty(storesProcessed[i], 'confirmed.operator.permissions.supervisor.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'confirmed.operator.permissions.supervisor.lastname');
      supervisor = supervisor == '' || supervisor == ' '?'Sin Supervisor':supervisor; 

      let operator = (!this.getDataProperty(storesProcessed[i], 'confirmed.ok'))?this.getDataProperty(storesProcessed[i], 'installed.operator.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'installed.operator.lastname'):this.getDataProperty(storesProcessed[i], 'confirmed.operator.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'confirmed.operator.lastname');
      operator = operator == '' || operator == ' '?'Elemento no instalado y/o confirmado':operator;
      
      let region = this.getDataProperty(storesProcessed[i], 'store.region');
      let province = this.getDataProperty(storesProcessed[i], 'store.location.province.name');
      let district = this.getDataProperty(storesProcessed[i], 'store.location.district.name');
      let imagemeasu = (storesProcessed[i].measurementsPhoto)?'=HIPERVINCULO("' + 'http://quasar.atypax.com:3036/'+storesProcessed[i].measurementsPhoto + '";"link")' + '\n' : '';

      let motiveNoInstalled = '-';
      if(this.getDataProperty(storesProcessed[i], 'installed.status') != 'INSTALADO') {
        let issueInstall = storesProcessed[i].issues.filter(item=> item.target && (item.target.toLowerCase() == 'install' || item.target.toLowerCase() == 'instalación'));
        motiveNoInstalled = issueInstall.length > 0?issueInstall[0].motive:'-';
      }
      let isBlocked = this.getDataProperty(storesProcessed[i], 'blocked');
      let enabled = this.getDataProperty(storesProcessed[i], 'campaign.enabled');
      let fInventory = this.getDataProperty(storesProcessed[i], 'inventory');
      console.log(storesProcessed[i]);
      console.log(this.getDataProperty(storesProcessed[i], 'installed.status'));
      let data = [
        this.getDataProperty(storesProcessed[i], 'campaign.created').split('T')[0],
        operator,
        supervisor,
        year,
        month,
        this.getDataProperty(storesProcessed[i], 'element.implementer'),
        this.getDataProperty(storesProcessed[i], 'element.code'),
        '1',//quantity?quantity:'1',                  
        this.getDataProperty(storesProcessed[i], 'element.class'),
        this.getDataProperty(storesProcessed[i], 'element.element'),
        this.getDataProperty(storesProcessed[i], 'element.elementType'),
        this.getDataProperty(storesProcessed[i], 'campaign.client'),
        this.getDataProperty(storesProcessed[i], 'campaign.brand'),
        this.getDataProperty(storesProcessed[i], 'campaign.name'),
        this.getDataProperty(storesProcessed[i], 'store.reference.chain.name'),
        this.getDataProperty(storesProcessed[i], 'store.sapCode'),
        this.getDataProperty(storesProcessed[i], 'store.name'),
        region == 'Lima'?(district?district:province):this.getDataProperty(storesProcessed[i], 'store.location.department.name'),
        region,
        this.getDataProperty(storesProcessed[i], 'programed.install').split('T')[0],
        this.getDataProperty(storesProcessed[i], 'programed.uninstall').split('T')[0],
        this.getDataProperty(storesProcessed[i], 'installed.date').split('T')[0]?this.getDataProperty(storesProcessed[i], 'installed.date').split('T')[0]:'-',
        this.getDataProperty(storesProcessed[i], 'uninstalled.date').split('T')[0]?this.getDataProperty(storesProcessed[i], 'uninstalled.date').split('T')[0]:'-',
        motiveNoInstalled,
        (isBlocked || !enabled)?'CANCELADO':this.getDataProperty(storesProcessed[i], 'installed.status'),
        this.getDataProperty(storesProcessed[i], 'measurementsObservation')?this.getDataProperty(storesProcessed[i], 'measurementsObservation'):'-',
        (this.getDataProperty(storesProcessed[i], 'location')? this.getDataProperty(storesProcessed[i], 'location'): this.getDataProperty(storesProcessed[i], 'element.location')),
        this.getDataProperty(storesProcessed[i], 'campaign.category'),
        (fInventory.length > 0? (fInventory[0].ok?'INVENTARIADO': 'PENDIENTE'):'PENDIENTE'),
        (fInventory.length > 0? fInventory[0].date:'-'),
        (fInventory.length > 0 && fInventory[0].comment? fInventory[0].comment:'-'), 
        this.getDataProperty(storesProcessed[i], 'installed.picture')?'=HIPERVINCULO("' + AppSettings.BASE_PATH + this.getDataProperty(storesProcessed[i], 'installed.picture') + '","link")' + '\n' : (imagemeasu?imagemeasu:'-'),
      ];

      for (var k = 0; k < data.length; k++) {
        ws[this.utilService.getLetter(k) + fila] = {t:'s', v:data[k], s:AppSettings.fillClient};
        col++;
      }

      //  For inventories
      /*for (var k = 0; k < countInventoriesMax; k++) {
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.ok'), 
          s:AppSettings.fillClient
        };
        col ++;
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.date'), 
          s:AppSettings.fillClient
        };
        col ++;
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.comment'), 
          s:AppSettings.fillClient
        };
        col++;
      }*/
      fila++;
    }


    ws["!ref"] = "A1:CZ" + (fila+1);
    ws['!cols'] = wscols;

    var wopts:any = { bookType:'xlsx', bookSST:true, type:'binary', Props:{cellStyles:true, cellFormula:true}};

    var wbout = XLSX.write(this.wb,wopts);

    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([this.utilService.s2ab(wbout)],{type:"application/octet-stream"}), "reporte_campanas.xlsx");
    this.loader.fire(false);
  }

  /**
   *  To figure out if an object have all properties passed
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    let allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }
    return obj;
  }

  convertFile2Base64 = function(url, callback, parent) {
    var xhr = new XMLHttpRequest();
     xhr.withCredentials = true;
    xhr.onload = function() {
      var reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result, parent);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.send();
  }

  convertUrl2Base64 = function(url, callback, parent){
    parent.campaignS.getImageCamp(url)
    .subscribe(response => {
      callback(response.result, parent);
    });
  }

  preExportPdf(){
    this.loader.fire(true);
    this.pdfElement = this.campaign.elements[0];
    var pdfClient = this.list_clients.filter(item => item._id == this.campaign.client)[0].name;
    this.pdfStores = this.pdfElement.stores.filter(item => item.installed.status.toUpperCase() == 'INSTALADO');
    var pdfCountInstalls = this.pdfStores.length;
    this.pdfNumStores = 0;
    this.pdfTablaStores = {
      style: 'table-normal',
      table: {
        widths: ['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
        headerRows: 1,
        dontBreakRows:true,
        // keepWithHeaderRows: 1,rowSpan: 4, 
        body: [
          [{text: '#', style: [ 'body-normal']}, {text: 'FECHA DE INSTALACIÓN', style: [ 'body-normal']}, {text: 'CADENA', style: [ 'body-normal']}, {text: 'TIENDA', style: [ 'body-normal']}, {text: 'UBICACIÓN', style: [ 'body-normal']}, {text: 'FOTO', style: [ 'body-normal']}, {text: 'OBSERVACIONES', style: [ 'body-normal']}],
        ],
      }, 
      layout: ''
    };
    this.convertFile2Base64('./assets/images/menbre.png', function(imagen, parent){
      parent.tempPdf = {
        pageSize:"A4",
        pageMargins:[30,40,30,40],
        //pageOrientation: 'landscape', header:{ }, footer:{ },
        content: [
          //{ text: '$scope.text_pdf', style: ['header']},
        ],
      }
      parent.tempPdf.content.push({
        table: {
          widths: ['auto', '*'],
          headerRows: 0,
          // keepWithHeaderRows: 1,rowSpan: 4, 
          body: [
              [{image: imagen, width: 360}, {text: ["REPORTE DE IMPLEMENTACIONES"], style: [ 'header-title-im']}],
          ]
        }, 
        layout: 'noBorders'
      });
      parent.addStyles(parent.tempPdf);
      parent.convertUrl2Base64(parent.campaign.image, function(imagen, parent){
        parent.tempPdf.content.push({
          style: 'table-normal',
          table: {
            widths: ['auto', '*', 'auto'],
            headerRows: 0,
            // keepWithHeaderRows: 1, 
            body: [//width: 180, height: 150
                [{text: "Cliente", style: [ 'body-normal']}, {text: pdfClient, style: [ 'body-normal']}, {image: imagen, rowSpan: 8, fit:[180, 150]}],
                [{text: "Marca", style: [ 'body-normal']}, {text: parent.campaign.brand, style: [ 'body-normal']}, ''],
                [{text: "Campaña", style: [ 'body-normal']}, {text: parent.campaign.name, style: [ 'body-normal']}, ''],
                [{text: "Tipo de elemento", style: [ 'body-normal']}, {text: parent.pdfElement.elementType, style: [ 'body-normal']}, ''],
                [{text: "Código de elemento", style: [ 'body-normal']}, {text: parent.pdfElement.code, style: [ 'body-normal']}, ''],
                [{text: "Fecha Inicial", style: [ 'body-normal']}, {text: parent.campaign.duration.start.format('L'), style: [ 'body-normal']}, ''],
                [{text: "Fecha Final", style: [ 'body-normal']}, {text: parent.campaign.duration.end.format('L'), style: [ 'body-normal']}, ''],
                [{text: "Elementos Instalados", style: [ 'body-normal']}, {text: pdfCountInstalls, style: [ 'body-normal']}, ''],
            ],
          }, 
          layout: ''
        });

        if(pdfCountInstalls == 0){
          pdfMake.createPdf(parent.tempPdf).download('campaña1.pdf');
          this.loader.fire(false);
        }
        else{
          parent.addStoresToPDf(parent);
        }
      }, parent);

    }, this);
  }

  addStoresToPDf(parent){
    parent.convertUrl2Base64(parent.pdfStores[parent.pdfNumStores].installed.picture.replace('campaigns', 'campaigns/low') , function(imagen, parent){
      var comment = parent.pdfStores[parent.pdfNumStores].installed.comment?parent.pdfStores[parent.pdfNumStores].installed.comment:'Sin observaciones';
      var dateInstalled = moment(parent.pdfStores[parent.pdfNumStores].installed.date);
      console.log(parent.pdfNumStores + 1, dateInstalled.format('L') + ' ' + dateInstalled.format('LT'), parent.pdfStores[parent.pdfNumStores].store.reference.chain.name, parent.pdfStores[parent.pdfNumStores].store.name, parent.pdfStores[parent.pdfNumStores].location, imagen, comment);
      parent.pdfTablaStores.table.body.push([{text: (parent.pdfNumStores + 1), style: [ 'body-normal']}, {text: dateInstalled.format('L') + ' ' + dateInstalled.format('LT'), style: [ 'body-normal']}, {text: parent.pdfStores[parent.pdfNumStores].store.reference.chain.name, style: [ 'body-normal']}, {text: parent.pdfStores[parent.pdfNumStores].store.name, style: [ 'body-normal']}, {text: parent.pdfStores[parent.pdfNumStores].location, style: [ 'body-normal']}, {image: imagen, fit:[120, 140], alignment: 'center'}, {text: comment, style: [ 'body-normal']}]);
      parent.pdfNumStores++;
      if(parent.pdfNumStores == parent.pdfStores.length){
        parent.tempPdf.content.push(parent.pdfTablaStores);
        pdfMake.createPdf(parent.tempPdf).download('campaign_' + parent.campaign.name + '.pdf');
        parent.loader.fire(false);
        return;
      }
      parent.addStoresToPDf(parent);
    }, parent);    
  }

  //columns:[{alignment: 'right',text: 'footer',style:['footer-normal']},],

  addStyles(tempPdf){
    tempPdf.styles = {
      header: {
        fontSize: 10,
        bold:true,
        margin: [0, 10, 0, 0],
      },
      'header-title-im':{
        fontSize: 16,
        bold: true,
        alignment: 'center',
        margin: [0, 40, 0, 0],
      },
      'header-light':{
        fontSize: 9,
        bold:false,
        light:true,
      },
      'body-normal': {
        fontSize: 9,
      },
      'body-title':{
        fontSize: 10,
        bold:true,
      },
      'table-normal': {
        margin: [0, 5, 0, 15]
      },
      'table-principal': {
        margin: [0, 15, 0, 15]
      },
      headerTwo: {
        fontSize: 10,
        bold:true,
        margin: [0, 20, 0, 0],
        alignment: 'right'
      },
      anotherStyle: {
        alignment: 'center',
      },
      dateStyle:{
        fontSize: 17,
      },
      noteStyle:{
        alignment: 'right',
      },
      tableCompetencias:{
        margin: [0,4,0,4],
      }
    }
  }

}

