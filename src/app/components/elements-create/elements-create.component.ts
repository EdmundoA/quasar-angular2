import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { OrganizationService } from "../../services/organization.service";
import { LoaderEvent } from "../../services/loader-event";
import { ElementsService } from '../../services/elements.service';
import { RoutesService } from '../../services/routes.service';
import { ActivatedRoute, Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";
import { Overlay } from 'angular2-modal';
import { RoutesEntity } from "../../entities/routes.entity";
import { Modal } from 'ngx-modal';

@Component({
  selector: 'app-elements-create',
  templateUrl: './elements-create.component.html',
  providers:[
   LoaderEvent, 
   SubtitleEvent,
   ElementsService,
   RoutesService,
  ]
})
export class ElementsCreateComponent implements OnInit {

    @ViewChild('lgModal') public lgModal:Modal;
	  public message: any;
    public isModalShown:boolean = false;
    public message2: any;
  	public sub: any;
  	public creating: Boolean = true;
  	public id_client: String;
    public edit_type: Boolean = false;
    public isEdited:Boolean = false;
    public number_type: Number;
  	public type:any = {
  		name: '',
  		needConfirmation: null,
      needMeasurement: null,
  		measurements: [  			
  		]
  	};
  	public classes: Array<String> = [];
  	public showbar: Boolean = false;
    public showType: Boolean = false;
  	public elements: any = {
  		name: '',
  		class: '',
  		types: []
  		
  	};

  constructor(
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private routesService: RoutesService,
    private elementsService: ElementsService,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.sub = this.routeParams.params.subscribe(
      params => {
        if (params['id']) {
        	this.id_client = params['id'];
          const id = params['id'];
          this.elementsService.detail(id)
            .subscribe(response => {
              if (response.status === 200) {
                this.showbar = true;
                this.elements = response.result;
                // console.log(this.elements);
                this.creating = false;
                this.loader.fire(false);
              }else {

              }
            });
          this.subtitleEvent.fire('Editar');
        
        }else{
          this.isEdited = true;
          this.subtitleEvent.fire('Crear');
          this.loader.fire(false);
        }
      }
    );
    this.getClasses();
  }

  getClasses() {
  	this.loader.fire(true);
    this.elementsService.getClasses()
    .subscribe(
      response => {
        this.classes = response.result;
        this.loader.fire(false);
      }
    );
  }

  savePer() {
    this.lgModal.open();
    this.initType();
    this.edit_type = false;
    this.message = false;
    this.isModalShown = true;   
  }

  onHidden() {
    this.isModalShown = false;
  }

  hideModal() {
    this.lgModal.close();
    this.initType();
  }

  hideModal2() {
    this.lgModal.close();
    this.initType();
  }

  initType(){
    this.type = {
      name: '',
      needConfirmation: null,
      needMeasurement: null,
      measurements: [        
      ],      
    };
  }

  sendType() {
    if (this.type['name'] === '') {
      this.message2 = "Faltan completar campos";
    } else {
      if (this.edit_type) {
        // console.log(this.type);
        this.elements['types'][this.number_type + ''] = {
          name: this.type['name'],
          needConfirmation: this.type['needConfirmation'],
          needMeasurement: this.type['needMeasurement'],
          measurements: this.type['measurements'] || [],
        }
        this.initType();
        this.lgModal.close();
      } else {
        this.message2 = false;
        this.elements['types'].push(this.type);
        this.initType();
      }
      this.showbar = true;
      this.isEdited = true;
    }
  }

  onSubmit(value) {
    if (this.creating) {
      if (value.name == '' && value.class == '') {
        this.message = "Faltan completar campos";
      } else {
        this.loader.fire(true);
        this.elementsService.create(value)
        .subscribe(result => {
          // console.log(result.result);
          this.loader.fire(false);
          this.router.navigate(['elements', result.result._id, 'edit']);
        });
      }
    } else {
      this.loader.fire(true);
      this.elementsService.update(this.elements['_id'], value)
      .subscribe(
          result => {
            this.loader.fire(false);
            // console.log(result.result);
            //this.router.navigate(['elements']);
            this.isEdited = false;
          }
      );
    }
  }

  onDelete(data, index) {
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.elements['types'].splice(index, 1);
      this.isEdited = true;
    }
    if(this.elements['types'].length == 0) {
      this.showbar = true;
    }
  }

  editType(data, index) {
    this.edit_type = true;
    this.number_type = index;
    // console.log(this.number_type);
    // console.log(data);
    this.type = new Array();
    this.type['name'] = data['name'];
    this.type['needConfirmation'] = data['needConfirmation'];
    this.type['needMeasurement'] = data['needMeasurement'];
    this.type['measurements'] = (data['measurements'])?data['measurements']:[];
    this.isModalShown = true;
    this.lgModal.open();
  }

  cancel() {
    this.showType = false;
    this.showbar = false;
  }

  close() {
    this.message = false;
  }

}
