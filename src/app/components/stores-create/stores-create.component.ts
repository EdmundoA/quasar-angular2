import { Component, OnInit, Injectable, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { UserService } from '../../services/user.service';
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { StoresService } from "../../services/stores.service";
import { Modal } from 'ngx-modal';
import { OrganizationService } from "../../services/organization.service";
import { ChainsService } from "../../services/chains.service";
import { StoreEntity } from '../../entities/store.entity';
import { RoutesService } from "../../services/routes.service";

@Component({
  selector: 'app-stores-create',
  templateUrl: './stores-create.component.html',
  providers: [
    FormBuilder,
    UserService,
    LoaderEvent,
    SessionService,
    OrganizationService,
    SubtitleEvent,
    ChainsService,
    StoresService,
    RoutesService
  ]
})
export class StoresCreateComponent implements OnInit {

  @ViewChild('elementModal') public elementModal:Modal;

	public storeForm: FormGroup;
  public allData;
  public organization: String = '';
  public cadena: String = '';
  public message2;
  public showbar: Boolean = false;
  public positions;
  public listContact: Array<any> = [];
  public allStores: Array<any> = [];
  public message: any;
  public province: Boolean = false;
  public district: Boolean = false;
  public sub: any;
  public store: StoreEntity;
  public creating: Boolean = true;
  public regions: Array<String> = [];
  public districts: Array<String> = [];
  public zones: Array<String> = [];
  public clusters: Array<String> = [];
  public clusters2: Array<String> = [];
  public format: Array<String> = [];
  public departments: Array<String> = [];
  public provinces: Array<String> = [];
  public organizations: Array<String> = [];
  public chains: Array<String> = [];
  public sectors: Array<String> = [];
  public departamento: String;
  public provincia: String;
  public distrito: String;
  public changeone: Boolean = false;
  public changetwo: Boolean = false;
  public showChain: Boolean = false;
  public OC: String;
  public loading = false;
  public routes;

  public contact = {
    store: '',
    position: '',
    firstname: '',
    lastname: '',
    phone: '',
    email: ''
  }

  public newRoute: String = '';

  public EditC = false;
  public datacontact;

  public CH: String;
  public lato: Number;
  public longi: Number;
  public depa: String;
  public provi: String;
  public distri: String;

  constructor(
    private userService: UserService,
    private _fb: FormBuilder,
    private session: SessionService,
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private storeService: StoresService,
    private orgaService: OrganizationService,
    private chainService: ChainsService,
    private routeS: RoutesService,

  ) {
  }

  ngOnInit() {
    this.departamento = '';
    this.provincia = '';
    this.loader.fire(true);

  	this.storeForm = this._fb.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      sapCode: ['', Validators.required],
      region: ['', Validators.required],
      zone: ['', Validators.required],
      cluster: ['', Validators.required],
      cluster2: ['', Validators.required],
      format: ['', Validators.required],
      route: [''],
      address: ['', Validators.required],
      department: ['', Validators.required],
      province: ['', Validators.required],
      district: ['', Validators.required],
      lat: ['', Validators.required],
      lon: ['', Validators.required],
      organization: ['', Validators.required],
      chain: ['', Validators.required],
      sector: ['', Validators.required],
    });

    this.sub = this.routeParams.params.subscribe(
      params => {

        if (params['id']) {
          this.loading = true;
          const id = params['id'];
          this.storeService.detail(id)
            .subscribe(response => {
              if (response.status === 200) {
                 console.log(response);
                this.allData = response.result;
                this.contact['store'] = this.allData._id;

                if (this.allData.contacts.length != 0) {
                  this.showbar = true;
                }

                // console.log(this.allData);
                this.store = response.result;
                this.OC = response.result.reference.organization._id;
                this.CH = response.result.reference.chain._id;
                if (response.result.gpsPosition) {
                  this.lato = response.result.gpsPosition[1];
                  this.longi = response.result.gpsPosition[0];
                } else {
                  this.lato = null;
                  this.longi = null;
                }
                this.newRoute = response.result.route;
                this.cadena = response.result.reference.chain._id;                
                this.depa = response.result.location.department;
                this.provi = response.result.location.province;
                this.distri = response.result.location.district;
                this.showChain = true;
                this.creating = false;
                this.chainService.getChainsByOrganization({organization: this.OC})
                  .subscribe( response => {
                    this.chains = response.result;
                    this.refreshStore();
                  });
                
                
              }else {

              }
            });
          this.subtitleEvent.fire('Editar');
        }else{
          this.subtitleEvent.fire('Crear');
          //this.creating = true;
        }
      }
    );
    this.getRegions();

  }

  onDelete(data, index) {
    console.log(data);
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.firstname}?`);

    if(confirmation){
      this.storeService.deleteContact(data._id, data.position)
        .subscribe(response => {
          console.log(response);
        });
    }

  }

  hideElementModal() {
    this.elementModal.close();
  }

  refreshStore() {
    this.storeForm.controls['organization'].setValue(this.OC);
    this.storeForm.controls['chain'].setValue(this.CH);
    this.storeForm.controls['lat'].setValue(this.lato);
    this.storeForm.controls['lon'].setValue(this.longi);
    this.storeForm.controls['department'].setValue(this.depa);
    this.storeForm.controls['route'].setValue(this.newRoute);
    console.log(this.storeForm);
    this.storeForm.patchValue(this.store);
  }

  getRegions() {
    this.storeService.getRegions()
      .subscribe(
          response => {
            this.regions = response.result;
            this.storeService.getZones()
              .subscribe(
                  response => {
                    this.zones = response.result;
                    this.storeService.getClusters()
                      .subscribe(
                          response => {
                            this.clusters = response.result;
                            this.storeService.getClusters2()
                              .subscribe(
                                  response => {
                                    this.clusters2 = response.result;
                                    this.storeService.getFormats()
                                      .subscribe(
                                          response => {
                                            this.format = response.result;
                                            this.storeService.getDepartment()
                                              .subscribe(
                                                  response => {
                                                    this.departments = response.result;
                                                    this.orgaService.getAll()
                                                      .subscribe(
                                                          response => {
                                                            console.log(response);
                                                            this.organizations = response.result;
                                                            this.chainService.getChains()
                                                              .subscribe(
                                                                  response => {
                                                                    this.storeService.getSectors()
                                                                      .subscribe(
                                                                          response => {
                                                                            this.sectors = response.result;
                                                                            this.storeService.getStores()
                                                                              .subscribe(response => {
                                                                                console.log(response);
                                                                                for (let i = 0; i < response.result.length; i++) {
                                                                                  if (response.result[i].enabled) {
                                                                                    this.allStores.push(response.result[i])
                                                                                  }
                                                                                }
                                                                                this.storeService.getPositions()
                                                                                  .subscribe(response => {
                                                                                    this.positions = response.result;                                                                                    
                                                                                    this.routeS.getRoute()
                                                                                      .subscribe( response => {
                                                                                        this.routes = response.result;
                                                                                        this.loading = false;
                                                                                        this.loader.fire(false);
                                                                                      });
                                                                                    
                                                                                  });                                                                                
                                                                              });
                                                                          }
                                                                      );
                                                                  }
                                                              );                                                            
                                                          }
                                                      );
                                                  }
                                              );
                                          }
                                      );
                                  }
                              );
                          }
                      );
                  }
              );
          }
      );
  }

  changeProvince(id) {
    if (this.creating || this.changeone) {
      this.provincia = '';
      this.district = false;
      if (id == '') {
        this.province = false;
      } else {
        this.loader.fire(true);
        this.storeService.getProvinces(id)
          .subscribe(
              response => {
                this.province = true;
                this.provinces = response.result;
                this.loader.fire(false);
              }
            );
      }
    } else {
      if (id == '') {
        this.province = false;
      } else {
        this.storeService.getProvinces(id)
          .subscribe(
              response => {
                this.province = true;
                this.provinces = response.result;
              }
            );
        this.storeForm.controls['province'].setValue(this.provi);
        this.province = true;
        this.provincia = this.storeForm.controls['province'].value;
        this.changeone = true;
      }      
    }
  }

  AddContact() {
    this.contact = {
      store: this.allData._id,
      position: '',
      firstname: '',
      lastname: '',
      phone: '',
      email: ''
    }
    this.elementModal.open();
    this.EditC = false;
  }

  sendContact() {
    if (this.EditC) {
      this.storeService.updateContact(this.contact['store'], this.contact)
        .subscribe( response => {
          console.log(response);
          this.elementModal.close();
        });
    } else {
      this.storeService.createContact(this.contact)
      .subscribe(response => {
        this.showbar = true;
        this.allData.contacts.push(this.contact);

        this.contact = {
          store: this.allData._id,
          position: '',
          firstname: '',
          lastname: '',
          phone: '',
          email: ''
        }
        console.log(response);
      });
    }
    
  }

  editContact(data, index) {

    this.datacontact = data;
    this.contact = {
        store: this.allData._id,
        position: data.position,
        firstname: data.firstname,
        lastname: data.lastname,
        phone: data.phone,
        email: data.email
    }
    console.log(this.datacontact);
    this.EditC = true;
    this.elementModal.open();
    
  }

  changeDistrict(id) {
    if (this.creating || this.changetwo) {
      this.distrito = '';
      if (id === '') {
        this.district = false;
      } else {
        this.loader.fire(true);
        this.storeService.getDistricts(id)
          .subscribe(
              response => {
                this.district = true;
                this.districts = response.result;
                this.loader.fire(false);
              }
            );
      }
    } else {
      if (id === '') {
        this.district = false;
      } else {
        this.storeService.getDistricts(id)
          .subscribe(
              response => {
                this.district = true;
                this.districts = response.result;  
              }
            );
          this.storeForm.controls['district'].setValue(this.distri);
          this.distrito = this.storeForm.controls['district'].value;
          this.changetwo = true;
      }
    }
  }

  chainOrga(id) {
    console.log(id);
    if (id && !this.loading) {
      this.cadena = '';     
      this.showChain = true;
      this.chainService.getChainsByOrganization({organization: id})
        .subscribe( response => {
          this.chains = response.result;
        });
    }
  }

  onSubmit(model: StoreEntity, isValid: Boolean) {
    if (isValid) {
      let params = {
        name: model.name,
        shortName: model.shortName,
        sapCode: model.sapCode,
        region: model.region,
        zone: model.zone,
        cluster: model.cluster,
        cluster2: model.cluster2,
        format: model.format,
        route: model.route,
        address: model.address,
        sector: model.sector,
        location: {
          department: model.department,
          province: model.province,
          district: model.district
        },
        gpsPosition: [model.lon, model.lat],
        reference: {
          organization: model.organization,
          chain: model.chain
        }
      }
      if (this.creating) {
        this.storeService.createStore(params)
        .subscribe(
            response => {
              this.router.navigate(['stores']);
            }
          );
      } else {
        this.storeService.update(this.store._id, params)
          .subscribe(
              response => {
                this.router.navigate(['stores']);
              }
            );
      }
    } else {

    }
  }
}

