import { Component, OnInit, ViewChild } from '@angular/core';
import { LoaderEvent } from "../../services/loader-event";
import { CampaignsService } from "../../services/campaigns.service";
import { SubtitleEvent } from "../../services/subtitle-event";
import { ClientService } from "../../services/clients.service";
import { Modal } from 'ngx-modal';
import { ElementsService } from "../../services/elements.service";
import { UtilService } from '../../services/util.service';
import { AppSettings } from "../../app.settings";
import { HostListener } from '@angular/core';
import * as moment from "moment";
import * as $ from 'jquery';
import {saveAs} from 'file-saver';
declare var XLSX:any;
type AOA = Array<Array<any> >;

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  providers: [
    LoaderEvent, 
    CampaignsService,
    ElementsService,
    UtilService,
  ]
})
export class CampaignListComponent implements OnInit {

  @ViewChild('storeModal') public storeModal: Modal;

	public campaign: Array<any> = [];
	public userFilter: any = { name: '', brand: '', nameClient: '', elements:[
    {elementType: '', }
  ], validity: '' };
  public list_clients = [];
  public actualcampaignid;
  public sended = [];
  public searchCampaign: any = {
    from: moment(),
    to: moment()
  };
  public dataCampaigns: any = [];
  public list_elements: any = [];
  public wb:any = {};

  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
  };

  constructor(
    private loader: LoaderEvent,
    private campaignS: CampaignsService,
    private subtitleEvent: SubtitleEvent,
    private clientS: ClientService,
    private elementS: ElementsService,
    private utilService: UtilService,
  ) { }

  ngOnInit() {
  	this.subtitleEvent.fire('');
    this.loader.fire(true);

    this.campaignS.getCampaign()
    .subscribe(response => {
      if(response.status === 200){
        // console.log(response);
        this.campaign = response.result;
        this.clientS.getClients()
        .subscribe( response => {
          for (let i = 0; i < response.result.length; i++) {
            for (let o = 0; o < this.campaign.length; o++) {
              this.campaign[o].validity = (moment(this.campaign[o].duration.end) < moment().startOf('day'))?'Vencido':'Vigente';
              if (response.result[i]._id == this.campaign[o].client) {
                this.campaign[o]['nameClient'] = response.result[i].name;
              }
            }
          }

          this.loader.fire(false);
          //console.log(this.campaign);
        });
      }
    });
  }

  sendSearchCampaign(search){
    this.loader.fire(true);
    this.campaignS.dataReport(new Date(search.from).toISOString(), new Date(search.to).toISOString())
    .subscribe(response =>{
      console.log(response);
      this.dataCampaigns = response.result;
      console.log(this.dataCampaigns);
      this.elementS.getAll()
      .subscribe(response => {
        this.list_elements = response.result;
        this.clientS.getClients()
        .subscribe(response => {
          this.list_clients = response.result;
          this.exportReport();
        });
      });
    });
  }

  searchElement(id){
    for (var i = this.list_elements.length - 1; i >= 0; i--) {
      if(this.list_elements[i]._id == id){
        return this.list_elements[i].name;
      }
    }
    return '';
  }

  searchClient(id){
    for (var i = this.list_clients.length - 1; i >= 0; i--) {
      if(this.list_clients[i]._id == id){
        return this.list_clients[i].name;
      }
    }
    return '';
  }

  createWS(name){
    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;

    return this.wb.Sheets[name];
  }

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    let allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }
    return obj;
  }

  exportReport(){
    this.loader.fire(true);
    if(!this.dataCampaigns  || this.dataCampaigns.length == 0){
      this.loader.fire(false);
      return;
    }

    var dataProcessed = JSON.parse(JSON.stringify(this.dataCampaigns));
    var storesProcessed = [];

    for (var dp = dataProcessed.length - 1; dp >= 0; dp--) {
      for (var jj = 0; jj < dataProcessed[dp].elements.length; jj++) {
        for (var jk = 0; jk < dataProcessed[dp].elements[jj].stores.length; jk++) {
          var tempStore = JSON.parse(JSON.stringify(dataProcessed[dp].elements[jj].stores[jk]));
          tempStore.element = {
            code: dataProcessed[dp].elements[jj].code,
            element: this.searchElement(dataProcessed[dp].elements[jj].element._id),
            elementType: dataProcessed[dp].elements[jj].elementType,
            implementer: dataProcessed[dp].elements[jj].implementer,
            location: dataProcessed[dp].elements[jj].location,
            class: dataProcessed[dp].elements[jj].element.class
          }
          tempStore.campaign = {
            enabled: dataProcessed[dp].enabled,
            brand: dataProcessed[dp].brand,
            category: dataProcessed[dp].category,
            name: dataProcessed[dp].name,
            image: dataProcessed[dp].image,
            duration: dataProcessed[dp].duration,
            created: dataProcessed[dp].created,
            client: this.searchClient(dataProcessed[dp].client),
          }
          storesProcessed.push(tempStore);
        }
      }
    }


    var fila = 1;
    var col = 0;
    let wscols = [];
    const columsWidth = 35;
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };
    var ws = this.createWS('Report');

    ws['!merges'] = [];

    fila++;
    col = 0;

    let headersInv = ['STATUS', 'FECHA', 'OBS'];

    let headers = ['FECHA DE REGISTRO', 'OPERADOR', 'SUPERVISOR', 'AÑO', 'MES', 'IMPLEMENTADOR', 'COD. ELEMENTO', 'CANTIDAD', 'CLASIFICACION ELEMENTO', 'ELEMENTO', 'TIPO DE ELEMENTO', 'CLIENTE', 'MARCA', 'CAMPAÑA', 'CADENA', 'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGION', 'IN', 'OUT', 'IN 2', 'OUT 2', 'MOTIVOS NO INSTALACIÓN', 'STATUS INSTALACIÓN', 'OBSERVACIÓN', 'UBICACIÓN', 'CATEGORÍA'];

    for (var k = 0; k < headers.length; k++) {
      wscols.push({ wch: columsWidth });
    }

    for (var k = 0; k < headers.length; k++) {
      ws[this.utilService.getLetter(k) + fila] = {t:'s', v:headers[k], s:AppSettings.fillCabecera};
      col++;
    }

    // Find the maximum number of inventories
    let countInventoriesMax = 0;
    /*for (var k = 0; k < storesProcessed.length; k++) {
      let countInventories = storesProcessed[k].inventory.length;
      countInventoriesMax = ((countInventories > countInventoriesMax) ? countInventories : countInventoriesMax);
    }*/
    countInventoriesMax = 1;

    //  For draw inventories
    for (var k = 0; k < 1; k++) {//countInventoriesMax
      //  To merge cells dinamically
      ws['!merges'].push({ 
                        s: { r: 0, c: col }, 
                        e:{ r: 0, c: col + 2 } 
                      });
      //  Write intentory label
      ws[this.utilService.getLetter(col) + 1] = {t:'s', v:'INVENTARIO' + (k+1), s:AppSettings.fillClient};
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[0], s:AppSettings.fillCabecera};
      col++;
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[1], s:AppSettings.fillCabecera};
      col++;
      ws[this.utilService.getLetter(col) + fila] = {t:'s', v: headersInv[2], s:AppSettings.fillCabecera};
      col++;

    }

    for (var k = 0; k < countInventoriesMax*3; k++) {
      wscols.push({ wch: columsWidth });
    }

    ws[this.utilService.getLetter(col) + fila] = {t:'s', v:'FOTO INSTALACIÓN', s:AppSettings.fillCabecera};

    wscols.push({ wch: 90 });

    fila++;
    col = 0;

    for (var i = 0; i < storesProcessed.length; i++) {

      var side = '';
      var value = '';
      var quantity = '';
      for (var j = 0; j < storesProcessed[i].measurements.length; j++) {
        side += '-' + storesProcessed[i].measurements[j].side + '\n';
        value += '-' + storesProcessed[i].measurements[j].value + '\n';
        quantity += '-' + storesProcessed[i].measurements[j].side + ':' + storesProcessed[i].measurements[j].quantity + '\n';
      }

      col = 0;

      // Information
      let dateArray = storesProcessed[i].campaign.created.split('-');
      let year = dateArray[0];
      // To use month in spanish
      moment.locale('es');
      let month = moment().month(dateArray[1] - 1).format('MMMM');
      
      let supervisor = (!this.getDataProperty(storesProcessed[i], 'confirmed.ok'))?this.getDataProperty(storesProcessed[i], 'installed.operator.permissions.supervisor.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'installed.operator.permissions.supervisor.lastname'):this.getDataProperty(storesProcessed[i], 'confirmed.operator.permissions.supervisor.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'confirmed.operator.permissions.supervisor.lastname');
      supervisor = supervisor == '' || supervisor == ' '?'Sin Supervisor':supervisor; 

      let operator = (!this.getDataProperty(storesProcessed[i], 'confirmed.ok'))?this.getDataProperty(storesProcessed[i], 'installed.operator.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'installed.operator.lastname'):this.getDataProperty(storesProcessed[i], 'confirmed.operator.firstname') + ' ' + this.getDataProperty(storesProcessed[i], 'confirmed.operator.lastname');
      operator = operator == '' || operator == ' '?'Elemento no instalado y/o confirmado':operator;

      let motiveNoInstalled = '-';
      let region = this.getDataProperty(storesProcessed[i], 'store.region');
      let province = this.getDataProperty(storesProcessed[i], 'store.location.province.name');
      let district = this.getDataProperty(storesProcessed[i], 'store.location.district.name');

      let imagemeasu = (storesProcessed[i].measurementsPhoto)?'=HIPERVINCULO("' + 'http://quasar.atypax.com:3036/'+storesProcessed[i].measurementsPhoto + '";"link")' + '\n' : '';

      if(this.getDataProperty(storesProcessed[i], 'installed.status') != 'INSTALADO') {
        // Warning!, I set up to issue 0, because the no installed's motive is save there.
        // But I don't know how to identify the exact motive, because there is not a indentifier
        // to know that the motive is for not installed, and I'm guessing that the fisrt issues is for not installed
        let issueInstall = storesProcessed[i].issues.filter(item=> item.target && (item.target.toLowerCase() == 'install' || item.target.toLowerCase() == 'instalación'));
        motiveNoInstalled = issueInstall.length > 0?issueInstall[0].motive:false;//this.getDataProperty(storesProcessed[i], 'issues[0].motive');
      }
      let isBlocked = this.getDataProperty(storesProcessed[i], 'blocked');
      let enabled = this.getDataProperty(storesProcessed[i], 'campaign.enabled');
      let fInventory = this.getDataProperty(storesProcessed[i], 'inventory');
      let data = [
        this.getDataProperty(storesProcessed[i], 'campaign.created').split('T')[0],
        operator,
        supervisor,
        year,
        month,
        this.getDataProperty(storesProcessed[i], 'element.implementer'),
        this.getDataProperty(storesProcessed[i], 'element.code'),
        '1',//quantity?quantity:'1',                  
        this.getDataProperty(storesProcessed[i], 'element.class'),
        this.getDataProperty(storesProcessed[i], 'element.element'),
        this.getDataProperty(storesProcessed[i], 'element.elementType'),
        this.getDataProperty(storesProcessed[i], 'campaign.client'),
        this.getDataProperty(storesProcessed[i], 'campaign.brand'),
        this.getDataProperty(storesProcessed[i], 'campaign.name'),
        this.getDataProperty(storesProcessed[i], 'store.reference.chain.name'),
        this.getDataProperty(storesProcessed[i], 'store.sapCode'),
        this.getDataProperty(storesProcessed[i], 'store.name'),
        region == 'Lima'?(district?district:province):this.getDataProperty(storesProcessed[i], 'store.location.department.name'),
        region,
        this.getDataProperty(storesProcessed[i], 'campaign.duration.start').split('T')[0],
        this.getDataProperty(storesProcessed[i], 'campaign.duration.end').split('T')[0],
        this.getDataProperty(storesProcessed[i], 'installed.date').split('T')[0]?this.getDataProperty(storesProcessed[i], 'installed.date').split('T')[0]:'-',
        this.getDataProperty(storesProcessed[i], 'uninstalled.date').split('T')[0]?this.getDataProperty(storesProcessed[i], 'uninstalled.date').split('T')[0]:'-',
        motiveNoInstalled?motiveNoInstalled:'-',
        (isBlocked || !enabled)?'CANCELADO':this.getDataProperty(storesProcessed[i], 'installed.status'),
        this.getDataProperty(storesProcessed[i], 'measurementsObservation')?this.getDataProperty(storesProcessed[i], 'measurementsObservation'):'-',
        (this.getDataProperty(storesProcessed[i], 'location')? this.getDataProperty(storesProcessed[i], 'location'): this.getDataProperty(storesProcessed[i], 'element.location')),
        this.getDataProperty(storesProcessed[i], 'campaign.category'),
        (fInventory.length > 0? (fInventory[0].ok?'INVENTARIADO': 'PENDIENTE'):'PENDIENTE'),
        (fInventory.length > 0? fInventory[0].date:'-'),
        (fInventory.length > 0 && fInventory[0].comment? fInventory[0].comment:'-'), 
        this.getDataProperty(storesProcessed[i], 'installed.picture')?'=HIPERVINCULO("' + AppSettings.BASE_PATH + this.getDataProperty(storesProcessed[i], 'installed.picture') + '","link")' + '\n' : (imagemeasu?imagemeasu:'-'),
      ];

      for (var k = 0; k < data.length; k++) {
        ws[this.utilService.getLetter(k) + fila] = {t:'s', v:data[k], s:AppSettings.fillClient};
        col++;
      }

      //  For inventories
      /*for (var k = 0; k < countInventoriesMax; k++) {
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.ok'), 
          s:AppSettings.fillClient
        };
        col ++;
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.date'), 
          s:AppSettings.fillClient
        };
        col ++;
        ws[this.utilService.getLetter(col) + fila] =  {
          t:'s', 
          v: this.getDataProperty(storesProcessed[i], 'inventory.comment'), 
          s:AppSettings.fillClient
        };
        col++;
      }*/

      /*ws[this.utilService.getLetter(col) + fila] =  {
        t:'s', 
        v:(storesProcessed[i].measurementsPhoto)?'=HIPERVINCULO("' + 'http://quasar.atypax.com:3036/'+storesProcessed[i].measurementsPhoto + '";"link")' + '\n' : '', 
        s:AppSettings.fillClient
      };*/

      fila++;
    }


    ws["!ref"] = "A1:CZ" + (fila+1);
    ws['!cols'] = wscols;

    var wopts:any = { bookType:'xlsx', bookSST:true, type:'binary', Props:{cellStyles:true, cellFormula:true}};

    var wbout = XLSX.write(this.wb,wopts);

    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([this.utilService.s2ab(wbout)],{type:"application/octet-stream"}), "reporte_campanas.xlsx");
    this.loader.fire(false);
  }

  // Envia Cronograma
  sendCrono(data){
    // console.log(data);
    this.clientS.getClientsById(data.client)
      .subscribe( response => {
        this.list_clients = response.result;
        this.actualcampaignid = data._id;
        this.storeModal.open();
      });    
  }

  sendClients() {
    this.loader.fire(true);
    this.sended = [];
    for (let i = 0; i < this.list_clients.length; i++) {
      if (this.list_clients[i].check) {
        this.sended.push(this.list_clients[i].email);
      }
    }
    this.campaignS.sendMail({campaign: this.actualcampaignid, mails: this.sended})
      .subscribe( response => {
        this.loader.fire(false);
        alert('Mail Enviado');
      }, error => {
        this.loader.fire(false);
        alert('Cliente no tiene asociado');
      });
  } 

  hide(data){
    this.loader.fire(true);
    this.campaignS.nothidden(data._id)
      .subscribe( response => {
        // console.log(response.result);
        this.loader.fire(false);
        this.ngOnInit();
      });
  }

  hideStoreModal() {
    this.storeModal.close();
  }

  onDisabled(data){
    var confirmation = confirm(`¿Está seguro que desea bloquear ${data.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.campaignS.delete(data._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          this.ngOnInit();
        }
      );
    }
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.campaignS.deleteever(data._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          this.ngOnInit();
        }
      );
    }
  }

  compare(a){
    if(a.enabled == true) return -1;
    else if (a.enabled == false) return 1;
    return 0;
  }

  date(a, b) {
    let date1 = new Date(a.created);
    let date2 = new Date(b.created);
    if(date1 < date2) return -1;
    else if (date2 < date1) return 1;
    return 0;
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
