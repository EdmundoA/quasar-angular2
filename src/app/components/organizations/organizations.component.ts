import { Component, OnInit } from '@angular/core';
import { OrganizationService } from "../../services/organization.service";
import { OrganizationEntity } from "../../entities/organization.entity";
import { Router } from "@angular/router";
import { UserEvent} from "../../services/user-event";
import { SubtitleEvent } from "../../services/subtitle-event";
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  providers: [
    SubtitleEvent,
    UserEvent,
    SessionService,
  ]
})
export class OrganizationsComponent implements OnInit {
  
  public subtitle: string = '';
  public permissions:any;

  constructor(
      private subtitleEvent: SubtitleEvent,
      private userEvent: UserEvent,
      private session: SessionService,
    ) { }

  ngOnInit() {
    this.registerBroadcast();
    this.permissions = this.session.getObject('permissions');
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });

    this.userEvent.on()
    .subscribe(value => {
      this.permissions = value;
    });
  }

}
