import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/clients.service';
import { LoaderEvent } from '../../services/loader-event';
import { UserEvent } from '../../services/user-event';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  providers: [
    LoaderEvent,
    UserEvent,
    SessionService,
  ]
})
export class ClientesListComponent implements OnInit {

	public clientes:Array<String> = [];
  public userFilter: any = { name: '' };
  public permissions:any;

  constructor(
		private serviceCli: ClientService,
    private loader: LoaderEvent,
    private userEvent: UserEvent,
    private session: SessionService,
	) { }


  ngOnInit() {
  	this.loader.fire(true);
    this.permissions = this.session.getObject('permissions');
    this.serviceCli.getClients()
    .subscribe( response => {
      this.clientes = response.result;
      this.clientes.sort(this.orderAlphabetic);
      this.loader.fire(false);
    });
  }

  orderAlphabetic(a,b){
    if(a.name < b.name) return -1;
    if(b.name < a.name) return 1;
    return 0;
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.serviceCli.delete(data._id)
      .subscribe(
        response => {
          this.ngOnInit();
        }
      );
    }
  }

}
