import { Component, OnInit, ViewChild } from '@angular/core';
import { LoaderEvent } from "../../services/loader-event";
import { CampaignsService } from '../../services/campaigns.service';
import { ActivatedRoute, Router } from "@angular/router";
import { OrganizationEntity } from "../../entities/organization.entity";
import { SubtitleEvent } from "../../services/subtitle-event";
import { ClientService } from "../../services/clients.service";
import { StoresService } from "../../services/stores.service";
import { ElementsService } from "../../services/elements.service";
import { Modal } from 'ngx-modal';
import { FileSelectDirective, FileDropDirective, FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { AppSettings } from "../../app.settings";
import { SessionService } from "../../services/session.service";
import { UserService } from "../../services/user.service";
import { TaskService } from "../../services/task.service";
import { InventoryService } from "../../services/inventories.service";
import { UtilService } from '../../services/util.service';
import { Lightbox } from 'angular2-lightbox';
//import * as XLSX from 'ts-xlsx';
import * as moment from "moment";
import * as _ from "lodash";
import {saveAs} from 'file-saver';

declare var XLSX:any;
type AOA = Array<Array<any>>;

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  providers: [
    SubtitleEvent, LoaderEvent, StoresService, UtilService
  ]
})
export class InventoryComponent implements OnInit {
  @ViewChild('importExcelModal') public importExcelModal: Modal;
  @ViewChild('imageNoQuasarModal') public imageNoQuasarModal: Modal;

  public userFilter: any = { elementType: '', category: '', categoryFather: '' };
  public stores: Array<String> = [];
  public allImplementers;
  public allCategories;
  public resum_inventory;
  public filterName;
  public implementsByStore: Array<String> = [];
  public conditionsByStore: Array<String> = [];
  public levels: Array<String> = [];
  public categories: Array<String> = [];
  public subCategories:any = [];
  public inventory;
  public inventoryFull;
  public showTable:boolean = false;
  public base = AppSettings.BASE_PATH;
  private _album: Array<any> = [];
  public filterBy;
  public noQuasar:any = [];
  public viewDisplay = '';
  public showStoreNames;
  public photoStore = [];
  public photoClient = [];
  public allPhotoClient = [];
  public allCategoryPhotoClient = [];
  public storeid = '';
  public noQuasarData = [];
  public noQuasarId;
  public imageNoQuasar:String = '';
  public URL = AppSettings.BASE_PATH;

  public categoryTree = {
    master: '',
    father: '',
    category: '',
  }

  public message2 = '';

  //PARA EXCEL
  public estados = ['Ingresado', 'Pendiente Aprobación Jefe', 'Observado por Jefe', 'Aprobado por Jefe', 'Pendiente de Aprobación Planeamiento', 'Aprobado por Planeamiento', 'Contrato de gestión enviado', 'Contrato de gestión aceptado'];
  public wb:any = {
    SheetNames:[],
    Sheets:{},
  };

  public permissionView: Boolean = true;

  public excelNoQuasar:AOA;

  constructor(
    private storeService: StoresService,
    private subtitleEvent: SubtitleEvent,
    private loader: LoaderEvent,
    private inventorieService: InventoryService,
    private campaingService: CampaignsService,
    private _lightbox: Lightbox,
    private utilService: UtilService,
    private session: SessionService
  ) {

  }

  public search: any = {
    from: moment(),
    to: moment()
  };

  public searchRG: any = {
    date: moment(),
  };

  public searchNoQuasar: any = {
    from: moment(),
    to: moment()
  };

  public pieOptions = {
    responsive: true,
    responsiveAnimationDuration: 0.5,
    maintainAspectRatio: true,
  };

  public pieColors = [
    {
      backgroundColor: [
        '#28e679', '#fe303b'
      ]
    }
  ];

  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: { su: 'dom', mo: 'lun', tu: 'mar', we: 'mie', th: 'jue', fr: 'vie', sa: 'sab' }
  };

  ngOnInit() {
    this.loader.fire(true);
    // this.loader.fire(true);
    this.subtitleEvent.fire('');
    // Validate permissions
    let rolePermission = this.session.getObject('user').role;
    if (rolePermission == "Supervisor" || rolePermission == "Jefe de categoria" || rolePermission == "Gerente") {
      this.permissionView = false;
    }
    this.storeService.getStores()
    .subscribe(response => {
      if (response.status === 200) {
        for (let i = 0; i < response.result.length; i++) {
          if (response.result[i].enabled) {
            this.stores.push(response.result[i]);
          }
        }
        this.campaingService.getImplementers()
        .subscribe( response => {
          this.allImplementers = response.result;
          this.campaingService.getCategories()
            .subscribe( response => {
              this.allCategories = response.result;;
            });
        });
      }
      // this.loader.fire(false);
    });

    this.campaingService.getCategories()
    .subscribe( response => {       
      for (let item of response.result) {
        if (item.categories.length > 0) {
          for (let category of item.categories) {
            for (let subcategories of category.subcategories) {
              //this.subCategories.push(subcategories);
            }
          }
        }
      }
    });

    this.sendSearchRG({});
  }

  hideImportExcelModal(){
    this.importExcelModal.close();
  }

  hideImageNoQuasarModal(){
    this.imageNoQuasarModal.close();
  }

  sendSearch(search) {
    this.loader.fire(true);
    this.inventorieService.searchInventory(search)
    .subscribe( response => {
      this.viewDisplay = '';
      this.inventoryFull = response.result;
      this.noQuasar = response.result.infoNoQuasar;
      this.allInit();
      this.campaignCluster();
      this.loader.fire(false);
    });
  }

  sendSearchRG(search) {
    this.loader.fire(true);
    this.inventorieService.getInventory(search)
    .subscribe(response => {
      this.loader.fire(false);
      this.resum_inventory = response.result;
      this.implementsByStore = response.result.implementsByStore;
      this.conditionsByStore = response.result.conditionsByStore;
      for (let i = 0; i < this.implementsByStore.length; i++) {
        this.implementsByStore[i]['total'] = this.implementsByStore[i]['others'] + this.implementsByStore[i]['quasar'];
      }        
      for (let j = 0; j < this.conditionsByStore.length; j++) {
        this.conditionsByStore[j]['total'] = this.conditionsByStore[j]['count'] + this.conditionsByStore[j]['notFound'] + this.conditionsByStore[j]['pendientes'];
      }

      this.implementsByStore.sort(this.totalfilter);
      this.conditionsByStore.sort(this.totalfilter);
    });
  }

  sendExcel(){
    this.importExcelModal.open();
  }

  sendImageNoQuasar(id){
    this.noQuasarId = id;
    this.imageNoQuasarModal.open();
  }

  openExcel(files){
    if(files.length != 1) throw new Error("Cannot upload multiple files on the entry");
    const scope = this;
    const reader = new FileReader();
    reader.onload = function (e:any) {
      /* read workbook */
      const bstr = e.target.result;
      //const wb = XLSX.read(bstr, {type:'binary'});
      var wb = XLSX.read(bstr, {type:'binary'});

      /* grab first sheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* save data to scope */
      scope.excelNoQuasar = (<AOA>(XLSX.utils.sheet_to_json(ws, {header:1})));
    };
    reader.readAsBinaryString(files[0]);
  }

  saveExcelNoQuasar(){
    this.loader.fire(true);
    var params = {
      data:'',
    }

    var data = [];
    for (var i = this.excelNoQuasar.length - 1; i >= 1; i--) {
      data.push(
      {
        brand: this.excelNoQuasar[i][0],
        client: this.excelNoQuasar[i][1],
        store: this.excelNoQuasar[i][2],
        element: this.excelNoQuasar[i][3],
        category: this.excelNoQuasar[i][4],
        material: this.excelNoQuasar[i][5],
        quantity: this.excelNoQuasar[i][6],
        responsable: this.excelNoQuasar[i][7],
        comment: this.excelNoQuasar[i][8],
        date: this.excelNoQuasar[i][9] + "T" + this.excelNoQuasar[i][10] + ":00.000Z",
      });
    };
    params.data = JSON.stringify(data);
    this.hideImportExcelModal();

    this.inventorieService.sendInventoryNoQuasarMasive(params)
    .subscribe(response => {
      this.loader.fire(false);
      this.sendSearchNoQuasar(this.searchNoQuasar);
    });
  }

  handleFileImageSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderImageLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderImageLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.imageNoQuasar = btoa(binaryString);
  }

  saveImageNoQuasar(){
    this.loader.fire(true);
    this.hideImageNoQuasarModal();
    this.inventorieService.sendInventoryNoQuasarImage({
      file: this.imageNoQuasar,
      noquasar: this.noQuasarId,
    })
    .subscribe(response => {
      this.sendSearchNoQuasar(this.searchNoQuasar);
      this.loader.fire(false);
    });
  }

  sendSearchNoQuasar(search) {
    this.loader.fire(true);
    this.inventorieService.searchInventoryNoQuasar(new Date(search.from).toISOString(), new Date(search.to).toISOString())
    .subscribe( response => {
      this.noQuasarData = response.result;
      for (var i = this.noQuasarData.length - 1; i >= 0; i--) {
        this.noQuasarData[i].dateRegister = (this.noQuasarData[i].dateRegister)?this.noQuasarData[i].dateRegister:this.noQuasarData[i].date;
        var tDate = this.noQuasarData[i].dateRegister;
        this.noQuasarData[i].dateRegister = tDate.split('T')[0] + ' ' + tDate.split('T')[1].split('.')[0];
      }
      this.noQuasarData.sort(this.sortBrandElement);
      this.loader.fire(false);
    });
  }

  sortBrandElement(a, b){
    if(a.element[0].name < b.element[0].name){
      return -1;
    }
    if(b.element[0].name < a.element[0].name){
      return 1;
    }
    if(a.element[0].name == b.element[0].name){
      if(a.brand < b.brand){
        return -1;
      }
      if(b.brand < a.brand){
        return 1;
      }
    }
    return 0;
  }

  allInit(){
    this.showTable = true;
    this.inventory = JSON.parse(JSON.stringify(this.inventoryFull));
    for (let i = 0; i < this.inventory.photos.length; i++) {
      for (let j = 0; j < this.inventory.photos[i].stores.length; j++) {
        for (let h = 0; h < this.inventory.photos[i].stores[j].elements.length; h++) {
          this.inventory.photos[i].stores[j].elements[h].inventories.reverse();  
        }
      }
    }
  }

  showfilters() {
    this.initPhotoClient();
    this.userFilter.elementType = ''; 
    this.userFilter.category = ''; 
    this.userFilter.categoryFather = '';
    if (this.viewDisplay == '') {
      this.showStoreNames = false;
    } else {
      this.filterBy = '';
      if (this.viewDisplay == 'implementer') {
        this.filterName = 'Implementador: ';
        this.showStoreNames = false;
      }
      else if (this.viewDisplay == 'category') {
        this.filterName = 'Categoría Master: ';
        this.showStoreNames = false;
      }
      else if (this.viewDisplay == 't') {
        this.filterName = 'Tienda: ';
      }
    }
  }

  campaignCluster(){
    this.photoClient = [];
    for (var i = 0; i < this.inventoryFull.photos.length; i++) {
      var photo = this.inventoryFull.photos[i];
      for (var j = 0; j < photo.stores.length; j++) {
        var store = photo.stores[j];
        for (var k = 0; k < store.elements.length; k++) {
          var element = store.elements[k];
          for (var ll = element.inventories.length - 1; ll >= 0; ll--) {
            var inventory = element.inventories[ll];
            inventory.store = store.storeName;
            inventory.campaign = element.campaign;
            var tClient = this.existClient(photo._id);
            if(!tClient){
              var ObjCategoty = this.getCategory(element.category);
              this.photoClient.push({
                id:photo._id,
                elements: [{
                  id: element.element,
                  code: element.code.split('-')[0],
                  subcategory: element.category,
                  category: ObjCategoty.category,
                  categoryFather: ObjCategoty.categoryFather,
                  campaign: element.campaign,
                  unique: element.campaign + ' ' + element.element,
                  elementClass: element.elementClass,
                  elementType: element.elementType,
                  elementName: element.elementName,
                  inventories:[inventory],
                }]
              });
            }
            else{
              var tElement = this.existElementInClient(element.campaign + ' ' + element.element, tClient);
              if(tElement){
                var lastElement = tElement.inventories.length;
                if(this.dateIsHigher(inventory.date, tElement.inventories[0].date)){
                  tElement.inventories.unshift(inventory)
                }
                else if(this.dateIsHigher(inventory.date, tElement.inventories[lastElement - 1].date)){
                  tElement.inventories[lastElement] = JSON.parse(JSON.stringify(tElement.inventories[lastElement - 1]));
                  tElement.inventories[lastElement - 1] = inventory;
                }
                else{
                  tElement.inventories.push(inventory);
                }                
              }
              else{
                var ObjCategoty = this.getCategory(element.category);
                tClient.elements.push({
                  id: element.element,
                  code: element.code.split('-')[0],
                  subcategory: element.category,
                  category: ObjCategoty.category,
                  categoryFather: ObjCategoty.categoryFather,
                  campaign: element.campaign,
                  unique: element.campaign + ' ' + element.element,
                  elementClass: element.elementClass,
                  elementType: element.elementType,
                  elementName: element.elementName,
                  inventories:[inventory],
                })
              }
            }
          }          
        }
      }
    }
    this.allPhotoClient = JSON.parse(JSON.stringify(this.photoClient));
  }

  initPhotoClient(){
    this.photoClient = JSON.parse(JSON.stringify(this.allPhotoClient));
  }

  dateIsHigher(d1, d2){
    var time1 = (new Date(d1)).getTime();
    var time2 = (new Date(d2)).getTime();

    return time1 > time2;
  }

  getCategory(category){
    for (let o = 0; o < this.allCategories.length; o++) {
      for (let u = 0; u < this.allCategories[o].categories.length; u++) {
        for (let y = 0; y < this.allCategories[o].categories[u].subcategories.length; y++) {
          if (category == this.allCategories[o].categories[u].subcategories[y]) {
            return {category:this.allCategories[o].categories[u].name, categoryFather:this.allCategories[o].name};
          }
        }
      }
    }
  }

  existClient(clientid){
    for (var i = 0; i < this.photoClient.length; i++) {
      if(this.photoClient[i].id == clientid){return this.photoClient[i];}
    }
    return false;
  }

  existElementInClient(elementid, client){
    for (var i = 0; i < client.elements.length; i++) {
      if(client.elements[i].unique == elementid){return client.elements[i];}
    }
    return false;
  }

  changeStore(){
    this.initPhotoClient();
    if(this.storeid == ''){
      this.showStoreNames = false;
      return;
    }

    this.showStoreNames = true;
    for (var i = this.photoClient.length - 1; i >= 0; i--) {
      var photo = this.photoClient[i];
      for (var j = photo.elements.length - 1; j >= 0; j--) {
        var element = photo.elements[j];
        for (var k = element.inventories.length - 1; k >= 0; k--) {
          if(element.inventories[k].store != this.storeid){
            element.inventories.splice(k, 1);
          }
        }
        if(element.inventories.length == 0){
          photo.elements.splice(j, 1);
        }
      }
      if(photo.elements.length == 0){
        this.photoClient.splice(i, 1);
      }
    }
  }

  editImage(){

  }

  changeCategory(){
    this.initPhotoClient();
    if(this.userFilter.categoryFather == ''){
      return;
    }
    this.subCategories = this.allCategories.filter(category => category.name == this.userFilter.categoryFather)[0].categories;
    this.userFilter.category = '';

    for (var i = this.photoClient.length - 1; i >= 0; i--) {
      var photo = this.photoClient[i];
      for (var j = photo.elements.length - 1; j >= 0; j--) {
        if(photo.elements[j].categoryFather != this.userFilter.categoryFather){
          photo.elements.splice(j, 1);
        }
      }
      if(photo.elements.length == 0){
        this.photoClient.splice(i, 1);
      }
    }
    this.allCategoryPhotoClient = JSON.parse(JSON.stringify(this.photoClient));
  }

  clusterNoQuasar(){
    this.photoClient = [];
    for (var i = this.noQuasar.length - 1; i >= 0; i--) {
      //if(!this.noQuasar[i].client[0] || !this.noQuasar[i].element[0]){break};
      var tClient = this.existClient(this.noQuasar[i].client[0].name)
      var inventory = {
        elementType: this.noQuasar[i].element[0].name,
        picture: this.noQuasar[i].picture,
        campaign: this.noQuasar[i].element[0].name,
        date: this.noQuasar[i].date,
        store: this.noQuasar[i].store[0].name,
      };
      if(tClient){
        var tBrand = this.existElementInClient(this.noQuasar[i].brand, tClient);
        if(tBrand){
          var lastBrand = tBrand.inventories.length;
          if(this.dateIsHigher(inventory.date, tBrand.inventories[0].date)){
            tBrand.inventories.unshift(inventory)
          }
          else if(this.dateIsHigher(inventory.date, tBrand.inventories[lastBrand - 1].date)){
            tBrand.inventories[lastBrand] = JSON.parse(JSON.stringify(tBrand.inventories[lastBrand - 1]));
            tBrand.inventories[lastBrand - 1] = inventory;
          }
          else{
            tBrand.inventories.push(inventory);
          }  
        } 
        else{
          tClient.elements.push({
            code: this.noQuasar[i].brand,
            id: this.noQuasar[i].brand,
            category: ' ',
            categoryFather: ' ',
            campaign: this.noQuasar[i].element[0].name,
            unique: this.noQuasar[i].brand,
            elementType: ' ',
            inventories:[inventory],
          })
        }
      }
      else{
        this.photoClient.push({
          id:this.noQuasar[i].client[0].name,
          elements: [{
            code: this.noQuasar[i].brand,
            id: this.noQuasar[i].brand,
            category: ' ',
            categoryFather: ' ',
            campaign: this.noQuasar[i].element[0].name,
            unique: this.noQuasar[i].brand,
            elementType: ' ',
            inventories:[inventory],
          }]
        });;
      }
    }//this.noQuasar
  }

  inventoryView() {
    if (this.filterBy == '') {
      this.sendSearch(this.search);
    } else {
      this.inventory = JSON.parse(JSON.stringify(this.inventoryFull));
      if (this.viewDisplay == 'implementer') {
        if (this.filterBy == 'Quasar') {
          this.initPhotoClient();
        } else {
          this.clusterNoQuasar();
        }
      }      
    }    
  }

  categoryFilter() {
    this.photoClient = JSON.parse(JSON.stringify(this.allCategoryPhotoClient));
    if(this.userFilter.category == ''){
      return;
    }

    for (var i = this.photoClient.length - 1; i >= 0; i--) {
      var photo = this.photoClient[i];
      for (var j = photo.elements.length - 1; j >= 0; j--) {
        if(photo.elements[j].category != this.userFilter.category){
          photo.elements.splice(j, 1);
        }
      }
      if(photo.elements.length == 0){
        this.photoClient.splice(i, 1);
      }
    }
  }

  createWS(name){
    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};

    return this.wb.Sheets[name];
  }

  exportReport(){
    this.loader.fire(true);
    if(!this.inventory  || this.inventory.photos.length == 0){
      this.loader.fire(false);
      return;
    }

    var dataExcel = JSON.parse(JSON.stringify(this.inventory.photos));

    var dataExcelNoQuasar = JSON.parse(JSON.stringify(this.inventory.infoNoQuasar));

    var fila = 1;
    var col = 0;
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };
    var ws = this.createWS('Report');
    var wscols = [
      {wch:30},
      {wch:20},
      {wch:20},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:25},
      {wch:30},
      {wch:30},
      {wch:25},
      {wch:25},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:30},
      {wch:90},
      {wch:90},
      {wch:30},
      {wch:90}
    ];
    ws['!merges'] = [];

    fila++;

    let headers = ['FECHA DE INVENTARIO', 'AÑO', 'MES', 'OPERADOR', 'SUPERVISOR', 'IMPLEMENTADOR', 'COD. ELEMENTO',
                'CANTIDAD', 'CLASIFICACIÓN ELEMENTO', 'ELEMENTO', 'TIPO ELEMENTO', 'CLIENTE', 'CAMPAÑA', 'CADENA', 
                'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGION', 'IN', 'OUT', 'IN 2', 'OUT 2', 'STATUS INVENTARIO',
                'SUBMOTIVO', 'OBSERVACIÓN', 'UBICACIÓN', 'CATEGORIA', 'MATERIAL', 'LINK FOTO'];

    for (var k = 0; k < headers.length; k++) {
      ws[this.utilService.getLetter(k) + fila] = {t:'s', v:headers[k], s:AppSettings.fillCabecera};
    }
        
    fila++;

    col = 0;

    for (var k = 0; k < dataExcel.length; k++) {
      for (var l = 0; l < dataExcel[k].stores.length; l++) {
        for (var m = 0; m < dataExcel[k].stores[l].elements.length; m++) {
          for (var n = 0; n < dataExcel[k].stores[l].elements[m].inventories.length; n++) {

            let dateArray = dataExcel[k].stores[l].elements[m].inventories[n].date.split('-');
            let year = dateArray[0];
            // To use month in spanish
            moment.locale('es');
            let month = moment().month(dateArray[1] - 1).format('MMMM');

            let operator = (dataExcel[k].stores[l].elements[m].inventories[n].operator)?
                            dataExcel[k].stores[l].elements[m].inventories[n].operator.firstname + ' ' + 
                            dataExcel[k].stores[l].elements[m].inventories[n].operator.lastname : '';

            // Is necesary validate multiple properties, TODO: figure out a better way to make that.
            let supervisor = (dataExcel[k].stores[l].elements[m].inventories[n].operator)?
                             (dataExcel[k].stores[l].elements[m].inventories[n].operator.permissions && dataExcel[k].stores[l].elements[m].inventories[n].operator.permissions.supervisor? dataExcel[k].stores[l].elements[m].inventories[n].operator.permissions.supervisor.firstname + ' ' + dataExcel[k].stores[l].elements[m].inventories[n].operator.permissions.supervisor.lastname : dataExcel[k].stores[l].elements[m].inventories[n].operator.role):'-';

            let city = this.getDataProperty(dataExcel[k].stores[l], 'store.location.district.district');

            let data = [ 

                        this.getDataProperty(dataExcel[k].stores[l].elements[m].inventories[n], 'date').split('T')[0],
                        year,
                        month,
                        operator,
                        supervisor,
                        this.getDataProperty(dataExcel[k].stores[l].elements[m].inventories[n], 'implementer'),
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'code'),
                        '1', // Cuando es no quasar, sale cantidad
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'elementClass'),
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'elementName'),
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'elementType'),
                        this.getDataProperty(dataExcel[k], '_id'), // client
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'campaign'),
                        this.getDataProperty(dataExcel[k].stores[l], 'store.reference.chain.name'),
                        this.getDataProperty(dataExcel[k].stores[l], 'store.sapCode'),
                        this.getDataProperty(dataExcel[k].stores[l], 'storeName'),
                        city?city:this.getDataProperty(dataExcel[k].stores[l], 'store.location.province.name'),
                        this.getDataProperty(dataExcel[k].stores[l], 'store.region'),
                        this.getDataProperty(dataExcel[k].stores[l], 'programed.install').split('T')[0],
                        this.getDataProperty(dataExcel[k].stores[l], 'programed.uninstall').split('T')[0],
                        this.getDataProperty(dataExcel[k].stores[l], 'installed.date').split('T')[0],
                        this.getDataProperty(dataExcel[k].stores[l], 'uninstalled.date').split('T')[0],
                        this.getDataProperty(dataExcel[k].stores[l].elements[m].inventories[n], 'ok'),
                        '-', // Submotivo, Solo no quasar
                        this.getDataProperty(dataExcel[k].stores[l].elements[m].inventories[n], 'comment'),
                        this.getDataProperty(dataExcel[k].stores[l], 'store.address'),
                        this.getDataProperty(dataExcel[k].stores[l].elements[m], 'category'),
                        '-', // Material, Solo no quasar
                        (dataExcel[k].stores[l].elements[m].inventories[n].picture) ? 
                        '=HIPERVINCULO("' + AppSettings.BASE_PATH + dataExcel[k].stores[l].elements[m].inventories[n].picture + '","link")': ''
                      ];

            for (var o = 0; o < data.length; o++) {
              ws[this.utilService.getLetter(o) + fila] = {t:'s', v:data[o], s:AppSettings.fillElement};
              col++;
            }

            fila++;
            col = 0;
          }
        }
      }
      
    }

    // Fill data to excel for NoQuasar

    for (var k = 0; k < dataExcelNoQuasar.length; k++) {

      let dateArray = dataExcelNoQuasar[k].date.split('-');
      let year = dateArray[0];
      // To use month in spanish
      moment.locale('es');
      let month = moment().month(dateArray[1]).format('MMMM');
      let city = this.getDataProperty(dataExcelNoQuasar[k], 'store[0].location.district.name');

      let operator = this.getDataProperty(dataExcelNoQuasar[k], 'operator');
      let supervisor = (operator.permissions && operator.permissions.supervisor)?operator.permissions.supervisor.firstname:(operator?operator.role:'-');

      let data = [ 
                    this.getDataProperty(dataExcelNoQuasar[k], 'date').split('T')[0],
                    year,
                    month,
                    operator?operator.firstname:'Sin Operador',  // Operator
                    supervisor,  // Supervisor 
                    'No Quasar',  // Implementer
                    '-',  // Element code
                    this.getDataProperty(dataExcelNoQuasar[k], 'quantity'),  // Cuando es no quasar, sale cantidad
                    this.getDataProperty(dataExcelNoQuasar[k], 'element[0].class'), //OJO
                    this.getDataProperty(dataExcelNoQuasar[k], 'element[0].name'),  // name element
                    'Elemento no quasar',  //Element type
                    this.getDataProperty(dataExcelNoQuasar[k], 'client[0].name'),
                    '-', //campaign name
                    this.getDataProperty(dataExcelNoQuasar[k], 'store[0].reference.chain.name'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'store[0].sapCode'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'store[0].name'),
                    city?city:this.getDataProperty(dataExcelNoQuasar[k], 'store[0].location.province.name'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'store[0].region'),
                    '-',  // Programed install
                    '-',  // Programed uninstall,
                    '-',  // Installed date
                    '-',  // Uninstalled date
                    this.getDataProperty(dataExcelNoQuasar[k], 'status'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'submotive'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'comment'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'store[0].address'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'category'),
                    this.getDataProperty(dataExcelNoQuasar[k], 'material'),
                    (dataExcelNoQuasar[k].picture) ? 
                    '=HIPERVINCULO("' + AppSettings.BASE_PATH+ dataExcelNoQuasar[k].picture + '","link")': '-',
                 ];

      for (var o = 0; o < data.length; o++) {
        ws[this.utilService.getLetter(o) + fila] = {t:'s', v:data[o], s:AppSettings.fillElement};
        col++;
      }
      fila++;
      col = 0;

    }

    ws["!ref"] = "A1:AZ" + (fila+1);
    ws['!cols'] = wscols;

    var wopts:any = { bookType:'xlsx', bookSST:false, type:'binary'};

    var wbout = XLSX.write(this.wb,wopts);

    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([this.utilService.s2ab(wbout)],{type:"application/octet-stream"}), "reporte_inventarios.xlsx");
    this.loader.fire(false);
  }

  open(element: any, index: number) {
    this._album = [];
    for (let j = 0; j < element.inventories.length; j++) {
      const d = moment(element.inventories[j].date).format("YYYY-MM-DD");
      const src = this.base + element.inventories[j].picture;
      const caption = ((element.code == ' ')?element.inventories[j].code:element.code) + '-' + element.inventories[j].campaign + '-' + element.inventories[j].store + '-' + element.inventories.length + '-' + d;
      const album = {
        src: src,
        caption: caption,
      };
      this._album.push(album);
    }

    this._lightbox.open(this._album, index);
  }

  compare(a, b){
    let date1;
    let date2;
    
    if(date1 > date2) return -1;
    else if (date2 > date1) return 1;
    return 0;
  }

  totalfilter(a, b) {
    if (a.total > b.total) return -1;
    else if( b.total > a.total) return 1;
    return 0;
  }

  /**
   *  To figure out if an object have all properties passed
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties) {
    // Get all properties to analize
    const allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {

      let propertyRaw = allProperties[i];
      const indexOpen = allProperties[i].indexOf('[');
      const indexClose = allProperties[i].indexOf(']');

      if (indexOpen != -1) {
        propertyRaw = propertyRaw.substr(0, indexOpen);
      }

      if (!obj || !obj.hasOwnProperty(propertyRaw)) {
        return '';
      }
      if (indexOpen != -1) {
        const indexArray = parseInt(allProperties[i].substr(indexOpen + 1, indexClose - 1));
        obj = obj[propertyRaw][indexArray];
      } else {
        obj = obj[allProperties[i]];
      }
 
    }
    return obj;
  }
}
