import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { OrganizationEntity } from '../../entities/organization.entity';
import { LoaderEvent } from '../../services/loader-event';
import { SubtitleEvent } from "../../services/subtitle-event";
import { OrganizationService } from "../../services/organization.service";
import { ChainsService } from "../../services/chains.service";
import { ChainEntity } from "../../entities/chain.entity";
import { StoresService } from "../../services/stores.service";

@Component({
  selector: 'app-chains-create',
  templateUrl: './chains-create.component.html',
  providers: [
    FormBuilder,
    LoaderEvent,
    ChainsService,
    SubtitleEvent,
    OrganizationService,
    StoresService
  ]
})

export class ChainsCreateComponent implements OnInit {

  public strforOrga: Array<String> = [];
  public orga: Array<String> = [];
  public chainForm: FormGroup;
  public sub: any;
  public chain: ChainEntity;
  public OC: String;
  public creating: Boolean = true;

  constructor(
    private orgaService: OrganizationService,
    private _fb: FormBuilder,
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private chainService: ChainsService,
    private storeService: StoresService,

  ) { }

  ngOnInit() {
  	this.chainForm = this._fb.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      orga: ['', Validators.required],
    });

    this.sub = this.routeParams.params.subscribe(
      params => {
        this.loader.fire(true);
        if (params['id']) {
          const id = params['id'];
          this.chainService.detail(id)
            .subscribe(response => {
              if (response.status === 200) {
                this.chain = response.result;
                this.OC = response.result.reference.organization._id;
                this.storeService.getStoresById(this.chain._id)
                  .subscribe(
                    response => {
                      this.strforOrga = response.result;
                    }
                  );
                this.refreshChain();
                this.creating = false;
                this.loader.fire(false);
              }else {

              }

              this.loader.fire(false);
            });
          this.subtitleEvent.fire('Editar');

        }else{
          this.subtitleEvent.fire('Crear');
          this.loader.fire(false);
        }
      }
    );
    this.allOrga();

  }

  refreshChain(){
    this.chainForm.patchValue(this.chain);
    this.chainForm.controls['orga'].setValue(this.OC);
  }

  allOrga() {
    this.loader.fire(true);
    this.orgaService.getAll()
      .subscribe(
        response => {
          this.orga = response.result;
          this.loader.fire(false);
        }
      );
  }


  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.storeService.delete(data._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          // console.log(response);
          this.ngOnInit();
        }
      );
    }
  }
  
  onSubmit(model: OrganizationEntity, isValid: Boolean) {
    this.loader.fire(true);
    let params = {
          name: model.name,
          shortName: model.shortName,
          reference: {
            organization: model.orga,
          }
        }

    if (isValid) {
      if (this.creating) {
        

        this.chainService.createChains(params)
          .subscribe(
            response => {
              this.loader.fire(false);
              this.router.navigate(['chains']);
            }
          );
      } else {
        this.chainService.update(this.chain._id, params)
          .subscribe(
            response => {
              this.loader.fire(false);
              this.router.navigate(['chains']);
            }
          );
      }
      
    }
  }

}
