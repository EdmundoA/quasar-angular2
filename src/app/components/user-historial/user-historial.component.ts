import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderEvent } from '../../services/loader-event';
import { SubtitleEvent } from "../../services/subtitle-event";
import { UserService } from '../../services/user.service';
import { RouteService } from '../../services/route.service';
import { AppSettings } from '../../app.settings';
import * as moment from 'moment';

@Component({
  selector: 'app-user-historial',
  templateUrl: './user-historial.component.html',
  providers: [
  	UserService, LoaderEvent
  ]
})
export class UserHistorialComponent implements OnInit {

	public URL = AppSettings.BASE_PATH;
  public data: Boolean = false;

  public datePickerConfig = {
      closeOnSelect: true,
      disableKeypress: true,
      firstDayOfWeek: 'mo',
      weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'},
      monthsNames: {jan: 'ene', feb: 'feb'}
  };

	public sub:any;
	public name;

	public id;

	public search = {
	    from: moment(),
	    to: moment(),
	};

	public paths:Array<any> = [];

  constructor(
  		private userService: UserService,
  		private router: Router,
	    private routeParams: ActivatedRoute,
	    private loader: LoaderEvent,
	    private subtitleEvent: SubtitleEvent,
	    private routeS: RouteService
  	) { }

  ngOnInit() {

  	this.sub = this.routeParams.params.subscribe(
  			params => {
  				if (params['id']) {
  					this.subtitleEvent.fire('Historial');
  					this.id = params['id'];
  					this.userService.getUser(this.id)
  						.subscribe( response => {
  							this.name = response.result.firstname + response.result.lastname;
					});
  					
  				}
  			}
  		)
  	

  }

  sendDate() {
  	this.loader.fire(true);
    console.log(this.search.from.locale());
  	this.routeS.getRoute(this.id, {from: this.search.from.format().split('T')[0] + "T00:00:00-05:00", to: this.search.to.format().split('T')[0] + 'T23:59:59-05:00'})
		.subscribe( response => {
			// console.log(response);
      this.data = true;
			this.paths = response.result.navigation.reverse();
			this.loader.fire(false);
			
		});
  }

}
