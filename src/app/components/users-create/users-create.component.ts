import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { UserService } from '../../services/user.service';
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { ClientService } from "../../services/clients.service";
import { OrganizationService } from "../../services/organization.service";
import { CampaignsService } from "../../services/campaigns.service";

@Component({
  selector: 'app-user-create',
  templateUrl: './users-create.component.html',
  providers: [
    UserService,
    LoaderEvent,
    SessionService,
    SubtitleEvent,
    ClientService,
    OrganizationService,
    CampaignsService
  ]
})
export class UsersCreateComponent implements OnInit {

  public roles = [];

  public message = '';
  public messageSucces = '';
  
  public showTypeClient: Boolean = false;
  public sub: any;
  public user;
  public creating: Boolean = true;
  public showPer: Boolean = false;
  public showOrga: Boolean = false;
  public allCate: Boolean = false;
  public showOperator: Boolean = false;
  public checkcliente: String = '';
  public clients: Array<any> = [];
  public orga: Array<any> = [];
  public categories: Array<any> = [];
  public superRol;


  public userForm = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    password2: '',
    gender: '',
    role: '',
    permissions: {
      client: '',
      organization: null,
      categories: [],
      supervisor: '',
    }
  };


  constructor(
    private userService: UserService,
    private clientS: ClientService,
    private session: SessionService,
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private orgaS: OrganizationService,
    private campaignS: CampaignsService,
  ) { }

  ngOnInit() {
    this.Roles();
  }

  datauser(){
    this.sub = this.routeParams.params.subscribe(
      params => {

        if (params['id']) {
          this.loader.fire(true);
          const id = params['id'];
          this.userService.getUser(id)
            .subscribe(response => {
              if (response.status === 200) {
                this.user = response.result;
                // console.log(this.user);
                this.userForm = this.user;
                this.creating = false;
                if (this.user.role === 'Admin') {
                  this.showPer = true;
                } else if (this.user.role === 'Cliente') {
                  this.showTypeClient = true;
                } else if (this.user.role === 'Gerente' || this.user.role === 'Jefe de categoria') {
                  this.showOrga = true;
                  this.allCate = true;
                  this.compareCategoriesUser();
                } else if (this.user.role === 'Operador') {
                  this.showOperator = true;
                  this.userService.rolDetail("Supervisor")
                  .subscribe( response => {
                    this.superRol = response.result;
                  });
                }
                this.loader.fire(false);
              } else {
                this.router.navigate(['users']);
              }
            });
          this.subtitleEvent.fire('Editar');

        } else {
          this.subtitleEvent.fire('Crear');
        }
      }
    );
  }

  compareCategoriesUser(){
    for (var index =  0; index < this.categories.length; index++) {
      for (let i = 0; i < this.categories[index].categories.length; i++) {
        for (let j = 0; j < this.categories[index].categories[i].subcategories.length; j++) {
          var nameSubcategory = this.categories[index].categories[i].subcategories[j].name;
          this.categories[index].categories[i].subcategories[j].checked = (this.userForm.permissions.categories.filter(category => category == nameSubcategory).length > 0);
          this.categories[index].categories[i].checked = this.checkedSub(index, i);
          this.categories[index].checked = this.checkedAll(index);
          //console.log(this.categories[index].categories[i].subcategories[j]);
        }
      }
    }    
  }

  clientCheck() {
    // console.log(this.userForm.role);
    if (this.userForm.role == "Cliente") {
      this.showTypeClient = true;
      this.showOrga = false;
      this.allCate = false;
      this.showOperator = false;
    } else if (this.userForm.role == "Gerente" || this.userForm.role == "Jefe de categoria") {
      this.showOrga = true;
      this.showTypeClient = false;
      this.allCate = true;
      this.showOperator = false;

    } else if ( this.userForm.role == "Operador") {
      this.showOperator = true;
      this.showTypeClient = false;
      this.showOrga = false;
      this.allCate = false;
      this.userService.rolDetail("Supervisor")
        .subscribe( response => {
          // console.log(response);
          this.superRol = response.result;
        });
    }
    else {
      this.showOperator = false;
      this.showTypeClient = false;
      this.showOrga = false;
      this.allCate = false;

    }
  }

  checkAll(index) {
    for (let i = 0; i < this.categories[index].categories.length; i++) {
      this.categories[index].categories[i].checked = this.categories[index].checked;
      for (let j = 0; j < this.categories[index].categories[i].subcategories.length; j++) {
        this.categories[index].categories[i].subcategories[j].checked = this.categories[index].checked;
      }
    }
  }

  checkSub(index, c) {
    for (let k = 0; k < this.categories[index].categories[c].subcategories.length; k++) {
      this.categories[index].categories[c].subcategories[k].checked = this.categories[index].categories[c].checked;
    }
    this.categories[index].checked = this.checkedAll(index);
  }

  checkSubTwo(index, c, s){
    this.categories[index].categories[c].checked = this.checkedSub(index, c);
    this.categories[index].checked = this.checkedAll(index);
  }

  checkedAll(index){
    for (let i = 0; i < this.categories[index].categories.length; i++) {
      for (let j = 0; j < this.categories[index].categories[i].subcategories.length; j++) {
        if(!this.categories[index].categories[i].subcategories[j].checked){
          return false;
        }
      }
      if(!this.categories[index].categories[i].checked){
        return false;
      }
    }
    return true;
  }

  checkedSub(index, c){
    for (let k = 0; k < this.categories[index].categories[c].subcategories.length; k++) {
      if(!this.categories[index].categories[c].subcategories[k].checked){
        return false;
      }
    }
    return true;
  }


  Roles() {
    this.loader.fire(true);
    this.userService.getRol()
      .subscribe(response => {
        this.loader.fire(false);
        this.roles = response.result;
        this.clientS.getClients()
          .subscribe(response => {
            this.clients = response.result;
            // console.log(response);
            this.orgaS.getAll()
              .subscribe(response => {
                this.orga = response.result;
                // console.log(response.result);
                this.campaignS.getCategories()
                  .subscribe(response => {
                    let result = response.result;

                    this.categories = [];

                    result.forEach(master => {
                      let m = {
                        name: master.name,
                        checked: false,
                        categories: [],
                      };

                      this.categories.push(m);

                      master.categories.forEach(category => {
                        let c = {
                          name: category.name,
                          checked: false,
                          subcategories: []
                        };

                        m.categories.push(c);

                        category.subcategories.forEach(subcategory => {
                          let s = {
                            name: subcategory,
                            checked: false
                          }
                          c.subcategories.push(s);
                        });
                      });
                    });

                    this.datauser();
                  });
              });
          });

      });
  }

  onSubmit(model) {
    
    // console.log(model);
    let user = model.role;

    this.message = '';
    this.messageSucces = '';

    if (this.creating) {


      if (model.password.trim().length === 0) {
        this.message = 'Debes ingresar la contraseña';
      } else if (model.password2.trim().length === 0) {
        this.message = 'Debes repetir la contraseña';
      } else if (model.password !== model.password2) {
        this.message = 'Las contraseñas no coinciden';
      }
    } else {

      // console.log('model.password', model.password);
      if (model.password) {
        if (model.password.trim().length > 0 || model.password2.trim().length > 0) {
          if (model.password !== model.password2) {
            this.message = 'Las contraseñas no coinciden';
          }
        }
      }else{
        // console.log("no hagas nada");
      }

    }
    model.permissions.categories = [];

    if (user == 'Gerente' || user == 'Jefe de categoria') {
      model.permissions.supervisor = null;
      this.categories.forEach(master => {
        master.categories.forEach(category => {
          category.subcategories.forEach(subcategory => {
            if (subcategory.checked) {
              model.permissions.categories.push(subcategory.name);
            }
          })
        });
      });
      // console.log(model);
    }

    if (user != 'Cliente') {
      delete model.permissions.client;
    }

    if (user != 'Operador') {
      model.permissions.supervisor = null;
    }



    if (this.message.trim().length === 0) {
      if (this.creating) {
        this.loader.fire(true);
        
        // si está creando se hace la llamada POST
        this.userService.create(model)
          .subscribe(
          response => {
            this.loader.fire(false);

            if (response.status === 201) {
              // console.log(response.result);
              if (user === 'Admin') {
                this.showPer = true;
                this.router.navigate(['users/' + response.result._id + '/permisos']);
              } else {
                this.router.navigate(['users']);
              }

            } else {
              alert(response.message);
            }
          },
          error => {
            // console.log(error);
          }
          );
      } else {
        this.loader.fire(true);

        // si está actualizando se hace la llamada PUT
        this.userService.update(this.user._id, model)
          .subscribe(
          response => {
            this.loader.fire(false);
            
            // console.log(response);
            if (response.status === 209) {
              this.messageSucces = 'Datos actualizados';    
              this.message = '';    
              
              if (user === 'Admin') {
                this.showPer = true;
              } else {
                this.showPer = false;
              }
            } else {
              // alert(response.message);
            }
          },
          error => {
            // console.log(error);
          }
          );
      }
    }
  }
}
