import { Component, OnInit } from '@angular/core';
import { SubtitleEvent } from "../../services/subtitle-event";

import { UtilService } from '../../services/util.service';
import { AppSettings } from "../../app.settings";
import { LoaderEvent } from '../../services/loader-event';
import { MeasurementService } from '../../services/measurement.service';
import { saveAs } from 'file-saver';
import * as moment from 'moment';

declare var XLSX: any;

@Component({
  selector: 'app-measurement',
  templateUrl: './measurement.component.html',
  providers: [
    SubtitleEvent,
    UtilService,
    MeasurementService,
    LoaderEvent,
  ]
})
export class MeasurementComponent implements OnInit {

  public subtitle: string = '';

  // To define stuff excel
  // Variable for sheet definition
  public wb: any = {
    SheetNames:[],
    Sheets:{},
  };

  public elementsTypes:any = [];

  constructor(
    private subtitleEvent: SubtitleEvent,
    private utilService: UtilService,
    private measurementService: MeasurementService,
    private loader:LoaderEvent,
  ) { }

  ngOnInit() {
    this.registerBroadcast();
    this.measurementService.getAll_types()
    .subscribe(response => {
      console.log(response.result);
      this.elementsTypes = response.result;
    });
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });
  }

  /**
   *  To create a worksheet, note is a sheet not a excel
   *
   *  @param {string} name: name sheet excel
   */

  createWS(name){

    this.wb.SheetNames.push(name);
    this.wb.Sheets[name] = {};
    this.wb.cellFormula = true;

    return this.wb.Sheets[name];
  }

  /**
   *  Funcion when button Report is clicked, It calls to get all information to generate the excel
   */

  exportReport() {
    this.loader.fire(true);
    this.measurementService.getDataExcel()
    .subscribe(data => {
      this.generateReport(data)
    });
  }
  
  /**
   *  To generate the excel with the data passed
   *
   *  @param {array} dataExcel: all data to print in excel
   */

  generateReport(dataExcel) {

    // Clean the last excel
    this.wb = {
      SheetNames:[],
      Sheets:{},
    };

    let ws = this.createWS('Report');
    let wscols = [];
    const columsWidth = 35;
    const rowOffset = 2;
    ws['!merges'] = [];

    //  Headers of headers
    let headersH = [];
    let mergeCols = [];
    let propertyMeasurements = [];
    let lastCol = 22;

    for (var et = this.elementsTypes.length - 1; et >= 0; et--) {
      let countMeasurements = this.elementsTypes[et].measurements?this.elementsTypes[et].measurements.length:0;
      mergeCols.push([lastCol, lastCol + countMeasurements]);
      lastCol = lastCol + countMeasurements + 1;
      headersH.push(this.elementsTypes[et].name.toUpperCase());
    }

    //  Interval of colums number to merge

    mergeCols.forEach(mergeCol => {
       ws['!merges'].push({ 
        s: { r: 0, c: mergeCol[0] }, 
        e:{ r: 0, c: mergeCol[1] } 
      });
    });

    // Drawing headers of headers
    headersH.forEach((headerH, index) => {
      ws[this.utilService.getLetter(mergeCols[index][0]) + (rowOffset - 1)] = {
        t: 's',
        v: headerH,
        s: AppSettings.fillClient
      };
    });

    //  Header column
    const headers = ['FECHA', 'AÑO', 'MES', 'OPERADOR', 'SUPERVISOR', 'IMPLEMENTADOR', 'COD. ELEMENTO', 'CLASIFICACIÓN ELEMENTO', 'ELEMENTO', 'TIPO ELEMENTO', 'CLIENTE', 'CAMPAÑA', 'CADENA', 'CODIGO SAP', 'TIENDA', 'CIUDAD', 'REGIÓN', 'IN', 'OUT', 'IN 2', 'OUT 2', 'STATUS MEDICIÓN'];
    //['CANTIDAD', 'LATERAL', 'FRONTAL INFERIOR', 'FRONTAL SUPERIOR', 'FLEJERAS', 'CANTIDAD', 'LATERAL SUPERIOR', 'LATERAL INFERIOR', 'FRONTAL INFERIOR', 'FRONTAL SUPERIOR', 'FLEJERAS', 'CANTIDAD', 'LATERAL', 'LADO LATERAL', 'LADO FRONTAL', 'FLEJERAS', 'CANTIDAD', 'LADO 1', 'LADO 2', 'CANTIDAD', 'FRENTES', 'CANTIDAD', 'FRENTE'];
    for (var et = this.elementsTypes.length - 1; et >= 0; et--) {
      var tMeasurements = this.elementsTypes[et].measurements;
      headers.push('CANTIDAD');
      propertyMeasurements.push([this.elementsTypes[et].name, 'quantity']);
      if(tMeasurements){
        for (var m = tMeasurements.length - 1; m >= 0; m--) {
          headers.push(tMeasurements[m].toUpperCase());
          propertyMeasurements.push([this.elementsTypes[et].name, tMeasurements[m]]);
        }
      }
    }
    headers.push('LINK FOTO');

    // Configuring 12 width cells with same size
    for (var i = 0; i < (headers.length - 1); i++) {
      wscols.push({ wch: columsWidth });
    }

    //  For link picture
    wscols.push({ wch: 90 });

    // Drawing headers
    headers.forEach((header, index) => {
      ws[this.utilService.getLetter(index) + rowOffset] = {
        t: 's',
        v: header,
        s: AppSettings.fillCabecera
      };
    });

    moment.locale('es');
    // Getting main data and drawing in excel
    dataExcel.result.forEach((element, storeIndex) => {

      const dateArray = this.getDataProperty(element, 'date').split('-');
      const year = dateArray[0];
      const month = moment().month(dateArray[1] - 1).format('MMMM');

      let chInstall = this.getDataProperty(element, 'in1');
      let chUninstall = this.getDataProperty(element, 'out1');

      let data = [
        this.getDataProperty(element, 'date').split('T')[0],
        year,
        month,
        this.getDataProperty(element, 'operatorFn') + ' ' +
        this.getDataProperty(element, 'operatorLn'),
        this.getDataProperty(element, 'supervisorFn') + ' ' +
        this.getDataProperty(element, 'supervisorLn'),
        this.getDataProperty(element, 'implementer'),
        this.getDataProperty(element, 'elementCode'),
        this.getDataProperty(element, 'elementClass'),
        this.getDataProperty(element, 'element'),
        this.getDataProperty(element, 'elementType'),
        this.getDataProperty(element, 'client'),
        this.getDataProperty(element, 'name'),
        this.getDataProperty(element, 'chain'),
        this.getDataProperty(element, 'sapCode'),
        this.getDataProperty(element, 'store'),
        this.getDataProperty(element, 'city'),
        this.getDataProperty(element, 'region'),
        (chInstall?chInstall:this.getDataProperty(element, 'duration').start).split('T')[0],
        (chUninstall?chUninstall:this.getDataProperty(element, 'duration').end).split('T')[0],
        this.getDataProperty(element, 'in2').split('T')[0],
        this.getDataProperty(element, 'out2').split('T')[0],
        (this.getDataProperty(element, 'statusMeasure')?'PENDIENTE Y OBSERVADO':'FINALIZADO'),
      ];
      for (var pm = 0; pm < propertyMeasurements.length; pm++) {
        data.push(this.writeElement(element, propertyMeasurements[pm][0], propertyMeasurements[pm][1]));
      }
      data.push((element.picture) ? '=HIPERVINCULO("' + AppSettings.BASE_PATH + element.picture + '";"link")' + '\n' : '');

      /*
        this.writeElement(element, 'CABECERA PLANA', 'quantity'),
        this.writeElement(element, 'CABECERA PLANA', 'Lateral'),
        this.writeElement(element, 'CABECERA PLANA', 'Frontal Inferior'),
        this.writeElement(element, 'CABECERA PLANA', 'Frontal Superior'),
        this.writeElement(element, 'CABECERA PLANA', 'Flejeras'),
        this.writeElement(element, 'CABECERA CON PANZA', 'quantity'),
        this.writeElement(element, 'CABECERA CON PANZA', 'Lateral Superior'),
        this.writeElement(element, 'CABECERA CON PANZA', 'Lateral Inferior'),
        this.writeElement(element, 'CABECERA CON PANZA', 'Frontal Inferior'),
        this.writeElement(element, 'CABECERA CON PANZA', 'Frontal Superior'),
        this.writeElement(element, 'CABECERA CON PANZA', 'Flejeras'),
        this.writeElement(element, 'CABECERA CON RUMA', 'quantity'),
        this.writeElement(element, 'CABECERA CON RUMA', 'Lateral'),
        this.writeElement(element, 'CABECERA CON RUMA', 'Lado Lateral'),
        this.writeElement(element, 'CABECERA CON RUMA', 'Lado Frontal'),
        this.writeElement(element, 'CABECERA CON RUMA', 'Flejeras'),
        this.writeElement(element, 'RUMA', 'quantity'),
        this.writeElement(element, 'RUMA', 'Lado 1'),
        this.writeElement(element, 'RUMA', 'Lado 2'),
        this.writeElement(element, 'CENEFA', 'quantity'),
        this.writeElement(element, 'CENEFA', 'Frentes'),
        this.writeElement(element, 'MARCO', 'quantity'),
        this.writeElement(element, 'MARCO', 'Frentes'),
      */

      // Drawing main data in excel, only 1 row
      data.forEach((colum, columIndex) => {
        ws[this.utilService.getLetter(columIndex) + (storeIndex + rowOffset + 1)] = {
          t: 's',
          v: colum,
          s: AppSettings.fillClient
        };
      });

    });


    // Final configurations to draw excel

    // The total size is length data + offset + 1(for the header)
    ws["!ref"] = "A1:RW" + (dataExcel.result.length + rowOffset + 1);
    ws['!cols'] = wscols;

    let wopts: any = {
      bookType:'xlsx',
      bookSST:false,
      type : 'binary',
      Props : { cellStyles : true, cellFormula : true }
    };

    let wbout = XLSX.write(this.wb, wopts);
    saveAs(new Blob([ this.utilService.s2ab(wbout) ], { type : "application/octet-stream" }),
           "reporte_medidas.xlsx");
    this.loader.fire(false);
  }

  /**
   *  Find if the object contain elementType, if contain the return measureType
   *
   *  @param {Object} obj : object to analize
   *  @param {String} elementType : element type
   *  @param {String} measureType : measure type
   *  @return {String} : caracteristic pointed out by the input parameters
   */

  writeElement(obj, elementType, measureType) {

    let response = '';

    if (obj.elementType.toLowerCase() == elementType.toLowerCase()) {
      switch (measureType) {
        case 'quantity':
          // I'm not sure about this, for now I consider the first element
          response = '';
          if(!obj.measurements){
            response = '1';
          }
          else{
            for (var i = 0; i < obj.measurements.length; i++) {
              response += '-' + obj.measurements[i].side + ':' + obj.measurements[i].quantity + '\n';
            }
          }          
          break;
        default:
          //  Figure out all measurements and return the correcponding side
          for (var i = 0; i < obj.measurements.length; i++) {
            if (obj.measurements[i].side.toLowerCase() == measureType.toLowerCase()) {
              response = obj.measurements[i].value;
              break;
            }
          }
          break;
      }
    }
    return response;
  }

  /**
   *  To figure out if an object have all properties passed
   *  IMPORTANT, this function is just to validate if the data exist
   *  and it not has a influence in the logic
   *
   *  @param {object} obj : object to analize
   *  @return {string} : return string data if it exits else send empty string
   */

  getDataProperty(obj, properties)
  {
    // Get all properties to analize
    let allProperties = properties.split('.');
    // Analize all properties
    for (var i = 0; i < allProperties.length; i++) {
      if (!obj || !obj.hasOwnProperty(allProperties[i])) {
        return '';
      }
      obj = obj[allProperties[i]];
    }

    // New validation case the final value is null
    return obj ? obj : '';
  }


}
