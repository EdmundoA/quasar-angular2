import {Component, OnInit, ViewChild} from '@angular/core';
import {GeneralService} from "../../services/general.service";
import {LoaderEvent} from "../../services/loader-event";
import {SessionService} from "../../services/session.service";
import * as moment from 'moment';
import {ChainsService} from "../../services/chains.service";
import {OrganizationService} from "../../services/organization.service";
import {CampaignsService} from "../../services/campaigns.service";

@Component({
  selector: 'app-general-resume',
  templateUrl: './general-resume.component.html',
  providers: [
    GeneralService, LoaderEvent, SessionService, ChainsService, OrganizationService, CampaignsService
  ]
})
export class GeneralResumeComponent implements OnInit {

  public user;
  public showResults = false;
  public cadenas = [];
  public organizations = [];
  public organization = '';
  public resume = [];
  public report = {
    chains: [],
    stores: [],
    clients: [],
    elements: [],
    allInstalls: 0,
    classElements: [],
  };
  public chains = [];
  public categories = [];
  public subcategories = [];
  public search = {
    chain: '',
    from: moment(),
    to: moment(),
    category: ''
  };
  public datePickerConfig = {
    closeOnSelect: true,
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'}
  };

  public resumepie = {
    view: [700, 300],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    legendTitle: 'Cadenas',
    data: [ ]
  };

  public chainsPie = {
    view: [700, 400],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    legendTitle: 'Cadenas',
    data: [ ]
  };

  public storesPie = {
    view: [700, 400],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    legendTitle: 'Tiendas',
    data: [ ]
  };

  public clientsPie = {
    view: [700, 400],
    showLegend: true,
    showLabels: true,
    explodeSlices: false,
    doughnut: false,
    autoScale: true,
    legendTitle: 'Clientes',
    data: [ ]
  };

  public elementsPie = [];
  public showNoResult = false;

  constructor(
    private general: GeneralService,
    private chainService: ChainsService,
    private loader: LoaderEvent,
    private session: SessionService,
    private organizationService: OrganizationService,
    private campaignService: CampaignsService
  ) { }

  ngOnInit() {
    this.user = this.session.getObject('user');
    //this.categories = this.user.permissions.categories;
    this.subcategories = this.user.permissions.categories;

    if(this.user.role == 'Gerente' || this.user.role == 'Jefe de categoria'){
      this.organization = this.user.permissions.organization;
      this.getGeneral();
    }else{
      this.getOrganizations();
    }
  }

  getOrganizations(){
    this.loader.fire(true);
    this.organizationService.getAll()
    .subscribe( response => {
      this.loader.fire(false);
      this.organizations = response.result;
    });
  }

  onSelect(){
    if(this.organization != ''){
      this.getGeneral();
    }
  }

  getGeneral(){
    this.loader.fire(true);
    this.general.getResume({organization: this.organization})
    .subscribe( response => {
      this.cadenas = [];
      this.resume = response.result;

      console.log(this.resume);
      let total = 0;
      for (var i = 0; i < this.resume.length; i++) {
        for (var j = 0; j < this.resume[i].chains.length; j++) {
          this.cadenas.push(this.resume[i].chains[j]);
          total += this.resume[i].chains[j].count;
        }                     
      }

      let data = [];
      for (var i = 0; i < this.resume.length; i++) {
        for (var j = 0; j < this.resume[i].chains.length; j++) {
          this.resume[i].chains[j]['percent'] = ((this.resume[i].chains[j].count/total)*100).toFixed(2);
          data.push({ name: this.resume[i].chains[j].name, value: ((this.resume[i].chains[j].count/total)*100).toFixed(2)});
        }                     
      }
      this.cadenas.sort(this.maxNumber);
      // console.log(data);
      this.resumepie.data = data;
      this.loader.fire(false);
      this.getFilters();
    });
  }

  getFilters(){
    this.loader.fire(true);
    //obtener cadenas
    this.chainService.getChainsByOrganization({organization: this.organization})
    .subscribe( response => {
      this.chains = response.result;
      // console.log(this.chains);
      this.loader.fire(false);
      this.getCategories();
    });
  }

  getCategories(){
    this.loader.fire(true);
    this.campaignService.getCategories()
    .subscribe(response => {       
      // console.log(response);
      response.result.forEach( master => {
        master.categories.forEach( category => {
          if(this.user.role == 'Gerente' || this.user.role == 'Jefe de categoria'){
            category.subcategories.forEach( subcatgory =>{
              //this.categories.push(subcatgory);
              for (var i = 0; i < this.subcategories.length; i++) {
                if(subcatgory == this.subcategories[i] && !this.existCategories(category.name)){
                  this.categories.push(category.name);
                }
              }
            });
          }
          else{
            //if(this.user.role == 'Admin'){}
            this.categories.push(category.name);
          }
        });
      });
      this.loader.fire(false);
    });
  }

  existCategories(name){
    for (var i = 0; i < this.categories.length; i++) {
      if(this.categories[i] == name){
        return true;
      }
    }
    return false;
  }

  onSearch(){
    this.loader.fire(true);
    let params = {
      chain: this.search.chain,
      from: this.search.from.hours(0).minutes(0).second(0).milliseconds(0).toDate(),
      to:this.search.to.hours(0).minutes(0).second(0).milliseconds(0).toDate(),
      category: this.search.category,
    };

    this.general.getReport(params)
      .subscribe(response => {
        console.log(response);
        this.report = response.result;

        let chainsData = [];
        let storesData = [];
        let clientsData = [];
        let elementsData = [];
        let totalchain = 0;
        let totalstores = 0;
        let totalclients = 0;
        let totalelements = 0;
        let totalClassElements = {};
        let totalInstalls = this.report.allInstalls;

        console.log(totalInstalls);
        this.showNoResult = false;

        if(totalInstalls > 0){


          for (let i = 0; i < this.report.chains.length; i++) {
            totalchain += this.report.chains[i].count;
          }

          for (let i = 0; i < this.report.chains.length; i++) {
            this.report.chains[i]['percent'] = ((this.report.chains[i].count/((totalchain < totalInstalls)?totalInstalls:totalchain))*100).toFixed(2);
          }

          for (let i = 0; i < this.report.stores.length; i++) {
            totalstores += this.report.stores[i].count;
          }

          for (let i = 0; i < this.report.stores.length; i++) {
            this.report.stores[i]['percent'] = ((this.report.stores[i].count/((totalstores < totalInstalls)?totalInstalls:totalstores))*100).toFixed(2);
          }

          for (let i = 0; i < this.report.clients.length; i++) {
            totalclients += this.report.clients[i].count;
          }

          for (let i = 0; i < this.report.clients.length; i++) {
            this.report.clients[i]['percent'] = ((this.report.clients[i].count/((totalclients < totalInstalls)?totalInstalls:totalclients))*100).toFixed(2);
          }

          for (var re = this.report.elements.length - 1; re >= 0; re--) {
            var tClassElement = this.report.elements[re];
            for (var ce = this.report.classElements.length - 1; ce >= 0; ce--) {
              var classElementid = this.report.classElements[ce]._id;
              if(classElementid == tClassElement._id){
                if(!totalClassElements[classElementid]){
                  totalClassElements[classElementid] = {
                    count: 0,
                    total: this.report.classElements[ce].count,
                  };
                }
                if(totalClassElements[classElementid]){
                  for (var rel = tClassElement.elements.length - 1; rel >= 0; rel--) {
                    totalClassElements[classElementid].count += tClassElement.elements[rel].count;
                  }
                }
              }
            }            
          }

          for (let i = 0; i < this.report.elements.length; i++) {
            for (var j = 0; j < this.report.elements[i].elements.length; j++) {
              this.report.elements[i].elements[j]['percent'] = ((this.report.elements[i].elements[j].count/totalClassElements[this.report.elements[i]._id].count)*100).toFixed(2);
            }            
          }



          
          this.report.chains.sort(this.maxNumber);
          this.report.stores.sort(this.maxNumber);
          this.report.clients.sort(this.maxNumber);
          this.report.elements[0].elements.sort(this.maxNumber);
          if(this.report.elements.length > 1){
            this.report.elements[1].elements.sort(this.maxNumber);
          }



          if(totalstores < totalInstalls){
            this.report.stores[this.report.stores.length] = {
              _id: 'otros',
              count: totalInstalls - totalstores,
              percent: ((totalInstalls - totalstores)/totalInstalls*100).toFixed(2),
            }
          }

          if(totalchain < totalInstalls){
            this.report.chains[this.report.chains.length] = {
              _id: 'otros',
              count: totalInstalls - totalchain,
              percent: ((totalInstalls - totalchain)/totalInstalls*100).toFixed(2),
            }
          }

          if(totalclients < totalInstalls){
            this.report.clients[this.report.clients.length] = {
              _id: 'otros',
              count: totalInstalls - totalclients,
              percent: ((totalInstalls - totalclients)/totalInstalls*100).toFixed(2),
            }
          }

          for (var re = this.report.elements.length - 1; re >= 0; re--) {
            var tClassElement = this.report.elements[re];
            if(totalClassElements[tClassElement._id].count < totalClassElements[tClassElement._id].total){
              var ttotal = totalClassElements[tClassElement._id].total;
              var tcount = ttotal - totalClassElements[tClassElement._id].count;
              tClassElement.elements.push({
                _id: 'otros',
                count: tcount,
                percent: (tcount/ttotal*100).toFixed(2),
              });
            }        
          }



          this.report.chains.forEach( item => {          
            chainsData.push({
              name: item._id,
              value: item.count || 0,
            })
          });

          this.report.stores.forEach( item => {
            storesData.push({
              name: item._id,
              value: item.count || 0,
            })
          });

          this.report.clients.forEach( item => {
            clientsData.push({
              name: item._id,
              value: item.count || 0,
            })
          });



          this.report.elements.forEach( item => {
            let pie = {
              view: [700, 400],
              showLegend: true,
              showLabels: true,
              explodeSlices: false,
              doughnut: false,
              autoScale: true,
              legendTitle: 'Elementos',
              data: [ ],
            };
            let pieData = [];
            item.elements.forEach( element => {
              pieData.push({
                name: element._id,
                value: element.count || 0,
              })
            });
            pie.data = pieData;
            this.showResults = true;
            elementsData.push(pie);
          });

          this.chainsPie.data = chainsData;
          this.storesPie.data = storesData;
          this.clientsPie.data = clientsData;
          this.elementsPie = elementsData;

          console.log(totalClassElements);

          this.loader.fire(false);

        }else{
          this.loader.fire(false);
          this.showResults = false;
          this.showNoResult = true;
        }

      });
  }

  maxNumber(a, b) {
    if(a.count > b.count) return -1;
    else if (b.count > a.count) return 1;
    return 0;
  }
}
