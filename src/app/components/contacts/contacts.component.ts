import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  providers: [
  	SubtitleEvent
  ]
})
export class ContactsComponent implements OnInit {

  public subtitle: string = '';

  constructor(
		private subtitleEvent: SubtitleEvent
  	) { }

  ngOnInit() {
  	this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
