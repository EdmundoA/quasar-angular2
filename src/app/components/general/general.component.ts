import { Component, OnInit } from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  providers: [
    SubtitleEvent
  ]

})
export class GeneralComponent implements OnInit {

  public subtitle: string = '';

  constructor(
    private subtitleEvent: SubtitleEvent
  ){

  }

  ngOnInit(){
    this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
    .subscribe(value => {
      this.subtitle = value;
    });
  }

}
