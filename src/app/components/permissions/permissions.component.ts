import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { PermissionsService } from '../../services/permissions.service';
import { LoaderEvent } from '../../services/loader-event';

import {SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
})
export class PermissionsComponent implements OnInit {

	public permisos: Array<any> = [];
	public modules: Array<any> = [];
	public sub: any;
	public user_id:String;
  public cabeceras: Array<String> = [''];
  public tabla: Array<any> = [];

  constructor(
  		private subtitleEvent: SubtitleEvent,
  		private permissionService: PermissionsService,
  		private routeParams: ActivatedRoute,
  		private router: Router,
      private loader: LoaderEvent,
  	) { 
  		var user_id;
  	 }

  ngOnInit() {
  	this.permissionService.getPermissions()
		.subscribe(response => {
      this.loader.fire(true);
    	this.permisos = response.result;
    	console.log(this.permisos);

      this.permissionService.getModules()
      .subscribe(response => {
        this.modules = response.result;
        this.modules = this.modules.filter(item => item.name.toLowerCase() != 'permiso');
        console.log(this.modules);
        let i = 0;
        for(i = 0; i < this.permisos.length; i++){
          this.cabeceras.push(this.permisos[i].name.toString());
        }
        for(i = 0; i < this.modules.length; i++){
          var row: any = {
            module: this.modules[i],
            permissions: [],
          };
          this.modules[i].values.forEach(item => {
            var permission: any = {
              permission: this.permisos[item],
              model: false,
            };
            row.permissions.push(permission);
          });
          this.tabla.push(row);
        }

        this.sub = this.routeParams.params
        .subscribe(params => {
          this.user_id = params['id'];
          this.permissionService.getPermissionsPerUser(this.user_id)
          .subscribe(response => {
            console.log(response);
            this.loader.fire(false);
            this.tabla.forEach(row => {
              let permisos = response.result[row.module.key];
              if (permisos) {
                permisos.forEach(permiso => {
                  for (i = 0; i < row.permissions.length; i++) {
                    if (row.permissions[i].permission.key.toString() === permiso) {
                      row.permissions[i].model = true;
                      break;
                    }
                  }
                });
              }
            });
          });
        });
      });         
    });
  }

  onChangeModel(rowT, permissionT){
    if(permissionT.permission.key.toLowerCase() != 'view' && permissionT.model) {
      rowT.permissions[0].model = true;
    }

    if(permissionT.permission.key.toLowerCase() == 'view' && !permissionT.model) {
      rowT.permissions[1].model = false;
      rowT.permissions[2].model = false;
      rowT.permissions[3].model = false;
    }
  }

  savePer() {
  	if(this.modules.length == 0){
  		return;
  	}
  	let params = {
      id:this.user_id,
  		allows:[]
    };
    this.tabla.forEach(row => {
      let param: any = {
        resources: row.module.key,
        permissions: []
      };
      row.permissions.forEach(permission => {
        if(permission.model) {
          param.permissions.push(permission.permission.key.toString());
        }
      });
      if (param.permissions.length > 0) {
        params.allows.push(param);
      }
    });
    console.log(params);
  	this.permissionService.allow(params)
		.subscribe(response => {
			// console.log(response);
			this.router.navigate(['/users']);
		});
  }

}
