import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserEntity } from '../../entities/user.entity';
import { PromotionsService } from '../../services/promotions.service';
import { LoaderEvent } from '../../services/loader-event';
import { SessionService } from '../../services/session.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { AppSettings } from '../../app.settings';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-promotions-list',
  templateUrl: './promotions-list.component.html',
  providers: [
    SessionService,
    PromotionsService,
    LoaderEvent,
    SubtitleEvent,
  ]
})
export class PromotionsListComponent implements OnInit {

  public userFilter: any = { name: '' };
  public message: String;
  public URL = AppSettings.BASE_PATH;

  public promo;

  constructor(
    private session: SessionService,
    private promoS: PromotionsService,
    private loader: LoaderEvent,
    private router: Router,
    private subtitleEvent: SubtitleEvent
  ) {
  }

  ngOnInit() {
  	this.subtitleEvent.fire('');
  	this.loader.fire(true);
  	this.promoS.getPromotions()
  		.subscribe( response => {
  			// console.log(response.result);
  			this.promo = response.result;
  			this.loader.fire(false);
  		});
  }

  onDelete(data) {
  	var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.promoS.delete(data._id)
      .subscribe(
        response => {
          this.ngOnInit();
        }
      );
    }
  }
  
  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
