import { Component, OnInit } from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  providers: [
    SubtitleEvent
  ]
})
export class CampaignComponent implements OnInit {

	public subtitle: string = '';

	constructor(
			private subtitleEvent: SubtitleEvent
		) { }

	ngOnInit() {
	  this.registerBroadcast();
	}

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
