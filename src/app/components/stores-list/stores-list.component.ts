import { Component, OnInit } from '@angular/core';
import { StoresService } from '../../services/stores.service';
import { LoaderEvent } from '../../services/loader-event';
import { SubtitleEvent } from "../../services/subtitle-event";
import { SessionService } from "../../services/session.service";
import { HostListener } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-stores-list',
  templateUrl: './stores-list.component.html',
  providers: [
  	StoresService,
    SubtitleEvent,
    LoaderEvent,
    SessionService,
  ]
})
export class StoresListComponent implements OnInit {
	public stores: Array<String> = [];
  public userFilter: any = { name: '', region: '', sector: '', zone: '' };
  public message: String;
  public permissions:any;


  constructor(
  		private storeService: StoresService,
      private subtitleEvent: SubtitleEvent,
      private loader: LoaderEvent,
      private session: SessionService,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
    this.subtitleEvent.fire('');
    this.permissions = this.session.getObject('permissions');
  	this.storeService.getStores()
		.subscribe(response => {
			if(response.status === 200){
        for (let i = 0; i < response.result.length; i++) {
          if (response.result[i].enabled) {
            this.stores.push(response.result[i]);                  
          }
        }
        this.loader.fire(false);
      }            
		});
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar ${data.name}?`);
    if(confirmation){
      this.loader.fire(true);
      this.storeService.delete(data._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          // console.log(response);
          this.ngOnInit();
        }
      );
    }
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
