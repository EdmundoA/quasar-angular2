import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { LoaderEvent } from "../../services/loader-event";
import { InventoryService } from "../../services/inventories.service";
import * as moment from 'moment';

@Component({
  selector: 'app-cluster',
  templateUrl: './cluster.component.html',
  providers: [
  	LoaderEvent
  ]
})
export class ClusterComponent implements OnInit {

	public showPercent: Boolean = false;
	public filter = '1';

	public cluster;

	public search = {
		from : moment(),
		to: moment()
	}

	public elementsTotal = {
		aTotal : 0,
		bTotal : 0,
		cTotal : 0,
		vTotal: 0,
		totalTotal : 0,
		apercent: 0,
		bpercent: 0,
		cpercent: 0,
		vpercent: 0,
	}

	public cargaelements = {
		aTotal : 0,
		bTotal : 0,
		cTotal : 0,
		vTotal: 0,
		totalTotal : 0,
		apercent: 0,
		bpercent: 0,
		cpercent: 0,
		vpercent: 0,
	}

	public clientelements = {
		aTotal : 0,
		bTotal : 0,
		cTotal : 0,
		vTotal: 0,
		totalTotal : 0,
		apercent: 0,
		bpercent: 0,
		cpercent: 0,
		vpercent: 0,
	}

	public showTable: Boolean;

	public datePickerConfig = {
	    closeOnSelect: true,
	    disableKeypress: true,
	    firstDayOfWeek: 'mo',
	    weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'},
        monthsNames: {jan: 'ene', feb: 'feb'}
	};

	public message;

	public liviano = {
	    view: [700, 300],
	    showLegend: true,
	    showLabels: true,
	    explodeSlices: false,
	    doughnut: false,
	    autoScale: true,
	    title: 'Livianos',
	    data: [ ]
	};

	public carga = {
	    view: [700, 300],
	    showLegend: true,
	    showLabels: true,
	    explodeSlices: false,
	    doughnut: false,
	    autoScale: true,
	    title: 'Carga',
	    data: [ ]
	};

	public cliente = {
	    view: [700, 300],
	    showLegend: true,
	    showLabels: true,
	    explodeSlices: false,
	    doughnut: false,
	    autoScale: true,
	    title: 'Cliente',
	    data: [ ]
	};

    constructor(
  		private router: Router,
  		private loader: LoaderEvent,
  		private inventoryS: InventoryService,
  	) { }

    ngOnInit() {
    }

    showBy(filter) {
    	// 0 Porcentaje ---- 1 Numeros //
    	if (filter == 0) {
    		this.showPercent = true;
    	} else {
    		this.showPercent = false;
    	}
    }

    onSearch(data) {
    	this.loader.fire(true);
    	// console.log(data);
    	this.inventoryS.sendCluster(data)
		.subscribe( response => {
			this.cluster = response.result;
			this.showTable = true;

			if(this.cluster.class.length > 0){
				this.cluster.class[1].elements.sort(this.maxNumber);
				this.cluster.class[0].elements.sort(this.maxNumber);
			}						
			this.cluster.client.sort(this.maxNumber);

			this.elementsTotal.aTotal = 0;
			this.elementsTotal.bTotal = 0;
			this.elementsTotal.cTotal = 0;
			this.elementsTotal.vTotal = 0;
			this.elementsTotal.totalTotal = 0;
			this.cargaelements.aTotal = 0;
			this.cargaelements.bTotal = 0;
			this.cargaelements.cTotal = 0;
			this.cargaelements.vTotal = 0;
			this.cargaelements.totalTotal = 0;
			this.clientelements.aTotal = 0;
			this.clientelements.bTotal = 0;
			this.clientelements.cTotal = 0;
			this.clientelements.vTotal = 0;
			this.clientelements.totalTotal = 0;

			if(this.cluster.class.length == 0){
				this.loader.fire(false);
				this.showTable = false;
				return;
			}

			for (let i = 0; i < this.cluster.class[1].elements.length; i++) {
				this.cluster.class[1].elements[i]['percenta'] = ((this.cluster.class[1].elements[i].A/this.cluster.class[1].elements[i].total)*100).toFixed();
				this.cluster.class[1].elements[i]['percentb'] = ((this.cluster.class[1].elements[i].B/this.cluster.class[1].elements[i].total)*100).toFixed();
				this.cluster.class[1].elements[i]['percentc'] = ((this.cluster.class[1].elements[i].C/this.cluster.class[1].elements[i].total)*100).toFixed();
				this.cluster.class[1].elements[i]['percentv'] = ((this.cluster.class[1].elements[i].V/this.cluster.class[1].elements[i].total)*100).toFixed();
				this.elementsTotal.aTotal += this.cluster.class[1].elements[i].A;
				this.elementsTotal.bTotal += this.cluster.class[1].elements[i].B;
				this.elementsTotal.cTotal += this.cluster.class[1].elements[i].C;
				this.elementsTotal.vTotal += this.cluster.class[1].elements[i].V;
				this.elementsTotal.totalTotal += this.cluster.class[1].elements[i].total;
			}

			this.elementsTotal.apercent = Math.round((this.elementsTotal.aTotal/this.elementsTotal.totalTotal)*100);
			this.elementsTotal.bpercent = Math.round((this.elementsTotal.bTotal/this.elementsTotal.totalTotal)*100);
			this.elementsTotal.cpercent = Math.round((this.elementsTotal.cTotal/this.elementsTotal.totalTotal)*100);
			this.elementsTotal.vpercent = Math.round((this.elementsTotal.vTotal/this.elementsTotal.totalTotal)*100);

			for (let h = 0; h < this.cluster.class[0].elements.length; h++) {
				this.cluster.class[0].elements[h]['percenta'] = ((this.cluster.class[0].elements[h].A/this.cluster.class[0].elements[h].total)*100).toFixed();
				this.cluster.class[0].elements[h]['percentb'] = ((this.cluster.class[0].elements[h].B/this.cluster.class[0].elements[h].total)*100).toFixed();
				this.cluster.class[0].elements[h]['percentc'] = ((this.cluster.class[0].elements[h].C/this.cluster.class[0].elements[h].total)*100).toFixed();
				this.cluster.class[0].elements[h]['percentv'] = ((this.cluster.class[0].elements[h].V/this.cluster.class[0].elements[h].total)*100).toFixed();
				this.cargaelements.aTotal += this.cluster.class[0].elements[h].A;
				this.cargaelements.bTotal += this.cluster.class[0].elements[h].B;
				this.cargaelements.cTotal += this.cluster.class[0].elements[h].C;
				this.cargaelements.vTotal += this.cluster.class[0].elements[h].V;
				this.cargaelements.totalTotal += this.cluster.class[0].elements[h].total;
			}

			this.cargaelements.apercent = Math.round((this.cargaelements.aTotal/this.cargaelements.totalTotal)*100);
			this.cargaelements.bpercent = Math.round((this.cargaelements.bTotal/this.cargaelements.totalTotal)*100);
			this.cargaelements.cpercent = Math.round((this.cargaelements.cTotal/this.cargaelements.totalTotal)*100);
			this.cargaelements.vpercent = Math.round((this.cargaelements.vTotal/this.cargaelements.totalTotal)*100);

			for (let j = 0; j < this.cluster.client.length; j++) {
				this.cluster.client[j]['percenta'] = ((this.cluster.client[j].A/this.cluster.client[j].total)*100).toFixed();
				this.cluster.client[j]['percentb'] = ((this.cluster.client[j].B/this.cluster.client[j].total)*100).toFixed();
				this.cluster.client[j]['percentc'] = ((this.cluster.client[j].C/this.cluster.client[j].total)*100).toFixed();
				this.cluster.client[j]['percentv'] = ((this.cluster.client[j].V/this.cluster.client[j].total)*100).toFixed();
				this.clientelements.aTotal += this.cluster.client[j].A;
				this.clientelements.bTotal += this.cluster.client[j].B;
				this.clientelements.cTotal += this.cluster.client[j].C;
				this.clientelements.vTotal += this.cluster.client[j].V;
				this.clientelements.totalTotal += this.cluster.client[j].total;
			}

			this.clientelements.apercent = Math.round((this.clientelements.aTotal/this.clientelements.totalTotal)*100);
			this.clientelements.bpercent = Math.round((this.clientelements.bTotal/this.clientelements.totalTotal)*100);
			this.clientelements.cpercent = Math.round((this.clientelements.cTotal/this.clientelements.totalTotal)*100);
			this.clientelements.vpercent = Math.round((this.clientelements.vTotal/this.clientelements.totalTotal)*100);
			
			this.liviano.data = [
	  			{
	  				name: 'A',
	  				value: ((this.elementsTotal.aTotal/this.elementsTotal.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'B',
	  				value: ((this.elementsTotal.bTotal/this.elementsTotal.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'C',
	  				value: ((this.elementsTotal.cTotal/this.elementsTotal.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'V',
	  				value: ((this.elementsTotal.vTotal/this.elementsTotal.totalTotal)*100).toFixed(2),
	  			}

  			];

  			this.carga.data = [
  				{
	  				name: 'A',
	  				value: ((this.cargaelements.aTotal/this.cargaelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'B',
	  				value: ((this.cargaelements.bTotal/this.cargaelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'C',
	  				value: ((this.cargaelements.cTotal/this.cargaelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'V',
	  				value: ((this.cargaelements.vTotal/this.cargaelements.totalTotal)*100).toFixed(2),
	  			}
  			];

  			this.cliente.data = [
  				{
	  				name: 'A',
	  				value: ((this.clientelements.aTotal/this.clientelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'B',
	  				value: ((this.clientelements.bTotal/this.clientelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'C',
	  				value: ((this.clientelements.cTotal/this.clientelements.totalTotal)*100).toFixed(2),
	  			},
	  			{
	  				name: 'V',
	  				value: ((this.clientelements.vTotal/this.clientelements.totalTotal)*100).toFixed(2),
	  			}
  			];

  			this.loader.fire(false);

		}, error => {
			// console.log('xd');
			this.message = 'No se Encontraron Datos';
			this.loader.fire(false);
		});
    }

    maxNumber(a, b) {
    	if(a.total > b.total) return -1;
	    else if (b.total > a.total) return 1;
	    return 0;
    }

}
