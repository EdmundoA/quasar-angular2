import { Component, OnInit } from '@angular/core';
import {SubtitleEvent} from "../../services/subtitle-event";
import {ClientService} from "../../services/clients.service";
import {LoaderEvent} from "../../services/loader-event";
import {BtlService} from "../../services/btl.service";
import { ActivatedRoute, Router } from "@angular/router";
import * as moment from 'moment';

@Component({
  selector: 'app-btl-create',
  templateUrl: './btl-create.component.html',
  providers: [
    SubtitleEvent, ClientService, LoaderEvent, BtlService
  ]
})
export class BtlCreateComponent implements OnInit {

  public idBtl = '';
  public clients = [];
  public campaign = {
    name: '',
    client: '',
    brand: '',
    slides: [],
    message: '',
    objetives: [],
    detail: '',
    titleContacts: '',
    titleSales: '',
    label1: '',
    label2: '',
    label3: '',
    places: [],
    dates: [],
    products: [],
    contacts: [],
    activations: [],
    sales: [],
  };
  public newObjetive = '';
  public newPlace1 = '';
  public newPlace2 = '';
  public newPlace3 = '';
  public newProduct = '';
  public dates:Array<moment.Moment> = [moment()];

  public activations = [];
  public activationsByPlace = [];

  public datePickerConfig = {
    disableKeypress: true,
    firstDayOfWeek: 'mo',
    weekdayNames: {su: 'dom',mo: 'lun', tu: 'mar',we: 'mie',th: 'jue',fr: 'vie',sa: 'sab'},
    allowMultiSelect: true,
    closeOnSelect: false,
  };

  public message = '';
  public sub;
  public creating = true;

  public contactsDate = '';
  public indexContact = -1;

  public salesDate = '';
  public indexSale = -1;

  constructor(
    private subtitleEvent: SubtitleEvent,
    private clientService: ClientService,
    private loader: LoaderEvent,
    private btlService: BtlService,
    private routeParams: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    this.sub = this.routeParams.params.subscribe(
      params => {
        if (params['id']) {
          this.subtitleEvent.fire('Actualizar');

          this.loader.fire(true);
          this.btlService.detail(params['id'])
            .subscribe( response => {
              this.idBtl = params['id'];
              this.campaign = response.result;
              this.dates = [];
              for (var i = response.result.dates.length - 1; i >= 0; i--) {
                this.dates.push(moment(response.result.dates[i]));
              }
              this.loader.fire(false);
              this.creating = false;
              this.getClients();
            });

        }else{
          this.subtitleEvent.fire('Crear');
          this.getClients();
        }
      }
    );
  }

  getClients(){
    this.loader.fire(true);
    this.clientService.getClients()
      .subscribe( response => {
        this.clients = response.result;
        this.loader.fire(false);
      });
  }

  onAddObjetive(objetive){
    this.campaign.objetives.push(objetive);
    this.newObjetive = '';
  }

  removeObjetive(index){
    this.campaign.objetives.splice(index, 1);
  }

  onAddPlace(label1, label2, label3){
    this.campaign.places.push({
      label1: label1,
      label2: label2,
      label3: label3,
    });
    this.newPlace1 = '';
    this.newPlace2 = '';
    this.newPlace3 = '';
  }

  removePlace(index){
    this.campaign.places.splice(index, 1);
  }

  onAddProduct(product){
    this.campaign.products.push(product);
    this.newProduct = '';
  }

  removeProduct(index){
    this.campaign.products.splice(index, 1);
  }

  onSubmit(){
    //validaciones
    this.message = '';
    if(this.campaign.name.trim() == ''){
      this.message = 'Debes ingresar el nombre de la campaña';
    }else if(this.campaign.client.trim() == ''){
      this.message = 'Debes ingresar el cliente';
    }else if(this.campaign.brand.trim() == ''){
      this.message = 'Debes ingresar la marca';
    }else if(this.campaign.message.trim() == ''){
      this.message = 'Debes ingresar el mensaje a comunicar';
    }else if(this.campaign.detail.trim() == ''){
      this.message = 'Debes ingresar el detalle';
    }else if(this.campaign.objetives.length == 0){
      this.message = 'Debes ingresar los objetivos';
    }else if(this.dates.length == 0){
      this.message = 'Debes ingresar las fechas';
    }else if(this.campaign.places.length == 0){
      this.message = 'Debes ingresar los lugares';
    }else if(this.campaign.products.length == 0){
      this.message = 'Debes ingresar los productos';
    }else{
      /*this.dates.forEach( date => {
        this.campaign.dates.push(date.toDate());
      });*/
      if(this.creating){
        this.campaign.dates = this.dates;
        console.log(this.dates);
        this.loader.fire(true);
        this.btlService.save(this.campaign)
        .subscribe( response => {
          this.idBtl = response.result._id;
          this.loader.fire(false);
          this.router.navigate(['/btl/' + this.idBtl + '/edit']);
        });
      }
    }
  }

  showActivations(){
    this.loader.fire(true);
    this.btlService.getActivations(this.idBtl)
      .subscribe( response => {
        // console.log(response);
        this.activations = response.result;
        this.activationsByPlace = [];

        for(let activation of this.activations){
          activation.date = moment(new Date(activation._id)).format('DD/MM/YY');

          //extraer lugares
          for(let place of activation.places){

            let indPlace = this.activationsByPlace.findIndex( item =>
              item.place.label1 == place.place.label1 &&
              item.place.label2 == place.place.label2 &&
              item.place.label3 == place.place.label3
            );


            if(indPlace >= 0){
              this.activationsByPlace[indPlace][activation.date] = place.activated;
            }else{
              let inPlace = {
                place: place.place
              };

              inPlace[activation.date] = place.activated;

              this.activationsByPlace.push(inPlace);
            }
          }
        }


        this.loader.fire(false);
      });

  }

  saveActivations(){
    let params = [];

    //convertir a formato de la bd
    for(let activation of this.activations){

      for(let place of this.activationsByPlace){
        let actPlace = {
          place: place.place,
          activated: place[activation.date],
          date: new Date(activation._id),
        };

        params.push(actPlace);
      }
    }
    this.loader.fire(true);
    this.btlService.update(this.idBtl, {activations: params})
      .subscribe( response => {
        // console.log(response.result);
        this.campaign = response.result;
        this.loader.fire(false);
      });
  }

  showContacts(){

  }

  onSelectContactDate(date){
    this.indexContact = this.campaign.contacts.findIndex(item => item.date == date);
  }

  saveContacts(){
    this.loader.fire(true);
    this.btlService.update(this.idBtl, this.campaign)
      .subscribe( response => {
        // console.log(response.result);
        this.campaign = response.result;
        this.loader.fire(false);
      });
  }

  showSales(){

  }

  onSelectSaleDate(date){
    this.indexSale = this.campaign.contacts.findIndex(item => item.date == date);
    // console.log(this.campaign.sales[this.indexSale]);
  }
  
}
