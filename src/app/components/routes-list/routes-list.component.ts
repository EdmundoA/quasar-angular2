import { Component, OnInit } from '@angular/core';
import { RoutesService } from '../../services/routes.service';
import { LoaderEvent } from '../../services/loader-event';
import { SubtitleEvent } from "../../services/subtitle-event";
import { SessionService } from '../../services/session.service';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-routes-list',
  templateUrl: './routes-list.component.html',
  providers: [
    SubtitleEvent,
    LoaderEvent,
    RoutesService
  ]
})
export class RoutesListComponent implements OnInit {

  public routes: any = [];
  public routeFilter: any = { code: '' };
  public userFilter: any = { name: '' };
  public tRoutes: any = [];
  public permissions:any;

  constructor(
	  private routesService: RoutesService,
    private subtitleEvent: SubtitleEvent,
    private loader: LoaderEvent,
    private session: SessionService,
	) { }

  ngOnInit() {
  	this.loader.fire(true);
    this.subtitleEvent.fire('');
    this.permissions = this.session.getObject('permissions');
  	this.routesService.getRoute()
		.subscribe(response => {
		  this.routes = response.result;
      for (var i = this.routes.length - 1; i >= 0; i--) {
        for (var j = this.routes[i].implementer.length - 1; j >= 0; j--) {
           this.routes[i].implementer[j].name = this.routes[i].implementer[j].firstname + " " + this.routes[i].implementer[j].lastname;
        } 
      }
      this.tRoutes = JSON.parse(JSON.stringify(this.routes));
		  console.log(response);
		  this.loader.fire(false);
    });
  }

  changeName(ev){
    this.routes = JSON.parse(JSON.stringify(this.tRoutes));
    if(ev == ""){
      return;
    }
    ev = ev.toLowerCase();
    for (var i = this.routes.length - 1; i >= 0; i--) {
      var deleteRoute = true;
      for (var j = this.routes[i].implementer.length - 1; j >= 0; j--) {
        if(this.routes[i].implementer[j].name.toLowerCase().indexOf(ev) >= 0){
          deleteRoute = false;
        }
      }
      if(deleteRoute){
        this.routes.splice(i, 1);
      }
    }
  }

  onDelete(data){
    var confirmation = confirm(`¿Está seguro que desea eliminar la ruta con el código ${data.code}?`);
    if(confirmation){
      this.loader.fire(true);
      this.routesService.delete(data._id)
      .subscribe(
        response => {
          this.loader.fire(false);
          this.ngOnInit();
        }
      );
    }
  }

  compare(a, b) {
    if (a.code > b.code) return 1;
    else if( b.code > a.code) return -1;
    else return 0;
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone3");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone3');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone3").css({visibility:'hidden'});
            $("#clone3 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone3").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
