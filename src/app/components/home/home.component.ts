import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [SessionService]
})
export class HomeComponent implements OnInit {

  constructor(private sService: SessionService) { }

  ngOnInit() {
    // console.log(this.sService.getItem('token'));
  }

}
