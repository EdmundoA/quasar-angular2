import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { LoaderEvent } from "../../services/loader-event";
import { ClientService } from '../../services/clients.service';
import { ActivatedRoute, Router } from "@angular/router";
import { SubtitleEvent } from "../../services/subtitle-event";

@Component({
  selector: 'app-clientes-create',
  templateUrl: './clientes-create.component.html',
  providers:[
   LoaderEvent, 
   SubtitleEvent
  ]
})
export class ClientesCreateComponent implements OnInit {

  public message: String = '';
  public sub: any;
  public clientes: String;
  public creating: Boolean = true;
  public client: String;
  public id_client: String;


  constructor(
    private router: Router,
    private routeParams: ActivatedRoute,
    private loader: LoaderEvent,
    private subtitleEvent: SubtitleEvent,
    private serviceCli: ClientService, 
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.sub = this.routeParams.params.subscribe(
      params => {

        if (params['id']) {
        	this.id_client = params['id'];
          const id = params['id'];
          this.serviceCli.detail(id)
            .subscribe(response => {
              if (response.status === 200) {
                this.client = response.result.name;
                this.loader.fire(false);
                this.creating = false;
              }else {

              }
            });
          this.subtitleEvent.fire('Editar');
        
        }else{
          this.subtitleEvent.fire('Crear');
          this.loader.fire(false);
        }
      }
    );
  }

  onSubmit() {
  	let parametros = {
  		name: this.client,
  	}

  	if (this.creating) {
      this.loader.fire(true);
	    this.serviceCli.createClient(parametros)
	  		.subscribe(
	  			response => {
            this.loader.fire(false);
	  				this.router.navigate(['clientes']);
	  			},
          error => {
            this.loader.fire(false);
            this.message = 'Falta Ingresar un Nombre'
          }
	  		);
  	} else {
      this.loader.fire(true);
  		this.serviceCli.update(this.id_client, parametros)
  			.subscribe(
  				response => {
            this.loader.fire(false);
  					this.router.navigate(['clientes']);
  				}
  			);
  	}
  }

}
