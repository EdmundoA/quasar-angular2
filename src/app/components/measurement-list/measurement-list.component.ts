import { Component, OnInit } from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import { SubtitleEvent } from "../../services/subtitle-event";
import { LoaderEvent } from '../../services/loader-event';
import { AppSettings } from '../../app.settings';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-measurement-list',
  templateUrl: './measurement-list.component.html',
  providers: [
    LoaderEvent, SubtitleEvent
  ]
})
export class MeasurementListComponent implements OnInit {
  public URL = AppSettings.BASE_PATH;
	public measurement = [];
	public userFilter: any = { name: '', store: '', elementType: '', confirmed: { ok: null} };

  constructor(
      private campaignS: CampaignsService,
      private subtitleEvent: SubtitleEvent,
      private loader: LoaderEvent,
  	) { }

  ngOnInit() {
    this.loader.fire(true);
  	this.campaignS.getMeasurements()
		.subscribe( response => {
			this.measurement = response.result.sort(this.sortDate);
      for (let i = 0; i < this.measurement.length; i++) {
        if (!this.measurement[i].confirmed) {
          console.log(1);
          if (this.measurement[i].date) {
            this.measurement[i].confirmed = {
              ok:true
            };
          } else {
            this.measurement[i].confirmed = {
              ok:false
            };
          }            
        }
      }
      this.loader.fire(false);
      this.measurement.sort(function(a,b){return b.confirmed.ok-a.confirmed.ok});
    });
  }

  sortDate(a, b){
    var x = a.campaignsDate;
    var y = b.campaignsDate;
    if (x < y) { return 1;}
    if (x > y) { return -1;}
    return 0;
  }

  moveScrollTwo(){
      var scroll = $(window).scrollTop();
      var anchor_top = $("#tablescroll").offset().top;
      var anchor_bottom = $("#bottom_anchor").offset().top;
      if (scroll>anchor_top && scroll<anchor_bottom) {
        let clone_table = $("#clone2");
        if(clone_table.length == 0){
            clone_table = $("#tablescroll").clone();
            clone_table.attr('id', 'clone2');
            clone_table.css({position:'fixed',
                     'pointer-events': 'none',
                     top:0});
            clone_table.width($("#tablescroll").width());
            $("#contentScroll").append(clone_table);
            $("#clone2").css({visibility:'hidden'});
            $("#clone2 thead").css({visibility:'visible', 'pointer-events':'auto'});
        }
      } else {
        $("#clone2").remove();
      }
  };

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
      // console.log("scrolling...");
      this.moveScrollTwo();

  }

}
