import {AfterViewInit, Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {CampaignsService} from "../../services/campaigns.service";
import {LoaderEvent} from "../../services/loader-event";
import {SessionService} from "../../services/session.service";
import { Modal } from 'ngx-modal';
import {AppSettings} from "../../app.settings";
import {ClientService} from "../../services/clients.service";
import { PromotionsService } from "../../services/promotions.service";
import * as moment from "moment";

@Component({
  selector: 'app-clientes-resumen',
  templateUrl: './clientes-resumen.component.html',
  providers: [
    CampaignsService, LoaderEvent, SessionService
  ]
})
export class ClientesResumenComponent implements OnInit, AfterViewInit {

  @ViewChild('thisModal') public thisModal:Modal;
  public isModalShown:boolean = false;

  public elements = {
    active: [],
    concluded: []
  };
  public base = AppSettings.BASE_PATH;
  public idClient = '';
  public showPromotion: Boolean = false;
  public promotions;
  public detail;
  public role; 
  public user;
  public cliente;
  public type;
  public date = new Date();
  public year = this.date.getFullYear();
  public listClient = [];
  public fromYear = 0;
  public listYears = [];

  public datePickerConfig = {
    closeOnSelect: true,
    format: 'YYYY',
  };

  constructor(
    private campaignService: CampaignsService,
    private loader: LoaderEvent,
    private session: SessionService,
    private clientS: ClientService,
    private promoS: PromotionsService,
  ) { }

  ngOnInit() {
    var firstYear = 2016;
    var realYear = (new Date()).getFullYear();
    this.fromYear = realYear;
    for (var tYear = realYear; tYear >= firstYear; tYear--) {
      this.listYears.push(tYear);
    }

    this.listYears.reverse();

    this.user = this.session.getObject('user');
    this.clientS.getClients()
    .subscribe(response => {
      if(this.user.role == 'Cliente'){
        this.type = 'Cliente';
        this.cliente = true;
        this.getCampaigns(this.user.permissions.client);
        this.role = response.result.filter(item => item._id ==  this.user.permissions.client)[0].name;
        let id = response.result.filter(item => item._id ==  this.user.permissions.client)[0]._id;
        this.promoS.promotionByClient(id)  
        .subscribe( response => {
          this.showPromotion = true;
          this.promotions = response.result;
        });
      }
      else{
        this.listClient = response.result;
        console.log(this.listClient);
      }
    });
  }

  ngAfterViewInit() {
    if (this.user.role == 'Cliente') {
      this.thisModal.open();
    }    
  }

  closeModal() {
    this.thisModal.close();
  }

  changeYear(){
    this.getCampaigns();
  }

  getCampaigns(client=null){
    this.loader.fire(true);
    if(client){
      this.idClient = client;
    }    
    console.log('cliente: ' + this.idClient);
    this.campaignService.getCampaignsByClient2(this.idClient, this.fromYear.toString())
    .subscribe( response => {
      console.log(response);
      if(response.status == 200){
        this.loader.fire(false);        
        this.elements = response.result;

        if(this.elements.active[0]){
          this.campaignService.getCampaignsByClientDetail2(this.elements.active[0].campaigns[0]._id, this.elements.active[0].campaigns[0].code, this.fromYear.toString())
            .subscribe( response => {
              console.log(response);
              this.detail = response.result;
            });
        } else if(this.elements.concluded[0]){
          this.campaignService.getCampaignsByClientDetail2(this.elements.concluded[0].campaigns[0]._id, this.elements.concluded[0].campaigns[0].code, this.fromYear.toString())
            .subscribe( response => {
              console.log(response);
              this.detail = response.result;
            });
        }else{
          this.detail = {
            actives: 0,
            total: 0,
            without: 0,
            client: {
              _id: this.idClient,
              name: this.listClient.filter(tclient => tclient._id == this.idClient)[0].name,
            }
          }
        }          
      }
    });
  }
}
