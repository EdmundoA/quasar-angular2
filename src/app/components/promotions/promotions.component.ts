import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SubtitleEvent} from "../../services/subtitle-event";

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  providers: [
    SubtitleEvent
  ]
})
export class PromotionsComponent implements OnInit {

  public subtitle: string = '';

  constructor(
    private subtitleEvent: SubtitleEvent
  ){

  }

  ngOnInit(){
    this.registerBroadcast();
  }

  registerBroadcast() {
    this.subtitleEvent.on()
      .subscribe(value => {
        this.subtitle = value;
      });
  }

}
