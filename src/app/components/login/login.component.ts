import { Component, OnInit, Injectable } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserEntity } from '../../entities/user.entity';
import { SessionService } from '../../services/session.service';
import { Routes, RouterModule, Router } from '@angular/router';
import { LoaderEvent } from '../../services/loader-event';
import { AppPaths } from '../../app.path';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [
    FormBuilder, UserService, SessionService, LoaderEvent
  ]
})
export class LoginComponent implements OnInit {

  public userForm: FormGroup;

  public logo: string;
  public message: any;
  public Mshow: boolean;


  constructor(
    private _fb: FormBuilder,
    private userService: UserService,
    private session: SessionService,
    private router: Router,
    private loader: LoaderEvent,
  ) {
    this.logo = AppPaths.LOGO;

  }

  ngOnInit() {
    this.loader.fire(false);

    this.userForm = this._fb.group({
      email:  ['', Validators.required],
      password:  ['', Validators.required]
    });
  }

  close() {
    this.message = false;
  }

  onSubmit(model: UserEntity, isValid: Boolean) {
    if (isValid) {
      this.loader.fire(true);
      this.userService.login(model)
        .subscribe(
          response => {
            console.log(response);
            this.loader.fire(false);

            this.session.setItem('token', response.token);
            this.session.setObject('user', response.result);

            if(response.result.role == 'Admin'){
              this.router.navigate(['/users']);
            }else if(response.result.role == 'Cliente'){
              this.router.navigate(['/client']);
            }else if(response.result.role == 'Gerente' ||
                     response.result.role == 'Supervisor' ||
                     response.result.role == 'Jefe de categoria') {
              this.router.navigate(['/general']);
            }
          },
          error => {
            this.message = 'Datos Incorrectos';
            this.loader.fire(false);
          }
        );
    }

  }



  public ingresar = () => {

  }

}
