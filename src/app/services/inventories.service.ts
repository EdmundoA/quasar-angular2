import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CustomAuthHttp} from "../auth/custom-auth-http";


@Injectable()
export class InventoryService {

  constructor(
    private http: CustomAuthHttp
  ) { }


  getResume() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.INVENTORY)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getInventory(params = {}){
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.INVENTORY_GET, body, options)
     .map((res: Response) => {
       const response = res.json();
       return response;
     })
    .catch(this.handleError);
  }

  getMaterials(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.INVENTORY_MATERIALS)
     .map((res: Response) => {
       const response = res.json();
       return response;
     })
    .catch(this.handleError);
  }

  sendCluster(params:Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.INVENTORY_CLUSTER, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);    
  }

  searchInventory(params:Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.INVENTORY_STORES, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  searchInventoryNoQuasar(from, to){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.INVENOTRY_NOQUASAR + '/' + from + '/' + to)
     .map((res: Response) => {
       const response = res.json();
       return response;
     })
    .catch(this.handleError);
  }

  sendInventoryNoQuasarMasive(params:Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.INVENTORY_NOQUASAR_MASIVE, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  sendInventoryNoQuasarImage(params:Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.INVENTORY_NOQUASAR_IMAGE, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  handleError(error) {
    return Observable.throw(error.json.error || 'Server error');
  }

}
