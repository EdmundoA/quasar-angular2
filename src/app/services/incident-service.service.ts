import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {AuthHttp} from 'angular2-jwt';
import {CustomAuthHttp} from "../auth/custom-auth-http";

@Injectable()
export class IncidentServiceService {

  constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }


  	sendInsi(params: Object) {
  		const body = JSON.stringify(params);
	    const headers = new Headers({
	      'Content-Type':  'application/json',
	    });
	    const options = new RequestOptions({ headers:  headers });

	    return this.http.post(AppSettings.BASE_PATH + AppSettings.INSIDENTS, body, options)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
  	}

  	deleteInsi(params: Object){
  		const body = JSON.stringify(params);
  		const headers = new Headers({
  			'Content-Type': 'application/json',
  		});
  		const options = new RequestOptions({headers: headers});
  		return this.http.post(AppSettings.BASE_PATH + AppSettings.DELETE_INSI, body, options)
  		.map((res: Response) => {
  			const response = res.json();
  			return response;
  		})
  		.catch(this.handleError);
  	}


  	listInsi(params: Object) {
  		const body = JSON.stringify(params);
	    const headers = new Headers({
	      'Content-Type':  'application/json',
	    });
	    const options = new RequestOptions({ headers:  headers });

	    return this.http.post(AppSettings.BASE_PATH + AppSettings.LIST_INSI, body, options)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
  	}

  	updateInsi(id: String, params: Object) {
	    const body = JSON.stringify(params);
	    const headers = new Headers({
	      'Content-Type':  'application/json',
	      // 'Authorization':  SessionService.getInstance().get('token')
	    });
	    const options = new RequestOptions({ headers:  headers });

	    return this.http.put(AppSettings.BASE_PATH + AppSettings.INSIDENTS + id, body, options)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
	}



  handleError(error) {
	    return Observable.throw(error.json.error || 'Server error');
    }

}
