import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CustomAuthHttp} from "../auth/custom-auth-http";

@Injectable()
export class MaintenanceService {

  constructor(
    private http: CustomAuthHttp
  ) { }


  getMain() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CMP_MAINTENANCE)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  handleError(error) {
    return Observable.throw(error.json.error || 'Server error');
  }

  /**
   *  Get information to print inside excel worksheets
   */

  getDataExcel() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CMP_MAINTENANCE_EXCEL)
      .map((res: Response) => {
        return res.json();
      });
  }

}
