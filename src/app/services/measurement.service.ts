import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CustomAuthHttp} from "../auth/custom-auth-http";

@Injectable()
export class MeasurementService {

  constructor(
    private http: CustomAuthHttp
  ) { }

  /**
   *  Get information to print inside excel worksheets
   */

  getDataExcel() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.MEASUREMENT_EXCEL)
    .map((res: Response) => {
      return res.json();
    });
  }

  getAll_types() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.MEASUREMENT_ELEMENT_TYPES)
    .map((res: Response) => {
      const response = res.json();
      return response;
    })
  }

}
