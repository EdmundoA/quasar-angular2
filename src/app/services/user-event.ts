import { Observable } from 'rxjs/Observable';
import { Broadcaster } from './broacaster';
import { Injectable } from '@angular/core';

@Injectable()
export class UserEvent {
  constructor(private broadcaster: Broadcaster) {}

  fire(value: any): void {
    this.broadcaster.broadcast(UserEvent, value);
  }

  on(): Observable<any> {
    return this.broadcaster.on<any>(UserEvent);
  }
}