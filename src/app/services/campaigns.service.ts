import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CustomAuthHttp} from "../auth/custom-auth-http";

@Injectable()
export class CampaignsService {

  constructor(
  	private http: CustomAuthHttp,
  ) { }

  delete(id: String) {
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + id, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }



  nothidden(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + id + '/enable')
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  deleteever(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + id + '/delete')
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getImageCamp(route: any) {
    const body = JSON.stringify({file:route});
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + 'image_campaign', body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  deleteStore(params: Object) {
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers, body: params });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.CMP_STR, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCategories() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CATEGORIES)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getMeasurements() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CMP_MEASU)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getConfirmation() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CMP_CONFIRM)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getImplementers() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_IMPLEMENTERS)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  dataReport(from, to){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_REPORT + '/' + from + '/' + to)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getSubmotive(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_SUBMOTIVE)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getMotive(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_MOTIVE)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getMotiveInstall(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_MOTIVE_INSTALL)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getSubmotiveInstall(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_SUBMOTIVE_INSTALL)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  update(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getImage(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CMP_IMG + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaign() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGN)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }


  addElement(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CMP_ELE + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  sendMail(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_MAIL, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  addStore(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CMP_STR + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateStore(id: String, params: any) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.CMP_STR + id , body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  execStore(id: String, params: any) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.CMP_STR_EXEC + id , body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateLocationStores(id: String, params: any) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.CMP_STR_LOCATION + id , body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateEle(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.CMP_ELE + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  // SE PASA EL ID DE LA CAMPAÑA Y EL CODIGO DEL ELEMENTO

  deleteEle(id: String, code:String) {
    return this.http.delete(AppSettings.BASE_PATH + AppSettings.CMP_ELE + id + '/' + code)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  deleteEleever(id: String, code:String) {
    return this.http.delete(AppSettings.BASE_PATH + AppSettings.CMP_ELE + id + '/' + code + '/delete')
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  eleOnHidden(id: String, code: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CMP_ELE + id + '/' + code + '/enable')
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  create(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGN, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  detail(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGN + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClient(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT + id + '/')
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClient2(id: String, year:String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT + id + '/' + year)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClientDetail(campaign: String, code: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_DETAIL + campaign + '/' + code)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClientDetail2(campaign: String, code: String, year:String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_DETAIL + campaign + '/' + code + '/' + year)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClientResume(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_RESUME, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getCampaignsByClientInstalls(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_INSTALLS, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getBinnacle(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_BINNACLE, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }


  getEvidence(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.CAMPAIGNS_CLIENT_EVIDENCE, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  

  handleError(error) {
    return Observable.throw(error.json.error || 'Server error');
  }

}
