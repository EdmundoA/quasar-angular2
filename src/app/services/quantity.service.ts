import { Observable } from 'rxjs/Observable';
import { Broadcaster } from './broacaster';
import { Injectable } from '@angular/core';

@Injectable()
export class Quantity {
  constructor(private broadcaster: Broadcaster) {}

  fire(value: Boolean): void {
    this.broadcaster.broadcast(Quantity, value);
  }

  on(): Observable<boolean> {
    return this.broadcaster.on<boolean>(Quantity);
  }
}
