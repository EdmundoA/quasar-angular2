import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthHttp } from 'angular2-jwt';
import { CustomAuthHttp } from "../auth/custom-auth-http";

@Injectable()
export class TaskService {

  constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }

  createInstall(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKS_INSTALL, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  sendConfirmation(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_CONFIRM, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  sendMeasurement(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_MEASU, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createUninstall(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKS_UNINSTALL, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createInventory(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_INVENTORY, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateInventory(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_UPDATE_INVENTORY, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  deleteInventory(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_DELETE_INVENTORY, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createIssue(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({headers: headers});

    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASK_ISSUE, body, options)
    .map((res:Response) => {
      const response = res.json();
      return response;
    })
    .catch(this.handleError);
  }

  handleError(error) {
    return Observable.throw(error);
  }

  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

}
