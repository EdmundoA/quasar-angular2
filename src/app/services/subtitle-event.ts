import { Observable } from 'rxjs/Observable';
import { Broadcaster } from './broacaster';
import { Injectable } from '@angular/core';

@Injectable()
export class SubtitleEvent {
  constructor(private broadcaster: Broadcaster) {}

  fire(value: string): void {
    this.broadcaster.broadcast(SubtitleEvent, value);
  }

  on(): Observable<string> {
    return this.broadcaster.on<string>(SubtitleEvent);
  }
}
