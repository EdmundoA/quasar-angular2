import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthHttp } from 'angular2-jwt';
import { CustomAuthHttp } from "../auth/custom-auth-http";


@Injectable()
export class RoutesService {

  constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }


  getRoute() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.ROUTES)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  create(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.ROUTES, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createAlone(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.ROUTES_ALONE, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  delete(id: String) {
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.ROUTES + id, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  update(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.ROUTES + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  detail(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.ROUTES + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }


  handleError(error) {
    return Observable.throw(error);
  }

  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }


}

 