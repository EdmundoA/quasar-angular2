import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {AuthHttp} from 'angular2-jwt';
import {CustomAuthHttp} from "../auth/custom-auth-http";


@Injectable()
export class PermissionsService {
	constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }

	getPermissions() {
	    return this.http.get(AppSettings.BASE_PATH + AppSettings.ACL_PER)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
	}

	getPermissionsPerUser(id: String) {
	    return this.http.get(AppSettings.BASE_PATH + AppSettings.ACL_PER + id)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
	}

	allow(params: Object) {
	    const body = JSON.stringify(params);
	    const headers = new Headers({
	      'Content-Type':  'application/json',
	    });
	    const options = new RequestOptions({ headers:  headers });

	    return this.http.post(AppSettings.BASE_PATH + AppSettings.ACL_ALLOW, body, options)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
	}

	getModules() {
		return this.http.get(AppSettings.BASE_PATH + AppSettings.ACL_ADM)
	      .map((res: Response) => {
	        const response = res.json();
	        return response;
	      })
	      .catch(this.handleError);
	}

	handleError(error) {
	    return Observable.throw(error.json.error || 'Server error');
	}

	isAuthenticated() {
	    if (localStorage.getItem('token')) {
	      return true;
	    }
	    return false;
	}
}