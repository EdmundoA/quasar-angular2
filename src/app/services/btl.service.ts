import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthHttp } from 'angular2-jwt';
import { CustomAuthHttp } from "../auth/custom-auth-http";


@Injectable()
export class BtlService {

  constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }


  getAll() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.BTL)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getActivations(id) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.BTL_ACTIVATIONS + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  detail(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.BTL + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  update(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.BTL + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  save(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.BTL, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  delete(id: String) {
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.BTL + id, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  handleError(error) {
    return Observable.throw(error);
  }

  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }


}
