import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AuthHttp } from 'angular2-jwt';
import { CustomAuthHttp } from "../auth/custom-auth-http";


@Injectable()
export class StoresService {

  constructor(
    private session: SessionService,
    private http: CustomAuthHttp,
    private httpNoAuth: Http) { }


  getStores() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.STORES)
    .map((res: Response) => {
      const response = res.json();
      return response;
    })
    .catch(this.handleError);
  }

  groupOrganizations(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.STR_BY_ORGANIZATIONS)
    .map((res: Response) => {
      const response = res.json();
      return response;
    })
    .catch(this.handleError);
  }

  getPositions() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.STR_POSITION)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getStoresById(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.STR_CHAIN + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  delete(id: String) {
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.STORES + id, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  deleteContact(id: String, pos: String) {
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.delete(AppSettings.BASE_PATH + AppSettings.STR_CONTACT + id + '/' + pos, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getSectors() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.SECTORS)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
    
  }

  getRegions() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.REGIONS)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getZones() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.ZONES)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getClusters() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CLUSTER)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError); 
  }

  getClusters2() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CLUSTER2)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError); 
  }

  update(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.STORES + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateImageInstalled(id: String, params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.STR_IMG + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  updateContact(id: String, params: Object) {    
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
      // 'Authorization':  SessionService.getInstance().get('token')
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.put(AppSettings.BASE_PATH + AppSettings.STR_CONTACT + id, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getDepartment() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.DEPARTMENTS)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getProvinces(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.PROVINCES + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createStore(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.STORES, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  createContact(params: Object) {
    const body = JSON.stringify(params);
    const headers = new Headers({
      'Content-Type':  'application/json',
    });
    const options = new RequestOptions({ headers:  headers });

    return this.http.post(AppSettings.BASE_PATH + AppSettings.STR_CONTACT, body, options)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getDistricts(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.DISTRICTS + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getFormats() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.FORMATS)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  detail(id: String) {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.STORES + id)
      .map((res: Response) => {
        const response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  handleError(error) {
    return Observable.throw(error);
  }

  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

  /**
   *  To get information to print in the excel(report)
   */

  getDataExcel() {
    return this.http.get(AppSettings.BASE_PATH + AppSettings.EXCEL)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }


}
