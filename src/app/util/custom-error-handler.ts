import {ErrorHandler} from '@angular/core';
import {AuthorizationError} from "./authorization-error";

export class CustomErrorHandler extends ErrorHandler {
  constructor(){
    super(false);
  }

  public handleError(error: any): void {
    if(error.originalError instanceof AuthorizationError){
      console.info(`[CUSTOM ERROR]:::${error.originalError.toString()}`);
    } else {
      super.handleError(error);
    }
  }
}
