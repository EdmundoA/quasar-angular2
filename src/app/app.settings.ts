export class AppSettings {
  //public static BASE_PATH = "http://192.168.1.37:3036/"; // servidor de akira
  //public static BASE_PATH = "http://192.168.1.16:3036/"; // servidor de erving

  //public static BASE_PATH = 'http://dev.atypax.com:3036/'; // servidor de desarrollo
  //public static BASE_PATH = 'http://107.20.212.91:3036/'; // servidor de Quasar
  public static BASE_PATH = 'http://localhost:3036/'; //servidor de pablo

  public static ORGANIZATIONS = 'organizations/';

  public static CHAINS = 'chains/';
  public static CHAINS_ORGANIZATION = 'chains/organization/';

  public static SERVICES = 'services/';
  public static SERVICES_CLASS = 'services/class';

  public static USERS = 'users/';
  public static STORES = 'stores/';
  public static CLIENTS = 'clients/';
  public static LOCATIONS = 'locations';
  public static ELEMENTS = 'elements/';
  public static ROUTES = 'routes/';
  public static ROUTES_ALONE = 'routes/alone';
  public static CAMPAIGN = 'campaigns/';
  public static CAMPAIGNS_CLIENT = 'campaigns/client/';
  public static CAMPAIGNS_CLIENT_DETAIL = 'campaigns/client/detail/';
  public static CAMPAIGNS_CLIENT_INSTALLS = 'campaigns/client/installs/';
  public static CAMPAIGNS_CLIENT_RESUME = 'campaigns/client/resume/';
  public static CAMPAIGNS_CLIENT_EVIDENCE = 'campaigns/client/evidence/';
  public static CAMPAIGNS_CLIENT_BINNACLE = 'campaigns/client/binnacle/';
  public static CAMPAIGNS_IMPLEMENTERS = 'campaigns/implementers';
  public static CAMPAIGNS_MAIL = 'campaigns/sendmail';
  public static CAMPAIGNS_REPORT = 'campaigns/in';
  public static CAMPAIGNS_MOTIVE = 'campaigns/motive';
  public static CAMPAIGNS_MOTIVE_INSTALL = 'campaigns/motive/install';
  public static CAMPAIGNS_SUBMOTIVE = 'campaigns/submotive';
  public static CAMPAIGNS_SUBMOTIVE_INSTALL = 'campaigns/submotive/install';

  public static TASKS_INSTALL = 'tasks/install';
  public static TASKS_UNINSTALL = 'tasks/uninstall';
  public static TASK_CONFIRM = 'tasks/confirm';
  public static TASK_MEASU = 'tasks/measurement';
  public static TASK_INVENTORY = 'tasks/inventory';
  public static TASK_UPDATE_INVENTORY = 'tasks/updateInventory';
  public static TASK_DELETE_INVENTORY = 'tasks/deleteInventory';
  public static TASK_ISSUE = 'tasks/issue';

  public static INSIDENTS = 'incidents/';
  public static LIST_INSI = 'incidents/list';
  public static DELETE_INSI = 'incidents/deleteincident';

  public static PROMOTIONS = 'promotions/';
  public static PROM_IMGS = 'promotions/images/';

  public static NAVIGATION = 'navigations/';

  public static OPPORTUNITIES = 'opportunities/';
  public static OPPORTUNITIES_EXCEL = 'opportunities/excel';

  public static LOGIN = 'users/login';
  public static ROLES = 'users/roles/';

  public static CLASSES = 'elements/classes';

  public static TOP = 'top/';

  public static REGIONS = 'stores/regions';
  public static ZONES = 'stores/zones';
  public static CLUSTER = 'stores/clusters';
  public static CLUSTER2 = 'stores/clusters2';
  public static FORMATS = 'stores/formats';
  public static SECTORS = 'stores/sectors';
  public static STR_CHAIN = 'stores/chain/';
  public static STR_CONTACT = 'stores/contact/';
  public static STR_POSITION = 'stores/contact/positions';
  public static EXCEL = 'stores/excel';
  public static STR_IMG = 'stores/image/';
  public static STR_BY_ORGANIZATIONS = 'stores/group_organizations/';

  public static DEPARTMENTS = 'locations/departments';
  public static PROVINCES = 'locations/provinces/';
  public static DISTRICTS = 'locations/districts/';

  public static CATEGORIES = 'campaigns/categories';
  public static CMP_IMG = 'campaigns/image/';
  public static CMP_ELE = 'campaigns/element/';
  public static CMP_STR = 'campaigns/stores/';
  public static CMP_STR_EXEC = 'campaigns/stores/exec/';
  public static CMP_STR_LOCATION = 'campaigns/stores/location/';
  public static CMP_CONFIRM = 'campaigns/confirms';
  public static CMP_MEASU = 'campaigns/measurements';
  public static CMP_MAINTENANCE = 'campaigns/maintenances';
  public static CMP_MAINTENANCE_EXCEL = 'campaigns/maintenances/excel';

  public static MEASUREMENT_EXCEL = 'elements/excel';
  public static MEASUREMENT_ELEMENT_TYPES = 'elements/all_types_measurement';

  public static ACL_PER = 'acl/permissions/';
  public static ACL_ADM = 'acl/admin';
  public static ACL_ALLOW = 'acl/allow';

  public static GENERAL = 'generals/';
  public static GENERAL_REPORT = 'generals/report/';


  public static BTL = 'btls/';
  public static BTL_ACTIVATIONS = 'btls/activations/';

  public static INVENTORY = 'btls/activations/';

  public static ERROR_403 = 'No tienes permisos';

  public static INVENTORY_GET = 'inventories';
  public static INVENTORY_CLUSTER = 'inventories/cluster';
  public static INVENTORY_STORES = 'inventories/store';
  public static INVENOTRY_NOQUASAR = 'inventories/noquasar';
  public static INVENTORY_NOQUASAR_MASIVE = 'inventories/noquasar/massive';
  public static INVENTORY_NOQUASAR_IMAGE = 'inventories/noquasar/image';
  public static INVENTORY_MATERIALS = 'inventories/materials';

  public static fillTitle = {
    "font": {
        "sz": "18",
        "color": {
            "rgb": "8b4c94"
        },
        "name": "Arial"
    },
      "border": {},
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center'
    }
  };
  public static fillSubtitle = {
      "font": {
        "sz": "16",
        "color": {
          "rgb": "6b2c74"
        },
        "name": "Arial"
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'top'
      }
  };
  public static fillCabecera = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': '385C85'
        }
      },
      "font": {
        "sz": "14",
        "color": {
          "rgb": "ffffff"
        },
        "name": "Arial"
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center'
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };

  public static fillClient = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': 'f8f8f8'
        }
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center',
        'wrapText':true
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };

  public static fillStore = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': 'eeeeee'
        }
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center',
        'wrapText':true
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };

  public static fillElement = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': 'dadada'
        }
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center',
        'wrapText':true
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };

  public static fillPar = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': 'cccccc'
        }
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center',
        'wrapText':true
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };
  public static fillImpar = {
      'fill': {
        'patternType':'solid',
        'fgColor':{
          'rgb': 'dddddd'
        }
      },
      'alignment':{
        'vertical': 'center',
        'horizontal': 'center',
        'wrapText':true
      },
      'border':{
        'top':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'bottom':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'left':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        },
        'right':{
          'style': 'thin',
          'color':{
            'rgb': '000000',
          }
        }
      }
  };
}
