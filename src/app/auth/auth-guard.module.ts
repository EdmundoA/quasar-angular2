import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from "../services/user.service";
import { RouteService } from "../services/route.service";
import { AppSettings } from '../app.settings';
import { Location } from '@angular/common';


@Injectable()
export class AuthGuard implements CanActivate {

  public route;

  constructor(
    private router: Router, 
    private userServivice: UserService,
    private routeS: RouteService,
    private plat: Location
    ) { }

  canActivate() {

    if (this.userServivice.isAuthenticated()) {
      // logged in so return true
      var piecesUrl = this.plat.path().split('/');
      console.log(piecesUrl);
      if(piecesUrl.length > 2 && piecesUrl[1] == 'campaing'){
      }
      else{
        this.routeS.sendRoute({path: this.plat.path(), date: Date.now()})
        .subscribe( response => {
        });
      }      
      return true;
    }

    // not logged in so redirect to login page
    localStorage.clear();
    this.router.navigate(['/login']);
    return false;
  }
}
